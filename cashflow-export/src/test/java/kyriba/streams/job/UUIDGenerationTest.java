package kyriba.streams.job;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UUIDGenerationTest {
    @Test
    void testUUID(){
        assertEquals("32dc9922-4515-348b-b5eb-19a73d0cf8cb", UUID.nameUUIDFromBytes("CMP11".getBytes()).toString());
    }

}
