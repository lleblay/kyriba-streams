package kyriba.streams.job.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.common.typeutils.TypeSerializerSnapshot;
import org.apache.flink.core.memory.DataInputView;
import org.apache.flink.core.memory.DataOutputView;
import org.apache.flink.util.InstantiationUtil;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collections;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

/**
 * A serializer that serializes types via Avro.
 *
 * <p>The serializer supports:
 * <ul>
 * <li>efficient specific record serialization for types generated via Avro</li>
 * <li>serialization via reflection (ReflectDatumReader / -Writer)</li>
 * <li>serialization of generic records via GenericDatumReader / -Writer</li>
 * </ul>
 * The serializer instantiates them depending on the class of the type it should serialize.
 *
 * <p><b>Important:</b> This serializer is NOT THREAD SAFE, because it reuses the data encoders
 * and decoders which have buffers that would be shared between the threads if used concurrently
 */
public class KafkaKeyValueSerializer extends TypeSerializer<KafkaKeyValue> {

    private static final long serialVersionUID = 2L;
    private final String schemaRegistryUrl="http://schema-registry.data.k8s.kod.kyriba.com";
    private transient GenericAvroSerde serde = null;

    /**
     * The serializer configuration snapshot, cached for efficiency.
     */
    private static final ObjectMapper MAPPER = new ObjectMapper();
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    @Nonnull
    public Class<GenericRecord> getType() {
        return GenericRecord.class;
    }

    // ------------------------------------------------------------------------
    //  Properties
    // ------------------------------------------------------------------------

    @Override
    public boolean isImmutableType() {
        return false;
    }

    @Override
    public TypeSerializer<KafkaKeyValue> duplicate() {
        return new KafkaKeyValueSerializer();
    }

    @Override
    public KafkaKeyValue createInstance() {
        return InstantiationUtil.instantiate(KafkaKeyValue.class);
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public void serialize(KafkaKeyValue record, DataOutputView target) throws IOException {
        target.writeUTF(record.getTopic());
        try {
            GenericRecord value = record.getKey();
            if(value!=null) {
                byte[] buffer = serialize(record);
                target.writeInt(buffer.length);
                target.write(buffer);
            }
            else{
                target.writeInt(0);
            }

        } catch (RuntimeException e) {
            //to rework in PGS-343 error management
            throw new KafkaKeyValueSerializationException("Fail to write message key to the topic: " + record.getTopic(), e);
        }
        target.writeInt(record.getPartition());
        target.writeLong(record.getOffset());
        target.writeLong(record.getTimestamp());
        try {
            GenericRecord value = record.getValue();
            if(value!=null) {
                byte[] buffer = serialize(record);
                target.writeInt(buffer.length);
                target.write(buffer);
            }
            else{
                target.writeInt(0);
            }

        } catch (RuntimeException e) {
            //to rework in PGS-343 error management
            throw new KafkaKeyValueSerializationException("Fail to write message to the topic: " + record.getTopic(), e);
        }
    }

    @Override
    public KafkaKeyValue deserialize(DataInputView source) throws IOException {
        String topic = source.readUTF();
        int bufferSize = source.readInt();
        byte[] buffer = new byte[bufferSize];
        source.readFully(buffer);
        GenericRecord key = null;
        try {
            if(bufferSize!=0) {
                key = deserialize(topic, buffer);
            }
        } catch (RuntimeException e) {
            //to rework in PGS-343 error management
            throw new KafkaKeyValueSerializationException("Fail to read message to the topic: " + topic, e);
        }
        int partition = source.readInt();
        long offset = source.readLong();
        long timestamp = source.readLong();
        bufferSize = source.readInt();
        buffer = new byte[bufferSize];
        source.readFully(buffer);
        GenericRecord value = null;
        try {
            if(bufferSize!=0) {
                value = deserialize(topic, buffer);
            }
        } catch (RuntimeException e) {
            //to rework in PGS-343 error management
            throw new KafkaKeyValueSerializationException("Fail to read message to the topic: " + topic, e);
        }

        KafkaKeyValue kafkaKeyValue = KafkaKeyValue.create(topic, key, value, partition, offset, timestamp);
        return kafkaKeyValue;

    }

    @Override
    public KafkaKeyValue deserialize(KafkaKeyValue reuse, DataInputView source) throws IOException {
        return deserialize(source);
    }

    @Override
    public KafkaKeyValue copy(KafkaKeyValue from) {
        byte[] bytes = serialize(from);
        GenericRecord value = deserialize(from.getTopic(), bytes);
        return KafkaKeyValue.builder(from).withValue(value).build();
    }

    @Override
    public KafkaKeyValue copy(KafkaKeyValue from, KafkaKeyValue reuse) {
        return copy(from);
    }

    @Override
    public void copy(DataInputView source, DataOutputView target) throws IOException {
        // we do not have concurrency checks here, because serialize() and
        // deserialize() do the checks and the current concurrency check mechanism
        // does provide additional safety in cases of re-entrant calls
        serialize(deserialize(source), target);
    }


    // ------------------------------------------------------------------------
    //  Serialization
    // ------------------------------------------------------------------------

    @Override
    public int hashCode() {
        return 42 + GenericRecord.class.hashCode();
    }

    @Override
    public TypeSerializerSnapshot<KafkaKeyValue> snapshotConfiguration() {
        return new KafkaKeyValueSerializerSnapshot();
    }


    // ------------------------------------------------------------------------
    //  Copying
    // ------------------------------------------------------------------------

    @Override
    public boolean equals(Object obj) {
        return obj == this;
    }

    @Override
    public String toString() {
        return getClass().getName() + " (" + getType().getName() + ')';
    }

    private byte[] serialize(KafkaKeyValue record) {
        return getSerDes().serializer().serialize(record.getTopic(), record.getValue());
    }

    private GenericAvroSerde getSerDes() {
        if(this.serde==null){
            this.serde = new GenericAvroSerde();
            this.serde.configure(Collections.singletonMap(SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl), false);
        }

        return serde;
    }

    private GenericRecord deserialize(String topic, byte[] buffer) {
        return getSerDes().deserializer().deserialize(topic, buffer);
    }

    public static final class KafkaKeyValueSerializationException extends RuntimeException {
        public KafkaKeyValueSerializationException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
