package kyriba.streams.job.utils;

import org.apache.flink.api.common.typeinfo.TypeInfoFactory;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import java.lang.reflect.Type;
import java.util.Map;

public class KafkaKeyValueTypeInfoFactory extends TypeInfoFactory<KafkaKeyValue> {

    private static final KafkaKeyValueTypeInfo TYPE_INFO = new KafkaKeyValueTypeInfo();

    @Override
    public TypeInformation<KafkaKeyValue> createTypeInfo(Type t, Map<String, TypeInformation<?>> genericParameters) {
        return TYPE_INFO;
    }
}
