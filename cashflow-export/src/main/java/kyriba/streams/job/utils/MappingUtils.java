/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.UUID;

/**
 * @author M-VBE
 * @since 20.2
 */
public final class MappingUtils {
    private MappingUtils() {
    }


    public static Boolean toBoolean(Integer o) {
        if (o == null) return null;
        return o != 0;
    }


    public static Optional<BigInteger> toId(ByteBuffer value) {
        return value == null ? Optional.empty() : Optional.of(new BigInteger(value.array()));
    }


    public static Optional<Long> toLongId(ByteBuffer value) {
        return toId(value).map(BigInteger::longValue);
    }

    public static LocalDate toLocalDate(long date) {
        long dateInMilli = date;
        if (date > 1000000000000000L) {
            dateInMilli = date / 1000;
        }
        return LocalDate.ofInstant(Instant.ofEpochMilli(dateInMilli), ZoneOffset.UTC);
    }


    public static class UuidUtils {
        public static UUID asUuid(ByteBuffer bb) {
            long firstLong = bb.getLong();
            long secondLong = bb.getLong();
            return new UUID(firstLong, secondLong);
        }

        public static ByteBuffer asBytes(UUID uuid) {
            ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
            bb.putLong(uuid.getMostSignificantBits());
            bb.putLong(uuid.getLeastSignificantBits());
            return bb;
        }

    }

}
