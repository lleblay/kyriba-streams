package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * TypeInformation for {@link GenericRecord}.
 */
public class ConfluentAvroTypeInfo extends TypeInformation<GenericRecord> {

    private static final long serialVersionUID = 4141977586453820650L;

    private String topic;

    public ConfluentAvroTypeInfo(String topic) {
        this.topic = topic;
    }

    @Override
    public boolean isBasicType() {
        return false;
    }

    @Override
    public boolean isTupleType() {
        return false;
    }

    @Override
    public int getArity() {
        return 1;
    }

    @Override
    public int getTotalFields() {
        return 1;
    }

    @Override
    public Class<GenericRecord> getTypeClass() {
        return GenericRecord.class;
    }

    @Override
    public boolean isKeyType() {
        return false;
    }

    @Override
    public TypeSerializer<GenericRecord> createSerializer(ExecutionConfig config) {
        return new ConfluentAvroSerializer(topic);
    }

    @Override
    public String toString() {
        return String.format("GenericRecord(\"%s\")", topic);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ConfluentAvroTypeInfo) {
            ConfluentAvroTypeInfo avroTypeInfo = (ConfluentAvroTypeInfo) obj;
            return avroTypeInfo.topic.equals(topic);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return topic.hashCode();
    }

    @Override
    public boolean canEqual(Object obj) {
        return obj instanceof ConfluentAvroTypeInfo;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeUTF(topic);
    }

    private void readObject(ObjectInputStream ois) throws IOException {
        this.topic = ois.readUTF();
    }
}
