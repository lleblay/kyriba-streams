/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 09.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Expects that IN1 elements are keyed by unique value
 *
 * @author M-VBE
 * @since 20.2
 */
public class GenericKeyedBroadcastEnrichmentFunction<K, IN1, IN2> extends KeyedBroadcastProcessFunction<K, IN1, IN2, IN1> {
    private final MapStateDescriptor<K, IN2> brodcastStateDescritor;
    private final Function<IN2, K> keyExtractor;
    private final Function<IN1, K> keyExtractor2;
    private final BiFunction<IN1, IN2, IN1> enrichFunction;
    private ValueState<IN1> valuesToUpdated;
    private final ValueStateDescriptor<IN1> valueStateProperties;

    public GenericKeyedBroadcastEnrichmentFunction(ValueStateDescriptor<IN1> valueStateDescriptor, MapStateDescriptor<K, IN2> broadcastStateDescriptor,
                                                   SerializableFunction<IN2, K> keyBrodcastExtractor,
                                                   SerializableFunction<IN1, K> keyInExtractor,
                                                   SerializableBiFunction<IN1, IN2, IN1> enrichFunction) {
        this.brodcastStateDescritor = broadcastStateDescriptor;
        this.keyExtractor = keyBrodcastExtractor;
        this.keyExtractor2 = keyInExtractor;
        this.enrichFunction = enrichFunction;
        valueStateProperties = valueStateDescriptor;
    }

    @Override
    public void open(Configuration parameters) {
        valuesToUpdated = getRuntimeContext().getState(valueStateProperties);
    }

    @Override
    public void processElement(IN1 value, ReadOnlyContext ctx, Collector<IN1> out) throws Exception {
        IN2 in2Value = ctx
                .getBroadcastState(brodcastStateDescritor)
                .get(keyExtractor2.apply(value));

        if (in2Value != null) {
            out.collect(enrichFunction.apply(value, in2Value));
        } else {
            valuesToUpdated.update(value);
            final long futureTimer = ctx.timerService().currentProcessingTime() + 1000;
            ctx.timerService().registerProcessingTimeTimer(futureTimer);
        }
    }

    @Override
    public void processBroadcastElement(IN2 value, Context ctx, Collector<IN1> out) throws Exception {
        BroadcastState<K, IN2> bcState = ctx.getBroadcastState(brodcastStateDescritor);
        bcState.put(keyExtractor.apply(value), value);
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<IN1> out) throws Exception {
        final IN1 in1Value = valuesToUpdated.value();
        if (in1Value != null) {
            IN2 in2Value = ctx
                    .getBroadcastState(brodcastStateDescritor)
                    .get(keyExtractor2.apply(in1Value));

            if (in2Value != null) {
                out.collect(enrichFunction.apply(in1Value, in2Value));
            } else {
                final long futureTimer = ctx.timerService().currentProcessingTime() + 1000;
                ctx.timerService().registerProcessingTimeTimer(futureTimer);
            }
        }
    }
}
