package kyriba.streams.job.utils;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import javax.annotation.Nonnull;
import java.util.Map;

import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;
import static java.util.Collections.singletonMap;

/**
 * Configuration properties use to create Kafka producer and consumer.
 */
public class KafkaConfig {
    public static final String SCHEMA_REGISTRY_URL = "schema.registry.url";
    public static final String KAFKA_BOOTSTRAP = "kafka.bootstrapServers";

    private static Config config = ConfigFactory.parseResources("com.trmsys.pegasus.flink.conf").resolve();

    public String getBootstrapServers() {
        String urlOveride=System.getProperty(KAFKA_BOOTSTRAP);
        if (urlOveride != null && !urlOveride.isEmpty()) {
            return urlOveride;
        } else {
            return config.getString("bootstrapServers");
        }
    }

    public String getSchemaRegistryUrl() {
        String urlOveride=System.getProperty(SCHEMA_REGISTRY_URL);
        if (urlOveride != null && !urlOveride.isEmpty()) {
            return urlOveride;
        } else {
            return config.getString("schemaRegistryUrl");
        }
    }

    public String getUser() {
        return config.getString("user");
    }

    public String getPassword() {
        return config.getString("password");
    }

    public String getSecret(){
        return config.getString("secret");
    }

    public <T extends Map> T fillProperties(T props) {
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, getBootstrapServers());
        if (getBootstrapServers().startsWith("SASL_SSL")) {
            String offuscated = getPassword();
            props.put("security.protocol", "SASL_SSL");

            props.put(SaslConfigs.SASL_MECHANISM, "PLAIN");
            if (StringUtils.isEmpty(getUser())) {
                throw new KafkaException("KafkaUser shoudln't be empty when login on SQSL_SSL");
            }
            if (StringUtils.isEmpty(offuscated)) {
                throw new KafkaException("password shoudln't be empty when login on SQSL_SSL");
            }
            props.put(SaslConfigs.SASL_JAAS_CONFIG,
                "org.apache.kafka.common.security.plain.PlainLoginModule required username=\""
                + getUser() + "\" password=\"" + offuscated + "\";");
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/kafka-ssl/client.truststore.jks");
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, getSecret());
        } else if (getBootstrapServers().startsWith("SSL")) {
            String offuscated = getPassword();
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/kafka-ssl/client.truststore.jks");
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, offuscated);
            props.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, "PKCS12");
            props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/kafka-ssl/client.keystore.p12");
            props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, offuscated);
            props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, offuscated);
        }
        props.put(SCHEMA_REGISTRY_URL, getSchemaRegistryUrl());
        return props;
    }

    @Nonnull
    public Serde<GenericRecord> withGenericAvroSerde() {
        GenericAvroSerde valueSerde = new GenericAvroSerde();
        valueSerde.configure(singletonMap(SCHEMA_REGISTRY_URL_CONFIG, getSchemaRegistryUrl()), false);
        return valueSerde;
    }

    public static Serde<String> keySerde() {
        return Serdes.String();
    }
}
