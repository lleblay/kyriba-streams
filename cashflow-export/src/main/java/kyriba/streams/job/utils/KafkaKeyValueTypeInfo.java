package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.shaded.curator4.com.google.common.collect.ImmutableList;

import java.util.List;


/**
 * TypeInformation for {@link GenericRecord}.
 */
public class KafkaKeyValueTypeInfo extends PojoTypeInfo<KafkaKeyValue> {

    private static final long serialVersionUID = 4141977586453820650L;

    public KafkaKeyValueTypeInfo() {
        super(KafkaKeyValue.class, fields());
    }

    @Override
    public Class<KafkaKeyValue> getTypeClass() {
        return KafkaKeyValue.class;
    }

    @Override
    public boolean isKeyType() {
        return false;
    }

    @Override
    public TypeSerializer<KafkaKeyValue> createSerializer(ExecutionConfig config) {
        return new KafkaKeyValueSerializer();
    }

    @Override
    public String toString() {
        return "KafkaKeyValueType";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof KafkaKeyValueTypeInfo) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return 31 * getTotalFields();
    }

    @Override
    public boolean canEqual(Object obj) {
        return obj instanceof KafkaKeyValueTypeInfo;
    }


    private static List<PojoField> fields() {
        return ImmutableList
            .<PojoField>builder()
            .addAll(KafkaKVTypeInfo.fields())
            .build();
    }
}
