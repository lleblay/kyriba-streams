package kyriba.streams.job.utils;

import org.apache.flink.api.common.typeinfo.TypeInfo;

@TypeInfo(KafkaKVTypeInfoFactory.class)
public class KafkaKV<K,V> {
    private final String topic;
    private final int partition;
    private final long offset;
    private final long timestamp;
    private final K key;
    private final V value;

    public KafkaKV() {
        this.topic = "noTopic";
        this.key = null;
        this.value = null;
        this.partition = Integer.MIN_VALUE;
        this.offset = Long.MIN_VALUE;
        this.timestamp = Long.MIN_VALUE;
    }

    public KafkaKV(String topic, K key, int partition, long offset, long timestamp, V value) {
        this.topic = topic;
        this.key = key;
        this.partition = partition;
        this.offset = offset;
        this.timestamp = timestamp;
        this.value = value;
    }

    public String getTopic() {
        return topic;
    }

    public K getKey() {
        return key;
    }

    public int getPartition() {
        return partition;
    }

    public long getOffset() {
        return offset;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "KafkaKV{" +
            "topic='" + topic + '\'' +
            ", key='" + key + '\'' +
            ", partition=" + partition +
            ", offset=" + offset +
            ", timestamp=" + timestamp +
            ", value=" + value +
            '}';
    }

    public static <K,V> Builder<K,V> builder() {
        return new Builder<>();
    }

    public static class Builder<K,V> {
        private String topic;
        private K key;
        private int partition;
        private long offset;
        private long timestamp;
        private V value;

        public Builder<K,V> from(KafkaKV<K,V> that) {
            this.topic = that.topic;
            this.key = that.key;
            this.partition = that.partition;
            this.offset = that.offset;
            this.timestamp = that.timestamp;
            this.value = that.value;
            return this;
        }

        public String getTopic() {
            return topic;
        }

        public Builder<K,V> withTopic(String topic) {
            this.topic = topic;
            return this;
        }

        public K getKey() {
            return key;
        }

        public Builder<K,V> withKey(K key) {
            this.key = key;
            return this;
        }

        public int getPartition() {
            return partition;
        }

        public Builder<K,V> withPartition(int partition) {
            this.partition = partition;
            return this;
        }

        public long getOffset() {
            return offset;
        }

        public Builder<K,V> withOffset(long offset) {
            this.offset = offset;
            return this;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public Builder<K,V> withTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public V getValue() {
            return value;
        }

        public Builder<K,V> withValue(V value) {
            this.value = value;
            return this;
        }

        public KafkaKV<K,V> build() {
            return new KafkaKV<>(topic, key, partition, timestamp, timestamp, value);
        }

    }
}
