package kyriba.streams.job.utils;

import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.util.serialization.KeyedDeserializationSchema;
import org.apache.flink.streaming.util.serialization.KeyedSerializationSchema;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Collections;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

public class GenericAvroSchema implements KeyedDeserializationSchema<KafkaKeyValue>, KeyedSerializationSchema<KafkaKeyValue> {
    private static final long serialVersionUID = 1;
    private transient GenericAvroSerde valueSerde;
    private transient GenericAvroSerde keySerde;

    public GenericAvroSchema() {
    }

    public GenericAvroSerde getValueSerde() {
        if(this.valueSerde==null){
            this.valueSerde = new GenericAvroSerde();
            this.valueSerde.configure(Collections.singletonMap(SCHEMA_REGISTRY_URL_CONFIG, "http://schema-registry.data.k8s.kod.kyriba.com"), false);
        }
        return valueSerde;
    }

    public GenericAvroSerde getKeySerde() {
        if(this.keySerde==null){
            this.keySerde = new GenericAvroSerde();
            this.keySerde.configure(Collections.singletonMap(SCHEMA_REGISTRY_URL_CONFIG, "http://schema-registry.data.k8s.kod.kyriba.com"), true);
        }
        return keySerde;
    }

    @Override
    public boolean isEndOfStream(KafkaKeyValue nextElement) {
        return false;
    }

    @Override
    public KafkaKeyValue deserialize(byte[] messageKey, byte[] message, String topic, int partition, long offset) throws IOException {
        Deserializer<GenericRecord> valueDeserializer = getValueSerde().deserializer();
        Deserializer<GenericRecord> keyDeserializer = getKeySerde().deserializer();
        GenericRecord value = valueDeserializer.deserialize(topic, message);
        GenericRecord key = keyDeserializer.deserialize(topic, messageKey);
        return KafkaKeyValue.create(topic, key, value, partition, offset, 0);
    }

    @Override
    public KafkaKeyValue deserialize(ConsumerRecord<byte[], byte[]> record) {
        Deserializer<GenericRecord> valueDeserializer = getValueSerde().deserializer();
        Deserializer<GenericRecord> keyDeserializer = getKeySerde().deserializer();
        String topic = record.topic();
        GenericRecord value = valueDeserializer.deserialize(topic, record.value());
        GenericRecord key = keyDeserializer.deserialize(topic, record.key());
        return KafkaKeyValue.create(topic, key, value, record.partition(), record.offset(), record.timestamp());
    }

    @Override
    public TypeInformation<KafkaKeyValue> getProducedType() {
        return new KafkaKeyValueTypeInfo();
    }

    @Override
    public byte[] serializeKey(KafkaKeyValue element) {
        String topic = element.getTopic();
        return  getKeySerde().serializer().serialize(topic, element.getKey());
    }

    @Override
    public byte[] serializeValue(KafkaKeyValue element) {
        String topic = element.getTopic();
        return  getValueSerde().serializer().serialize(topic, element.getValue());
    }

    @Override
    public String getTargetTopic(KafkaKeyValue element) {
        String topic = element.getTopic();
        return topic;
    }
}
