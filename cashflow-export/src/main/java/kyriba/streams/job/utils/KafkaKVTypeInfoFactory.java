package kyriba.streams.job.utils;

import org.apache.flink.api.common.typeinfo.TypeInfoFactory;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import java.lang.reflect.Type;
import java.util.Map;

public class KafkaKVTypeInfoFactory extends TypeInfoFactory<KafkaKV> {
    @Override
    public TypeInformation<KafkaKV> createTypeInfo(Type t, Map<String, TypeInformation<?>> genericParameters) {
        return new KafkaKVTypeInfo();
    }
}
