package kyriba.streams.job.utils;

public class KafkaException extends RuntimeException {
    public KafkaException(String message){
        super(message);
    }
    public KafkaException(Exception ex){
        super(ex);
    }
}
