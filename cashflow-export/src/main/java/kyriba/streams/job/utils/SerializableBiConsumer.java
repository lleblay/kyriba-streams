/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 11.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import java.io.Serializable;
import java.util.function.BiConsumer;

/**
 * @author M-VBE
 * @since 20.2
 */
@FunctionalInterface
public interface SerializableBiConsumer<T, U> extends BiConsumer<T, U>, Serializable {
}
