package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.common.typeutils.TypeSerializerSchemaCompatibility;
import org.apache.flink.api.common.typeutils.TypeSerializerSnapshot;
import org.apache.flink.core.memory.DataInputView;
import org.apache.flink.core.memory.DataOutputView;

import java.io.IOException;

public class ConfluentAvroSerializerSnapshot implements TypeSerializerSnapshot<GenericRecord> {

    private String topic;

    public ConfluentAvroSerializerSnapshot()
    {

    }

    public ConfluentAvroSerializerSnapshot(String topic) {
        this.topic = topic;
    }

    @Override
    public int getCurrentVersion() {
        return 0;
    }

    @Override
    public void writeSnapshot(DataOutputView out) throws IOException {
        out.writeUTF(topic);
    }

    @Override
    public void readSnapshot(int readVersion, DataInputView in, ClassLoader userCodeClassLoader) throws IOException {
        this.topic = in.readUTF();
    }

    @Override
    public TypeSerializer<GenericRecord> restoreSerializer() {
        return new ConfluentAvroSerializer(topic);
    }

    @Override
    public TypeSerializerSchemaCompatibility<GenericRecord> resolveSchemaCompatibility(TypeSerializer<GenericRecord> newSerializer) {
        return TypeSerializerSchemaCompatibility.compatibleAsIs();
    }
}
