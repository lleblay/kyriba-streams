/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 03.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroSerializationSchema;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.annotation.Nullable;

/**
 * @author M-VBE
 * @since 20.2
 */
public class KafkaKeyedSerializationSchemaWrapper<K extends GenericRecord, V extends GenericRecord> implements KafkaSerializationSchema<Tuple2<K, V>> {

    private final ConfluentRegistryAvroSerializationSchema<V> valueSchema;
    private final ConfluentRegistryAvroSerializationSchema<K> keySchema;
    private final String topic;

    public KafkaKeyedSerializationSchemaWrapper(ConfluentRegistryAvroSerializationSchema<K> keySchema,
                                                ConfluentRegistryAvroSerializationSchema<V> valueSchema,
                                                String topic) {
        this.valueSchema = valueSchema;
        this.keySchema = keySchema;
        this.topic = topic;
    }

    @Override
    public void open(SerializationSchema.InitializationContext context) throws Exception {
        keySchema.open(context);
        valueSchema.open(context);
    }

    @Override
    public ProducerRecord<byte[], byte[]> serialize(Tuple2<K, V> element, @Nullable Long timestamp) {
        return new ProducerRecord<>(topic, keySchema.serialize(element.f0), valueSchema.serialize(element.f1));
    }
}
