/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;

import java.util.function.BiConsumer;

/**
 * @author M-VBE
 * @since 20.2
 */

// todo: how to deal with case when child was updated ?? Answer - we should map childs before the cash flow update
// todo: make it more generic if neecessary (replace tuple with class + factory method passed to constructor) 
public class OneToOneJoinFunctionWithoutSideOutput<T extends GenericRecord, V extends GenericRecord> extends KeyedCoProcessFunction<Long, T, V, Tuple2<T, V>> {
    private ValueState<T> main;
    private ValueState<V> join;

    private final Class<T> mainClass;
    private final Class<V> joinClass;


    public OneToOneJoinFunctionWithoutSideOutput(Class<T> mainClass, Class<V> joinClass) {
        this.mainClass = mainClass;
        this.joinClass = joinClass;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        main = getRuntimeContext().getState(new ValueStateDescriptor<>("main", mainClass));
        join = getRuntimeContext().getState(new ValueStateDescriptor<>("join", joinClass));
    }


    @Override
    public void processElement1(T mainValue, Context ctx,
                                Collector<Tuple2<T, V>> out) throws Exception {
        V joinValue = join.value();
        if (joinValue != null) {
            out.collect(Tuple2.of(mainValue, joinValue));
        } else {
            main.update(mainValue);
        }
    }


    @Override
    public void processElement2(V joinValue, Context ctx,
                                Collector<Tuple2<T, V>> out) throws Exception {
        final T mainValue = main.value();
        if (mainValue != null) {
            out.collect(Tuple2.of(mainValue, joinValue));
        } else {
            join.update(joinValue);
        }

    }
}
