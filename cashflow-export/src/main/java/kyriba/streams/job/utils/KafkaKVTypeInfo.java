package kyriba.streams.job.utils;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.runtime.kryo.KryoSerializer;
import org.apache.flink.shaded.curator4.com.google.common.collect.ImmutableList;

import java.util.List;

public class KafkaKVTypeInfo extends PojoTypeInfo<KafkaKV> {
    private static final long serialVersionUID = 1;

    public KafkaKVTypeInfo(Class<KafkaKV> typeClass, List<PojoField> fields) {
        super(typeClass, fields);
    }

    public KafkaKVTypeInfo() {
        super(KafkaKV.class, fields());
    }


    @Override
    public TypeSerializer<KafkaKV> createSerializer(ExecutionConfig config) {
        return new KryoSerializer<>(getTypeClass(), config);
    }

    static List<PojoField> fields() {
        return ImmutableList.of(
            newPojoField("topic", String.class),
            newPojoField("key", String.class),
            newPojoField("partition", int.class),
            newPojoField("offset", long.class),
            newPojoField("timestamp", long.class)
        );
    }

    static PojoField newPojoField(String fieldName, Class<?> typeClass, Class<?> clazz) {
        try {
            return new PojoField(clazz.getDeclaredField(fieldName), TypeInformation.of(typeClass));
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException("Cannot get the PojoField of: " + fieldName, e);
        }
    }

    private static PojoField newPojoField(String fieldName, Class<?> typeClass) {
        return newPojoField(fieldName, typeClass, KafkaKV.class);
    }

}
