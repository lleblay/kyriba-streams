/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;

import java.util.function.BiConsumer;

/**
 * @author M-VBE
 * @since 20.2
 */

// todo: how to deal with case when child was updated ?? Answer - we should map childs before the cash flow update
// todo: make it more generic if neecessary (replace tuple with class + factory method passed to constructor
// todo: does the steate shared across functions (name should be unique?)
public class OneToOneJoinFunction<T extends GenericRecord, V extends GenericRecord> extends KeyedCoProcessFunction<Object, T, V, Tuple2<T, V>> {
    private final String prefix;
    private ValueState<T> main;
    private ValueState<V> join;
    private ValueState<Long> timestamp;

    private final Class<T> mainClass;
    private final Class<V> joinClass;

    private final BiConsumer<Context, ? super V> sideOutputProducer;

    public OneToOneJoinFunction(Class<T> mainClass, Class<V> joinClass, SerializableBiConsumer<Context, ? super V> sideOutputProducer, String prefix) {
        this.prefix = prefix;
        this.mainClass = mainClass;
        this.joinClass = joinClass;
        this.sideOutputProducer = sideOutputProducer;
    }

    @Override
    public void open(Configuration parameters) {
        main = getRuntimeContext().getState(new ValueStateDescriptor<>(prefix + "main", mainClass));
        join = getRuntimeContext().getState(new ValueStateDescriptor<>(prefix + "join", joinClass));
        timestamp = getRuntimeContext().getState(new ValueStateDescriptor<>(prefix + "timestamp", Long.class));
    }


    @Override
    public void processElement1(T mainValue, Context ctx,
                                Collector<Tuple2<T, V>> out) throws Exception {
        Object currentKey = ctx.getCurrentKey();

        V joinValue = join.value();
        if (joinValue != null) {
//            join.clear();
//            if (timestamp.value() != null) {
//                ctx.timerService().deleteProcessingTimeTimer(timestamp.value());
//                timestamp.clear();
//            }
            out.collect(Tuple2.of(mainValue, joinValue));
        } else {
            main.update(mainValue);
        }
    }


    @Override
    public void processElement2(V joinValue, Context ctx,
                                Collector<Tuple2<T, V>> out) throws Exception {
        Object currentKey = ctx.getCurrentKey();

        final T mainValue = main.value();
        if (mainValue != null) {
            out.collect(Tuple2.of(mainValue, joinValue));
        } else {
            join.update(joinValue);
            sideOutputProducer.accept(ctx, joinValue);
//            final long time = ctx.timerService().currentProcessingTime() + 10000;
//            timestamp.update(time);
//            ctx.timerService().registerProcessingTimeTimer(time);
        }
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple2<T, V>> out) throws Exception {
        final V value = join.value();
        if (value != null) {
            sideOutputProducer.accept(ctx, value);
        }
    }
}
