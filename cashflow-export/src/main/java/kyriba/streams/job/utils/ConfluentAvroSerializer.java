package kyriba.streams.job.utils;

import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.common.typeutils.TypeSerializerSnapshot;
import org.apache.flink.core.memory.DataInputView;
import org.apache.flink.core.memory.DataOutputView;
import org.apache.flink.util.InstantiationUtil;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

/**
 * A serializer that serializes types via Avro.
 *
 * <p>The serializer supports:
 * <ul>
 * <li>efficient specific record serialization for types generated via Avro</li>
 * <li>serialization via reflection (ReflectDatumReader / -Writer)</li>
 * <li>serialization of generic records via GenericDatumReader / -Writer</li>
 * </ul>
 * The serializer instantiates them depending on the class of the type it should serialize.
 *
 * <p><b>Important:</b> This serializer is NOT THREAD SAFE, because it reuses the data encoders
 * and decoders which have buffers that would be shared between the threads if used concurrently
 */
public class ConfluentAvroSerializer extends TypeSerializer<GenericRecord> {

    private static final long serialVersionUID = 2L;

    /**
     * The serializer configuration snapshot, cached for efficiency.
     */
    private String topic;
    private transient GenericAvroSerde serDes = null;


    // ------------------------------------------------------------------------


    /**
     * Creates a new ConfluentAvroSerializer for the type indicated by the given class.
     * This constructor is expected to be used only with {@link GenericData.Record}.
     */
    public ConfluentAvroSerializer(String topic) {
        this.topic = topic;
    }


    // ------------------------------------------------------------------------

    @Nonnull
    public Class<GenericRecord> getType() {
        return GenericRecord.class;
    }

    // ------------------------------------------------------------------------
    //  Properties
    // ------------------------------------------------------------------------

    @Override
    public boolean isImmutableType() {
        return false;
    }

    @Override
    public TypeSerializer<GenericRecord> duplicate() {
        return new ConfluentAvroSerializer(topic);
    }

    @Override
    public GenericRecord createInstance() {
        return InstantiationUtil.instantiate(GenericRecord.class);
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public void serialize(GenericRecord record, DataOutputView target) throws IOException {
        byte[] buffer = getSerDes().serializer().serialize(topic, record);
        target.writeInt(buffer.length);
        target.write(buffer);
    }

    @Override
    public GenericRecord deserialize(DataInputView source) throws IOException {
        int bufferSize = source.readInt();
        byte[] buffer = new byte[bufferSize];
        source.readFully(buffer);
        return getSerDes().deserializer().deserialize(topic, buffer);
    }

    @Override
    public GenericRecord deserialize(GenericRecord reuse, DataInputView source) throws IOException {
        return deserialize(source);
    }

    @Override
    public GenericRecord copy(GenericRecord from) {
        byte[] bytes = getSerDes().serializer().serialize(topic, from);
        GenericRecord record = getSerDes().deserializer().deserialize(topic, bytes);
        return record;
    }


    // ------------------------------------------------------------------------
    //  Serialization
    // ------------------------------------------------------------------------

    @Override
    public GenericRecord copy(GenericRecord from, GenericRecord reuse) {
        byte[] bytes = getSerDes().serializer().serialize(topic, from);
        GenericRecord record = getSerDes().deserializer().deserialize(topic, bytes);
        return record;
    }

    @Override
    public void copy(DataInputView source, DataOutputView target) throws IOException {
        // we do not have concurrency checks here, because serialize() and
        // deserialize() do the checks and the current concurrency check mechanism
        // does provide additional safety in cases of re-entrant calls
        serialize(deserialize(source), target);
    }


    // ------------------------------------------------------------------------
    //  Copying
    // ------------------------------------------------------------------------

    @Override
    public int hashCode() {
        return 42 + GenericRecord.class.hashCode();
    }

    @Override
    public TypeSerializerSnapshot<GenericRecord> snapshotConfiguration() {
        return new ConfluentAvroSerializerSnapshot(topic);
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this;
    }

    @Override
    public String toString() {
        return getClass().getName() + " (" + getType().getName() + ')';
    }

    private GenericAvroSerde getSerDes() {
        if (serDes == null) {
            GenericAvroSerde valueSerde = new GenericAvroSerde();
            Map<String, Object> config = new HashMap<>();
            config.put(SCHEMA_REGISTRY_URL_CONFIG, getConfig().getSchemaRegistryUrl());
            valueSerde.configure(config, false);
            serDes = valueSerde;
        }
        return serDes;
    }

    private KafkaConfig getConfig() {
        return new KafkaConfig();
    }
}
