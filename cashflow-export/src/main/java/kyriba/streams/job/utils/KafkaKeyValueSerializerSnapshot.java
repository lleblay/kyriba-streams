package kyriba.streams.job.utils;

import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.common.typeutils.TypeSerializerSchemaCompatibility;
import org.apache.flink.api.common.typeutils.TypeSerializerSnapshot;
import org.apache.flink.core.memory.DataInputView;
import org.apache.flink.core.memory.DataOutputView;

import java.io.IOException;

public class KafkaKeyValueSerializerSnapshot implements TypeSerializerSnapshot<KafkaKeyValue> {

    @Override
    public int getCurrentVersion() {
        return 0;
    }

    @Override
    public void writeSnapshot(DataOutputView out) throws IOException {
        // Nothing to be written
    }

    @Override
    public void readSnapshot(int readVersion, DataInputView in, ClassLoader userCodeClassLoader) throws IOException {
        // Nothing to be read
    }

    @Override
    public TypeSerializer<KafkaKeyValue> restoreSerializer() {
        return new KafkaKeyValueSerializer();
    }

    @Override
    public TypeSerializerSchemaCompatibility<KafkaKeyValue> resolveSchemaCompatibility(TypeSerializer<KafkaKeyValue> newSerializer) {
        return TypeSerializerSchemaCompatibility.compatibleAsIs();
    }
}
