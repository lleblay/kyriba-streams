/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 09.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Almost same as {@link GenericKeyedBroadcastEnrichmentFunction} but collects values with the same attribute until necessary joined value will arrive
 * Requires stream IN1 to be keyed by joining key (e.g. for tenant enrichment data domain uid)
 *
 * @author M-VBE
 * @since 20.2
 */
public class MultipleProcessingGenericBroadcastFunction<K, IN1, IN2> extends KeyedBroadcastProcessFunction<K, IN1, IN2, IN1> {
    private final MapStateDescriptor<K, IN2> broadcastStateDescriptor;
    private final Function<IN2, K> keyBrodcastExtractor;
    private final Function<IN1, K> keyJoinExtractor;
    private final BiFunction<IN1, IN2, IN1> enrichFunction;
    private final MapStateDescriptor<K, List<IN1>> mapStateDesc;
    private MapState<K, List<IN1>> joinedEntitiesNotEnriched;

    public MultipleProcessingGenericBroadcastFunction(MapStateDescriptor<K, List<IN1>> mapStateDesc,
                                                      MapStateDescriptor<K, IN2> broadcastStateDescriptor,
                                                      SerializableFunction<IN2, K> keyBrodcastExtractor,
                                                      SerializableFunction<IN1, K> keyJoinExtractor,
                                                      SerializableBiFunction<IN1, IN2, IN1> enrichFunction) {
        this.broadcastStateDescriptor = broadcastStateDescriptor;
        this.keyBrodcastExtractor = keyBrodcastExtractor;
        this.keyJoinExtractor = keyJoinExtractor;
        this.enrichFunction = enrichFunction;
        this.mapStateDesc = mapStateDesc;
    }

    @Override
    public void open(Configuration parameters) {
        joinedEntitiesNotEnriched = getRuntimeContext().getMapState(mapStateDesc);
    }

    @Override
    public void processElement(IN1 value, ReadOnlyContext ctx, Collector<IN1> out) throws Exception {
        final ReadOnlyBroadcastState<K, IN2> broadcastedCouples = ctx.getBroadcastState(broadcastStateDescriptor);

        //add new element 
        final K joinedEntityUid = keyJoinExtractor.apply(value);
        List<IN1> joinedEntities = joinedEntitiesNotEnriched.get(joinedEntityUid);
        joinedEntities = joinedEntities == null ? new ArrayList<>() : joinedEntities;
        joinedEntities.add(value);
        joinedEntitiesNotEnriched.put(joinedEntityUid, joinedEntities);

        //process new and existing ones which are not processed
        for (Map.Entry<K, List<IN1>> entry : joinedEntitiesNotEnriched.entries()) {
            final K joiningKey = entry.getKey();
            final List<IN1> entitiesNotEnriched = entry.getValue();

            final IN2 couple = broadcastedCouples.get(joiningKey);
            if (couple == null)
                continue;

            for (IN1 toBeEnriched : entitiesNotEnriched) {
                out.collect(enrichFunction.apply(toBeEnriched, couple));
            }

            //clean processed state
            joinedEntitiesNotEnriched.remove(joinedEntityUid);
        }


    }

    @Override
    public void processBroadcastElement(IN2 value, Context ctx, Collector<IN1> out) throws Exception {
        BroadcastState<K, IN2> bcState = ctx.getBroadcastState(broadcastStateDescriptor);
        bcState.put(keyBrodcastExtractor.apply(value), value);
    }
}
