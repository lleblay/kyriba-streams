package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.typeinfo.TypeInfo;

import java.io.Serializable;
import java.util.Map;

@TypeInfo(KafkaKeyValueTypeInfoFactory.class)
public class KafkaKeyValue extends KafkaKV<GenericRecord,GenericRecord> implements Serializable {
    private static final long serialVersionUID = 1;

    public KafkaKeyValue() {
        super();
    }

    private KafkaKeyValue(String topic, GenericRecord key, int partition, long offset, long timestamp, GenericRecord value) {
        super(topic, key, partition, offset, timestamp, value);
    }

    public static KafkaKeyValue create(String topic,
                                       GenericRecord key,
                                       GenericRecord value,
                                       int partition,
                                       long offset,
                                       long timestamp) {
        return new KafkaKeyValue(topic, key, partition, offset, timestamp, value);
    }

    public static Builder builder(KafkaKeyValue other) {
        return new Builder(other);
    }

    public enum EventType {
        CREATE_OR_UPDATE,
        DELETE
    }

    public static class Builder {
        private GenericRecord key;
        private GenericRecord value;
        private String topic;
        private int partition;
        private long offset;
        private long timestamp;
        private EventType eventType;
        private Map<String, String> metaData;

        public Builder(KafkaKeyValue other) {
            this.key = other.getKey();
            this.value = other.getValue();
            this.topic = other.getTopic();
            this.partition = other.getPartition();
            this.offset = other.getOffset();
            this.timestamp = other.getTimestamp();
        }

        public KafkaKeyValue build() {
            return new KafkaKeyValue(topic, key, partition, offset, timestamp, value);
        }

        public Builder withKey(GenericRecord key) {
            this.key = key;
            return this;
        }

        public Builder withValue(GenericRecord value) {
            this.value = value;
            return this;
        }

        public Builder withEventType(EventType eventType) {
            this.eventType = eventType;
            return this;
        }

        public Builder withMeta(Map<String, String> metaData) {
            this.metaData = metaData;
            return this;
        }

        public Builder withTopic(String topic) {
            this.topic = topic;
            return this;
        }

        public Builder withPartition(int partition) {
            this.partition = partition;
            return this;
        }

        public Builder withOffset(long offset) {
            this.offset = offset;
            return this;
        }

        public Builder withTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }
    }
}
