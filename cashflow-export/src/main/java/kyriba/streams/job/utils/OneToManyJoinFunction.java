/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;

import java.math.BigInteger;
import java.util.function.BiConsumer;

/**
 * @author M-VBE
 * @since 20.2
 */

//todo: Review the code to ensure it's working for entities which are updated/deleted/created
// todo: make it more generic if neecessary (replace tuple with class + factory method passed to constructor)
// todo: extract side output logic out of here
public class OneToManyJoinFunction<T extends GenericRecord, V extends GenericRecord> extends KeyedCoProcessFunction<BigInteger, T, V, Tuple2<T, V>> {
    private final Class<T> mainClass;
    private final Class<V> joinClass;

    private final BiConsumer<Context, ? super V> sideOutputProducer;

    private ListState<T> main;
    private ValueState<V> join;

    public OneToManyJoinFunction(Class<T> mainClass, Class<V> joinClass, BiConsumer<Context, ? super V> sideOutputProducer) {

        this.mainClass = mainClass;
        this.joinClass = joinClass;
        this.sideOutputProducer = sideOutputProducer;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        main = getRuntimeContext().getListState(new ListStateDescriptor<>("main", mainClass));
        join = getRuntimeContext().getState(new ValueStateDescriptor<>("join", joinClass));
    }


    @Override
    public void processElement1(T mainValue, Context ctx,
                                Collector<Tuple2<T, V>> out) throws Exception {
        V joinValue = join.value();
        if (joinValue != null) {
            out.collect(Tuple2.of(mainValue, joinValue));
        } else {
            main.add(mainValue);
        }
    }


    @Override
    public void processElement2(V joinValue, Context ctx,
                                Collector<Tuple2<T, V>> out) throws Exception {
        //flush main values
        final Iterable<T> mainValues = main.get();
        if (mainValues != null) {
            for (T mainValue : mainValues) {
                out.collect(Tuple2.of(mainValue, joinValue));
            }
            main.clear();
        }
        join.update(joinValue);

        sideOutputProducer.accept(ctx, joinValue);
    }
}
