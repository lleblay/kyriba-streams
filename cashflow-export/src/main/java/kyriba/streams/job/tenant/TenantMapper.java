/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 03.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.tenant;

import DataWarehouse.DimTenantForCreation;
import dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope;
import kyriba.streams.job.utils.MappingUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import java.time.Instant;
import java.util.UUID;

/**
 * @author M-VBE
 * @since 20.2
 */
class TenantMapper implements MapFunction<Envelope, Tuple2<DimTenantForCreation, Envelope>> {
    @Override
    public Tuple2<DimTenantForCreation, Envelope> map(Envelope value) {
        String interfacecode = value.getAfter().getINTERFACECODE();
        Long ddu = MappingUtils.toLongId(value.getAfter().getCUSTOMERID()).orElseThrow();
        return Tuple2.of(DimTenantForCreation.newBuilder()
                .setDWHTenantKey(UUID.nameUUIDFromBytes(ddu.toString().getBytes()).toString())
                .setTenantName(interfacecode)
                .setETLUpdateTS(Instant.now())
                .build(), value);
    }
}
