package kyriba.streams.job.tenant;


import DataWarehouse.DimTenantForCreation;
import DataWarehouse.DimTenantForCreationKey;
import DataWarehouse.FactCashFlows;
import dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope;
import kyriba.streams.job.StreamingJob;
import kyriba.streams.job.statedescriptors.StateDescriptors;
import kyriba.streams.job.utils.GenericKeyedBroadcastEnrichmentFunction;
import kyriba.streams.job.utils.KafkaKeyedSerializationSchemaWrapper;
import kyriba.streams.job.utils.OneToOneJoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroDeserializationSchema;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroSerializationSchema;
import org.apache.flink.formats.avro.typeutils.AvroTypeInfo;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.OutputTag;

import java.util.Properties;

import static kyriba.streams.job.utils.MappingUtils.toLongId;

public class TenantEnrichmentStreamProcessor {

    public static BroadcastStream<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, Envelope>> addTenantEnrichmentProcessor(Properties properties, StreamExecutionEnvironment env, String sourcePrefix, String targetPrefix) {

        // create flink consumer for corporate_customer topic
        FlinkKafkaConsumer<Envelope> corporateCustomerConsumer = new FlinkKafkaConsumer<>(sourcePrefix+".CORPORATE_CUSTOMER",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        corporateCustomerConsumer.setStartFromEarliest();

        // read corporate_custmer data stream and key it by interface code
        KeyedStream<Envelope, String> keyedCorporateCustomerStream = env
                .addSource(corporateCustomerConsumer, "CORPORATE_CUSTOMER TOPIC")
                .filter(value -> value.getAfter() != null) // We'll manage delete later ...
                .keyBy(corpCustEnvelope -> corpCustEnvelope.getAfter().getINTERFACECODE());

        // create flink consumer for DimTenant topic
        FlinkKafkaConsumer<dcusdemo.DEMO.DimTenant.Envelope> dimTenantConsumer = new FlinkKafkaConsumer<>(targetPrefix + ".DimTenant",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(dcusdemo.DEMO.DimTenant.Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        dimTenantConsumer.setStartFromEarliest();

        DataStream<dcusdemo.DEMO.DimTenant.Envelope> dimTenantDataStream = env.addSource(dimTenantConsumer, "DimTenant TOPIC")
                .filter(value -> value.getAfter() != null); // We'll manage delete later ...

        //todo: handle deletion
        KeyedStream<dcusdemo.DEMO.DimTenant.Envelope, String> keyedDimTenantDataStream = dimTenantDataStream
                .filter(value -> !value.getOp().equals("d") && value.getAfter().getTenantName() != null)
                .keyBy(dimTenantEnvelope -> dimTenantEnvelope.getAfter().getTenantName());


        final OutputTag<dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope> outputTag = new OutputTag<>("TenantCreation", new AvroTypeInfo<>(dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope.class));


        SingleOutputStreamOperator<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, Envelope>> joinedTuplesOfStreams = keyedDimTenantDataStream
                .connect(keyedCorporateCustomerStream)
                .process(new TenantJoinFunction(outputTag));

        // broadcast the tenants and create the broadcast state
        final BroadcastStream<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, Envelope>> tenantsBroadcastStream =
                joinedTuplesOfStreams.broadcast(StateDescriptors.tenantStateDescriptor);

        //define sink for created tenants
        FlinkKafkaProducer<Tuple2<DimTenantForCreationKey, DimTenantForCreation>> tenantOut = new FlinkKafkaProducer<>(
                "DimTenantOut",                  // target topic
                new KafkaKeyedSerializationSchemaWrapper<>(ConfluentRegistryAvroSerializationSchema.forSpecific(DimTenantForCreationKey.class, "DimTenantOut-key", StreamingJob.SCHEMA_REGISTRY_URL),
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimTenantForCreation.class, "DimTenantOut-value", StreamingJob.SCHEMA_REGISTRY_URL),
                        "DimTenantOut"),
                properties,
                FlinkKafkaProducer.Semantic.AT_LEAST_ONCE);

        //create new tenants using side output
        joinedTuplesOfStreams
                .getSideOutput(outputTag)
                .map(new TenantMapper())
                .map(new EntityKeyedRemapper())
                .addSink(tenantOut)
                .name("Tenant sink");


        return tenantsBroadcastStream;
    }

    //todo code duplication
    private static class TenantJoinFunction extends OneToOneJoinFunction<dcusdemo.DEMO.DimTenant.Envelope, Envelope> {
        public TenantJoinFunction(OutputTag<Envelope> outputTag) {
            super(dcusdemo.DEMO.DimTenant.Envelope.class, Envelope.class, (ctx, val) -> {
                if ("r".equals(val.getOp()) || "c".equals(val.getOp())) {
                    ctx.output(outputTag, val);
                }
            }, "Ten");
        }
    }

    public static class TenantEnrichmentFunction extends GenericKeyedBroadcastEnrichmentFunction<Long, Tuple2<dcusdemo.DEMO.CASH_FLOW.Envelope, FactCashFlows>, Tuple2<dcusdemo.DEMO.DimTenant.Envelope, Envelope>> {

        public TenantEnrichmentFunction() {
            super(new ValueStateDescriptor<>("TenantEnrichmentFunction", TypeInformation.of(new TypeHint<>() {
                    })), StateDescriptors.tenantStateDescriptor,
                    v -> toLongId(v.f1.getAfter().getCUSTOMERID()).orElseThrow(),
                    v -> toLongId(v.f0.getAfter().getDATADOMAINUID()).orElseThrow(),
                    (flow, tenant) -> {
                        flow.f1.setDWHTenantKey(tenant.f0.getAfter().getDWHTenantKey());
                        flow.f1.setTenantKey(tenant.f0.getAfter().getTenantKey());
                        return flow;
                    });
        }
    }


    private static class EntityKeyedRemapper implements MapFunction<Tuple2<DimTenantForCreation, Envelope>, Tuple2<DimTenantForCreationKey, DimTenantForCreation>> {
        @Override
        public Tuple2<DimTenantForCreationKey, DimTenantForCreation> map(Tuple2<DimTenantForCreation, Envelope> v) {
            final DimTenantForCreation accountForCreation = v.f0;
            final DimTenantForCreationKey accountForCreationKey = DimTenantForCreationKey.newBuilder()
                    .setTenantName(accountForCreation.getTenantName())
                    .build();
            return new Tuple2<>(accountForCreationKey, accountForCreation);
        }
    }
}