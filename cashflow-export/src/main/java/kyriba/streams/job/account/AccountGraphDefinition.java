/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.account;

import DataWarehouse.DimAccountForCreation;
import DataWarehouse.DimAccountForCreationKey;
import dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope;
import kyriba.streams.job.StreamingJob;
import kyriba.streams.job.statedescriptors.StateDescriptors;
import kyriba.streams.job.utils.GenericKeyedBroadcastEnrichmentFunction;
import kyriba.streams.job.utils.KafkaKeyedSerializationSchemaWrapper;
import kyriba.streams.job.utils.OneToOneJoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroDeserializationSchema;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroSerializationSchema;
import org.apache.flink.formats.avro.typeutils.AvroTypeInfo;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.OutputTag;

import java.util.Properties;

import static kyriba.streams.job.utils.MappingUtils.toLongId;

/**
 * @author M-VBE
 * @since 20.2
 */
public class AccountGraphDefinition {

    public static BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimAccount.Envelope, Envelope>> defineGraph(Properties properties,
                                                                                                            StreamExecutionEnvironment env,
                                                                                                            final BroadcastStream<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> tenantBroadcastStream, String sourceKey, String sourcePrefix, String targetPrefix) {
        FlinkKafkaConsumer<DATATEAM.DataWarehouse.DimAccount.Envelope> dimAccountKafkaConsumer = new FlinkKafkaConsumer<>(targetPrefix + ".DimAccount",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(DATATEAM.DataWarehouse.DimAccount.Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        dimAccountKafkaConsumer.setStartFromEarliest();
        DataStream<DATATEAM.DataWarehouse.DimAccount.Envelope> dimAccountStream = env.addSource(dimAccountKafkaConsumer, "DimAccount Topic")
                .filter(value -> value.getAfter() != null); // We'll manage delete later ...;


        FlinkKafkaConsumer<dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope> accuuntConsumer = new FlinkKafkaConsumer<>(sourcePrefix + ".COMPANY_BANK_ACCOUNT",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        accuuntConsumer.setStartFromEarliest();
        DataStream<dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope> accountTopic = env.addSource(accuuntConsumer, "COMPANY_BANK_ACCOUNT Topic")
                .filter(value -> value.getAfter() != null); // We'll manage delete later ...;


        final OutputTag<Envelope> outputTag = new OutputTag<>("AccountCreation", new AvroTypeInfo<>(Envelope.class));

        //join with dimAccount with Kapp's account
        final SingleOutputStreamOperator<Tuple2<DATATEAM.DataWarehouse.DimAccount.Envelope, Envelope>> processedStream = dimAccountStream
                .keyBy(acc -> acc.getAfter().getAccountAlternateKey())
                .connect(accountTopic.keyBy(value -> toLongId(value.getAfter().getCOMPANYACCOUNTID()).orElseThrow()))
                .process(new AccountJoinFunction(outputTag)); //the process function produces side output to the tag

        //broadcast processed items
        final BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimAccount.Envelope, Envelope>> broadcastStream = processedStream
                .broadcast(StateDescriptors.accountStateDescriptor);

        //define sink for created accounts
        FlinkKafkaProducer<Tuple2<DimAccountForCreationKey, DimAccountForCreation>> accountSink = new FlinkKafkaProducer<>(
                "DimAccountOut",                  // target topic
                new KafkaKeyedSerializationSchemaWrapper<>(
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimAccountForCreationKey.class, "DimAccountOut-key", StreamingJob.SCHEMA_REGISTRY_URL),
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimAccountForCreation.class, "DimAccountOut-value", StreamingJob.SCHEMA_REGISTRY_URL),
                        "DimAccountOut"
                ),    // serialization schema
                properties,
                FlinkKafkaProducer.Semantic.AT_LEAST_ONCE);

        //create new accounts using side output
        processedStream
                .getSideOutput(outputTag)
                .map(new AccountMapper(sourceKey))
                .keyBy(v -> toLongId(v.f1.getAfter().getCOMPANYACCOUNTID()).orElseThrow())
                .connect(tenantBroadcastStream)
                .process(new AccountTenantEnrichmentFunction())
                .map(new EntityKeyedRemapper())
                .addSink(accountSink)
                .name("Account sink");

        return broadcastStream;
    }


    private static class AccountJoinFunction extends OneToOneJoinFunction<DATATEAM.DataWarehouse.DimAccount.Envelope, Envelope> {
        public AccountJoinFunction(OutputTag<Envelope> outputTag) {
            super(DATATEAM.DataWarehouse.DimAccount.Envelope.class, Envelope.class, (ctx, val) -> {
                if ("r".equals(val.getOp()) || "c".equals(val.getOp())) {
                    ctx.output(outputTag, val);
                }
            }, "Acc");
        }

    }


    private static class AccountTenantEnrichmentFunction extends GenericKeyedBroadcastEnrichmentFunction<Long, Tuple2<DimAccountForCreation, Envelope>, Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> {
        public AccountTenantEnrichmentFunction() {
            super(
                    new ValueStateDescriptor<>("AccountTenantEnrichmentFunctionValue", TypeInformation.of(new TypeHint<>() {
                    })),
                    StateDescriptors.tenantStateDescriptor,
                    v -> toLongId(v.f1.getAfter().getCUSTOMERID()).orElseThrow(),
                    v -> toLongId(v.f1.getAfter().getDATADOMAINUID()).orElseThrow(),
                    (flows, tenant) -> {
                        String dwhTenantKey = tenant.f0.getAfter().getDWHTenantKey();
                        flows.f0.setDWHTenantKey(dwhTenantKey);
                        return flows;
                    });
        }
    }

    private static class EntityKeyedRemapper implements MapFunction<Tuple2<DimAccountForCreation, Envelope>, Tuple2<DimAccountForCreationKey, DimAccountForCreation>> {
        @Override
        public Tuple2<DimAccountForCreationKey, DimAccountForCreation> map(Tuple2<DimAccountForCreation, Envelope> v) {
            final DimAccountForCreation accountForCreation = v.f0;
            final DimAccountForCreationKey accountForCreationKey = DimAccountForCreationKey.newBuilder()
                    .setAccountAlternateKey(accountForCreation.getAccountAlternateKey())
                    .setDWHSourceKey(accountForCreation.getDWHSourceKey())
                    .setDWHTenantKey(accountForCreation.getDWHTenantKey())
                    .build();
            return new Tuple2<>(accountForCreationKey, accountForCreation);
        }
    }
}
