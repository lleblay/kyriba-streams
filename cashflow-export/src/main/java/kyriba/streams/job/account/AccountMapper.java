/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.account;

import DataWarehouse.DimAccountForCreation;
import dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope;
import kyriba.streams.job.utils.MappingUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import java.time.Instant;

/**
 * @author M-VBE
 * @since 20.2
 */
//todo: How to deal with fields encryption??!?
public class AccountMapper implements MapFunction<Envelope, Tuple2<DimAccountForCreation, Envelope>> {

    private final String sourceKey;

    public AccountMapper(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    @Override
    public Tuple2<DimAccountForCreation, Envelope> map(Envelope value) {
        final DimAccountForCreation accountRec = DimAccountForCreation.newBuilder()
                .setDWHSourceKey(sourceKey)
                .setAccountAlternateKey(MappingUtils.toLongId(value.getAfter().getCOMPANYACCOUNTID()).orElseThrow())
                .setIsDummy(false)
                .setAccountCode(value.getAfter().getACCOUNTCODE())
                .setAccountDescr1(value.getAfter().getACCOUNTDESCRPLAIN())
                .setAccountDescr2(value.getAfter().getACCOUNTDESCR2())
                .setETLUpdateTS(Instant.now())
                .build();
        return Tuple2.of(accountRec, value);
    }
}
