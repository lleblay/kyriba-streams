/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 11.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.currency;

import DATATEAM.DEMO.CURRENCY.Envelope;
import DataWarehouse.DimCurrencyOut;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import java.time.Instant;

/**
 * @author M-VBE
 * @since 20.2
 */
class CurrencyCreationMapper implements MapFunction<Envelope, Tuple2<DimCurrencyOut, Envelope>> {

    @Override
    public Tuple2<DimCurrencyOut, Envelope> map(Envelope value) throws Exception {
        final DimCurrencyOut currencyOut = DimCurrencyOut.newBuilder()
                .setDWHTenantKey(value.getAfter().getCURRENCYCODE())
                .setCurrencyCode(value.getAfter().getCURRENCYCODE())
                .setIsDummy(false)
                .setETLUpdateTS(Instant.now())
                .build();

        return Tuple2.of(currencyOut, value);
    }
}
