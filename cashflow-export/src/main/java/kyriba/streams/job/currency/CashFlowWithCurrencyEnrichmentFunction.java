/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 12.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.currency;

import DataWarehouse.FactCashFlows;
import dcusdemo.DEMO.CASH_FLOW.Envelope;
import kyriba.streams.job.cashflow.CashFlowProcessingFieldsHolder;
import kyriba.streams.job.statedescriptors.StateDescriptors;
import kyriba.streams.job.utils.MappingUtils;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

/**
 * @author M-VBE
 * @since 20.2
 */
public class CashFlowWithCurrencyEnrichmentFunction extends KeyedBroadcastProcessFunction<Long, Tuple2<Envelope, FactCashFlows>, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>, Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder>> {
    private ValueState<Tuple2<Envelope, FactCashFlows>> valueToUpdate;


    @Override
    public void open(Configuration parameters) {
        valueToUpdate = getRuntimeContext().getState(new ValueStateDescriptor<>("CashFlowWithCurrencyEnrichmentBroadcastFunction", TypeInformation.of(new TypeHint<>() {
        })));
    }

    @Override
    public void processElement(Tuple2<Envelope, FactCashFlows> value, ReadOnlyContext ctx, Collector<Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder>> out) throws Exception {
        final ReadOnlyBroadcastState<Long, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> broadcastState =
                ctx.getBroadcastState(StateDescriptors.currencyStateDescriptor);

        final Long accountCurrency = MappingUtils.toLongId(value.f0.getAfter().getACCOUNTCURRENCYID()).orElseThrow();
        final Long flowCurrency = MappingUtils.toLongId(value.f0.getAfter().getFLOWCURRENCYID()).orElseThrow();
        final Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> accountCurrencyTuple = broadcastState.get(accountCurrency);
        final Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> flowCurrencyTuple = broadcastState.get(flowCurrency);
        if (accountCurrencyTuple != null && flowCurrencyTuple != null) {
            pushToOut(value, out, accountCurrencyTuple, flowCurrencyTuple);
        } else {
            valueToUpdate.update(value);
            final long futureTimer = ctx.timerService().currentProcessingTime() + 1000;
            ctx.timerService().registerProcessingTimeTimer(futureTimer);
        }
    }


    @Override
    public void processBroadcastElement(Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> value, Context ctx, Collector<Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder>> out) throws Exception {
        BroadcastState<Long, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> bcState = ctx.getBroadcastState(StateDescriptors.currencyStateDescriptor);
        bcState.put(MappingUtils.toLongId(value.f1.getAfter().getCURRENCYID()).orElseThrow(), value);
    }


    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder>> out) throws Exception {
        final Tuple2<Envelope, FactCashFlows> in1Value = valueToUpdate.value();
        if (in1Value != null) {
            final ReadOnlyBroadcastState<Long, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> broadcastState = ctx.getBroadcastState(StateDescriptors.currencyStateDescriptor);

            final Long accountCurrency = MappingUtils.toLongId(in1Value.f0.getAfter().getACCOUNTCURRENCYID()).orElseThrow();
            final Long flowCurrency = MappingUtils.toLongId(in1Value.f0.getAfter().getFLOWCURRENCYID()).orElseThrow();
            final Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> accountCurrencyTuple = broadcastState.get(accountCurrency);
            final Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> flowCurrencyTuple = broadcastState.get(flowCurrency);

            if (accountCurrencyTuple != null && flowCurrencyTuple != null) {
                pushToOut(in1Value, out, accountCurrencyTuple, flowCurrencyTuple);
            } else {
                final long futureTimer = ctx.timerService().currentProcessingTime() + 1000;
                ctx.timerService().registerProcessingTimeTimer(futureTimer);
            }
        }
    }


    private void pushToOut(Tuple2<Envelope, FactCashFlows> value, Collector<Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder>> out, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> accountCurrencyTuple, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> flowCurrencyTuple) {
        valueToUpdate.clear();

        value.f1.setAccountCurrencyKey(accountCurrencyTuple.f0.getAfter().getCurrencyKey());
        value.f1.setCashFlowCurrencyKey(flowCurrencyTuple.f0.getAfter().getCurrencyKey());
        final CashFlowProcessingFieldsHolder cashFlowProcessingFieldsHolder = new CashFlowProcessingFieldsHolder();
        cashFlowProcessingFieldsHolder.accountCurrencyCode = accountCurrencyTuple.f0.getAfter().getCurrencyCode();
        cashFlowProcessingFieldsHolder.flowCurrencyCode = flowCurrencyTuple.f0.getAfter().getCurrencyCode();
        out.collect(Tuple3.of(value.f0, value.f1, cashFlowProcessingFieldsHolder));
    }
}
