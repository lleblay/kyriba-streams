/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 10.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.currency;

import DATATEAM.DEMO.CURRENCY.Envelope;
import DataWarehouse.DimCurrencyOut;
import DataWarehouse.DimCurrencyOutKey;
import kyriba.streams.job.StreamingJob;
import kyriba.streams.job.statedescriptors.StateDescriptors;
import kyriba.streams.job.utils.GenericKeyedBroadcastEnrichmentFunction;
import kyriba.streams.job.utils.KafkaKeyedSerializationSchemaWrapper;
import kyriba.streams.job.utils.MappingUtils;
import kyriba.streams.job.utils.OneToOneJoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroDeserializationSchema;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroSerializationSchema;
import org.apache.flink.formats.avro.typeutils.AvroTypeInfo;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.OutputTag;

import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.UUID;

import static kyriba.streams.job.utils.MappingUtils.toLongId;

/**
 * @author M-VBE
 * @since 20.2
 */
public class CurrencyGraphDefinition {
    public static BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> defineGraph(Properties properties,
                                                                                                                                    StreamExecutionEnvironment env,
                                                                                                                                    final BroadcastStream<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> tenantBroadcastStream, String sourcePrefix, String targetPrefix) {

        final FlinkKafkaConsumer<DATATEAM.DataWarehouse.DimCurrency.Envelope> dimCurrencyKafkaConsumer = new FlinkKafkaConsumer<>(targetPrefix + ".DimCurrency",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(DATATEAM.DataWarehouse.DimCurrency.Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        dimCurrencyKafkaConsumer.setStartFromEarliest();
        DataStream<DATATEAM.DataWarehouse.DimCurrency.Envelope> dimCurrencyTopic = env.addSource(dimCurrencyKafkaConsumer, "DimCurrency Topic")
                .filter(value -> value.getAfter() != null); // We'll manage delete later ...;


        final FlinkKafkaConsumer<Envelope> currencyConsumer = new FlinkKafkaConsumer<>(sourcePrefix + ".CURRENCY",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        currencyConsumer.setStartFromEarliest();
        DataStream<Envelope> currencyTopic = env.addSource(currencyConsumer, "CURRENCY Topic")
                .filter(value -> value.getAfter() != null); // We'll manage delete later ...;


        final OutputTag<Envelope> outputTag =
                new OutputTag<>("CurrencyTag", new AvroTypeInfo<>(Envelope.class));


        //join with dimAccount with Kapp's account
        final SingleOutputStreamOperator<Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> processedStream = dimCurrencyTopic
                .keyBy(new KeySelector<DATATEAM.DataWarehouse.DimCurrency.Envelope, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> getKey(DATATEAM.DataWarehouse.DimCurrency.Envelope acc) throws Exception {
                        return Tuple2.of(acc.getAfter().getCurrencyCode(), acc.getAfter().getDWHTenantKey());
                    }
                })
                .connect(currencyTopic.keyBy(new KeySelector<Envelope, Tuple2<String, String>>() {
                    @Override
                    public Tuple2<String, String> getKey(Envelope value) throws Exception {
                        Long ddu = toLongId(value.getAfter().getDATADOMAINUID()).orElseThrow();
                        return Tuple2.of(value.getAfter().getCURRENCYCODE(), UUID.nameUUIDFromBytes(ddu.toString().getBytes()).toString());
                    }
                }))
                .process(new CurrencyJoinFunction(outputTag)); //the process function produces side output to the tag


        //broadcast processed items
        final BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> broadcastStream = processedStream
                .broadcast(StateDescriptors.currencyStateDescriptor);

        //define sink for created accounts
        FlinkKafkaProducer<Tuple2<DimCurrencyOutKey, DimCurrencyOut>> currencySink = new FlinkKafkaProducer<>(
                "DimCurrencyOut",                  // target topic
                new KafkaKeyedSerializationSchemaWrapper<>(
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimCurrencyOutKey.class, "DimCurrencyOut-key", StreamingJob.SCHEMA_REGISTRY_URL),
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimCurrencyOut.class, "DimCurrencyOut-value", StreamingJob.SCHEMA_REGISTRY_URL),
                        "DimCurrencyOut"
                ),    // serialization schema
                properties,
                FlinkKafkaProducer.Semantic.AT_LEAST_ONCE);

        //create new accounts using side output
        processedStream
                .getSideOutput(outputTag)
                .map(new CurrencyCreationMapper())
                .name("Currency Mapper")
                .keyBy(v -> toLongId(v.f1.getAfter().getCURRENCYID()).orElseThrow())
                .connect(tenantBroadcastStream)
                .process(new CurrencyTenantEnrichmentFunction())
                .name("Currency join")
                .map(new EntityKeyedRemapper())
                .name("Currency Remapper")
                .addSink(currencySink)
                .name("Currency sink");

        return broadcastStream;

    }


    private static class CurrencyJoinFunction extends OneToOneJoinFunction<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope> {
        public CurrencyJoinFunction(OutputTag<DATATEAM.DEMO.CURRENCY.Envelope> outputTag) {
            super(DATATEAM.DataWarehouse.DimCurrency.Envelope.class, DATATEAM.DEMO.CURRENCY.Envelope.class, (ctx, val) -> {
                if ("r".equals(val.getOp()) || "c".equals(val.getOp())) {
                    ctx.output(outputTag, val);
                }
            }, "Currency");
        }

    }


    private static class CurrencyTenantEnrichmentFunction extends GenericKeyedBroadcastEnrichmentFunction<Long, Tuple2<DimCurrencyOut, DATATEAM.DEMO.CURRENCY.Envelope>, Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> {
        public CurrencyTenantEnrichmentFunction() {
            super(
                    new ValueStateDescriptor<>("CurrencyTenantEnrichmentFunction", TypeInformation.of(new TypeHint<>() {
                    })),
                    StateDescriptors.tenantStateDescriptor,
                    v -> toLongId(v.f1.getAfter().getCUSTOMERID()).orElseThrow(),
                    v -> toLongId(v.f1.getAfter().getDATADOMAINUID()).orElseThrow(),
                    (flows, tenant) -> {
                        String dwhTenantKey = tenant.f0.getAfter().getDWHTenantKey();
                        flows.f0.setDWHTenantKey(dwhTenantKey);
                        return flows;
                    });
        }
    }


    private static class EntityKeyedRemapper implements MapFunction<Tuple2<DimCurrencyOut, DATATEAM.DEMO.CURRENCY.Envelope>, Tuple2<DimCurrencyOutKey, DimCurrencyOut>> {
        @Override
        public Tuple2<DimCurrencyOutKey, DimCurrencyOut> map(Tuple2<DimCurrencyOut, DATATEAM.DEMO.CURRENCY.Envelope> v) {
            final DimCurrencyOut currencyOut = v.f0;
            final DimCurrencyOutKey accountForCreationKey = DimCurrencyOutKey.newBuilder()
                    .setCurrencyCode(currencyOut.getCurrencyCode())
                    .setDWHTenantKey(currencyOut.getDWHTenantKey())
                    .build();
            return new Tuple2<>(accountForCreationKey, currencyOut);
        }
    }


}
