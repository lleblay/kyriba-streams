/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 01.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.statedescriptors;

import dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * @author M-VBE
 * @since 20.2
 */
public class StateDescriptors {

    public static final MapStateDescriptor<Long, Tuple2<dcusdemo.DEMO.DimTenant.Envelope, Envelope>> tenantStateDescriptor = new MapStateDescriptor<>(
            "TenantBroadcastState",
            BasicTypeInfo.LONG_TYPE_INFO,
            TypeInformation.of(new TypeHint<>() {
            }));


    public static final MapStateDescriptor<Long, Tuple2<DATATEAM.DataWarehouse.DimAccount.Envelope, dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope>> accountStateDescriptor =
            new MapStateDescriptor<>("DimAccountBroadcasrDescriptor",
                    BasicTypeInfo.LONG_TYPE_INFO,
                    TypeInformation.of(new TypeHint<>() {
                    })
            );


    public static final MapStateDescriptor<Long, Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> currencyStateDescriptor =
            new MapStateDescriptor<>("DimCurrencyBroadcasterDescriptor",
                    BasicTypeInfo.LONG_TYPE_INFO,
                    TypeInformation.of(new TypeHint<>() {
                    })
            );


    public static final MapStateDescriptor<Long, Tuple2<DATATEAM.DataWarehouse.DimFlowCode.Envelope, dcusdemo.DEMO.CASH_FLOW_CODE.Envelope>> dimCashFlowCodeBroadcaster =
            new MapStateDescriptor<>("DimCashFlowCodeBroadcaster",
                    BasicTypeInfo.LONG_TYPE_INFO,
                    TypeInformation.of(new TypeHint<>() {
                    })
            );

}
