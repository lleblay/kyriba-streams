/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.cashflowcode;

import DataWarehouse.DimFlowCodeForCreation;
import dcusdemo.DEMO.CASH_FLOW_CODE.Envelope;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static kyriba.streams.job.utils.MappingUtils.*;

/**
 * @author M-VBE
 * @since 20.2
 */
public class CashFlowCodeMapper implements MapFunction<Envelope, Tuple2<DimFlowCodeForCreation, dcusdemo.DEMO.CASH_FLOW_CODE.Envelope>> {
    @Override
    public Tuple2<DimFlowCodeForCreation, dcusdemo.DEMO.CASH_FLOW_CODE.Envelope> map(Envelope value) throws Exception {
        final dcusdemo.DEMO.CASH_FLOW_CODE.Value after = value.getAfter();
        final DimFlowCodeForCreation flowCode = DimFlowCodeForCreation.newBuilder()
                .setDWHTenantKey(UUID.randomUUID().toString())
                .setFlowCode(after.getCASHFLOWCODE())
                .setFlowCodeDescription(after.getCASHFLOWDESCR())
                .setFlowCodeType(FlowCodeType.fromCode(toId(after.getCASHFLOWTYPEID()).map(BigInteger::intValue).orElseThrow()).map(FlowCodeType::getExportCode).orElseThrow())
                .setIsFlowCodeIncludedinBalanceCalculation(toBoolean(after.getBALANCEFLG()))
                .setIsFlowCodeIncludedinTurnoverCalculation(toBoolean(after.getTURNOVERFLG()))
                .setIsBudgetCodeMandatoryForThisCashFlowCode(toBoolean(after.getBUDGETCODEMANDATORY()))
                .setIsFlowCodeHiddeninList(toBoolean(after.getHIDDEN()))
                //todo: is it really needed?
                .setFlowCodeBudgetUID(toId(after.getBUDGETCODEID()).map(BigInteger::toString).orElse(null))
                .setFlowCodeUID(toLongId(after.getCASHFLOWCODEID()).orElse(null))
                //todo: do we need these values?
                .setFlowCodeBudgetCode("")
                .setFlowCodeGroupingCode("")
                .setFlowCodeCounterpartyFlowCode("")
                //todo: internal key
                .setFlowCodeGroupingUID(toId(after.getGROUPINGCODEID()).map(BigInteger::toString).orElse(null))
                .setFlowCodeCounterpartyFlowUID(toId(after.getFLOWCODEID()).map(BigInteger::toString).orElse(null))
                .setETLUpdateTS(Instant.now())
                .build();
        return Tuple2.of(flowCode, value);
    }


    public enum FlowCodeType {
        COLLECTION(1001, "Collection"),
        COLLECTION_ON_EXPORT(1002, "Collection on export"),
        COLLECTION_ON_DRAFT(1003, "Collection on draft"),
        DISCOUNT_ON_ACCOUNT_REMITTANCE(1004, "Discount on account remittance"),
        COLLECTION_ON_DECISION(1005, "Collection on decision"),
        COLLECTION_ON_BALANCING(1006, "Collection on balancing"),
        DISBURSEMENT(7, "Disbursement"),
        DISBURSEMENT_ON_IMPORT(8, "Disbursement on import"),
        DISBURSEMENT_ON_DECISION(9, "Disbursement on decision"),
        DISBURSEMENT_ON_BALANCING(10, "Disbursement on balancing"),
        MANAGEMENT_FEES(11, "Management fees"),
        ADJUSTMENT(1012, "Adjustment"),
        BALANCE_INITIALIZATION(1013, "Balance initialization"),
        ONE_DAY_FLOAT(15, "One day float"),
        TWO_DAY_FLOAT(16, "Two day float"),
        THREE_DAY_FLOAT(17, "Three day float"),
        INVESTMENT_POSITION(1018, "Investment position");

        private final int code;
        private final String exportCode;

        FlowCodeType(int code, String exportCode) {

            this.code = code;
            this.exportCode = exportCode;
        }


        public int getCode() {
            return code;
        }

        public String getExportCode() {
            return exportCode;
        }

        public static Optional<FlowCodeType> fromCode(Integer code) {
            if (code == null) {
                return Optional.empty();
            }
            for (FlowCodeType v : FlowCodeType.values()) {
                if (v.code == code) {
                    return Optional.of(v);
                }
            }
            return Optional.empty();
        }
    }
}
