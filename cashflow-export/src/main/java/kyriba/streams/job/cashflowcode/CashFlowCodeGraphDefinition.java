/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.cashflowcode;

import DataWarehouse.DimFlowCodeForCreation;
import DataWarehouse.DimFlowCodeForCreationKey;
import dcusdemo.DEMO.CASH_FLOW_CODE.Envelope;
import kyriba.streams.job.StreamingJob;
import kyriba.streams.job.statedescriptors.StateDescriptors;
import kyriba.streams.job.utils.GenericKeyedBroadcastEnrichmentFunction;
import kyriba.streams.job.utils.KafkaKeyedSerializationSchemaWrapper;
import kyriba.streams.job.utils.OneToOneJoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroDeserializationSchema;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroSerializationSchema;
import org.apache.flink.formats.avro.typeutils.AvroTypeInfo;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.OutputTag;

import java.util.Properties;

import static kyriba.streams.job.utils.MappingUtils.toLongId;

/**
 * @author M-VBE
 * @since 20.2
 */
public class CashFlowCodeGraphDefinition {

    public static BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimFlowCode.Envelope, Envelope>> defineGraph(Properties properties,
                                                                                                             StreamExecutionEnvironment env,
                                                                                                             final BroadcastStream<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> tenantBroadcastStream, String sourcePrefix, String targetPrefix) {


        final FlinkKafkaConsumer<DATATEAM.DataWarehouse.DimFlowCode.Envelope> dimAccountKafkaConsumer = new FlinkKafkaConsumer<>(targetPrefix+".DimFlowCode",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(DATATEAM.DataWarehouse.DimFlowCode.Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        dimAccountKafkaConsumer.setStartFromEarliest();
        DataStream<DATATEAM.DataWarehouse.DimFlowCode.Envelope> dimFlowCodeStream = env.addSource(dimAccountKafkaConsumer, "DimFlowCode Topic").filter(value -> value.getAfter() != null); // We'll manage delete later ...;


        final FlinkKafkaConsumer<Envelope> accuuntConsumer = new FlinkKafkaConsumer<>(sourcePrefix+".CASH_FLOW_CODE",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(Envelope.class, StreamingJob.SCHEMA_REGISTRY_URL), properties);
        accuuntConsumer.setStartFromEarliest();
        DataStream<Envelope> flowCodeTopic = env.addSource(accuuntConsumer, "CASH_FLOW_CODE Topic").filter(value -> value.getAfter() != null); // We'll manage delete later ...;


        final OutputTag<Envelope> outputTag =
                new OutputTag<>("CashFlowCodeCreation", new AvroTypeInfo<>(Envelope.class));


        //match Dim and KApp tables, add side output, and value broadcast for cash flow mapping 
        final SingleOutputStreamOperator<Tuple2<DATATEAM.DataWarehouse.DimFlowCode.Envelope, Envelope>> process = dimFlowCodeStream
                .filter(v -> v.getAfter().getFlowCodeUID() != null)
                .keyBy(v -> v.getAfter().getFlowCodeUID())
                .connect(flowCodeTopic.keyBy(v -> toLongId(v.getAfter().getCASHFLOWCODEID()).orElseThrow()))
                .process(new CashFlowCodeJoinFunction(outputTag));

        final BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimFlowCode.Envelope, Envelope>> broadcast = process
                .broadcast(StateDescriptors.dimCashFlowCodeBroadcaster);

        final FlinkKafkaProducer<Tuple2<DimFlowCodeForCreationKey, DimFlowCodeForCreation>> myProducer = new FlinkKafkaProducer<>(
                "DimFlowCodeOut",
                new KafkaKeyedSerializationSchemaWrapper<>(
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimFlowCodeForCreationKey.class, "DimFlowCodeOut-key", StreamingJob.SCHEMA_REGISTRY_URL),
                        ConfluentRegistryAvroSerializationSchema.forSpecific(DimFlowCodeForCreation.class, "DimFlowCodeOut-value", StreamingJob.SCHEMA_REGISTRY_URL),
                        "DimFlowCodeOut"),
                properties,
                FlinkKafkaProducer.Semantic.AT_LEAST_ONCE);

        process.getSideOutput(outputTag)
                .map(new CashFlowCodeMapper())
                .keyBy(v -> toLongId(v.f1.getAfter().getCASHFLOWCODEID()).orElseThrow())
                .connect(tenantBroadcastStream)
                .process(new CashFlowCodeTenantEnricher())
                .map(new EntityKeyRemapper())
                .addSink(myProducer)
                .name("CashFlowCode to DimFlowCode");

        return broadcast;
    }


    private static class CashFlowCodeJoinFunction extends OneToOneJoinFunction<DATATEAM.DataWarehouse.DimFlowCode.Envelope, Envelope> {

        public CashFlowCodeJoinFunction(OutputTag<Envelope> outputTag) {
            super(DATATEAM.DataWarehouse.DimFlowCode.Envelope.class, Envelope.class, (ctx, val) -> {
                if ("r".equals(val.getOp()) || "c".equals(val.getOp())) {
                    ctx.output(outputTag, val);
                }
            }, "CFC");
        }
    }

    private static class CashFlowCodeTenantEnricher extends GenericKeyedBroadcastEnrichmentFunction<Long, Tuple2<DimFlowCodeForCreation, Envelope>, Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> {
        public CashFlowCodeTenantEnricher() {
            super(new ValueStateDescriptor<>("CashFlowCodeTenantEnricherMapState", TypeInformation.of(new TypeHint<>() {
                    })), StateDescriptors.tenantStateDescriptor,
                    v -> toLongId(v.f1.getAfter().getCUSTOMERID()).orElseThrow(),
                    v -> toLongId(v.f1.getAfter().getDATADOMAINUID()).orElseThrow(),
                    (flowCodes, tenant) -> {
                        String dwhTenantKey = tenant.f0.getAfter().getDWHTenantKey();
                        flowCodes.f0.setDWHTenantKey(dwhTenantKey);
                        return flowCodes;
                    });
        }
    }

    private static class EntityKeyRemapper implements MapFunction<Tuple2<DimFlowCodeForCreation, Envelope>, Tuple2<DimFlowCodeForCreationKey, DimFlowCodeForCreation>> {

        @Override
        public Tuple2<DimFlowCodeForCreationKey, DimFlowCodeForCreation> map(Tuple2<DimFlowCodeForCreation, Envelope> v) {
            final DimFlowCodeForCreation entityForCreation = v.f0;
            final DimFlowCodeForCreationKey accountForCreationKey = DimFlowCodeForCreationKey.newBuilder()
                    .setFlowCodeUID(entityForCreation.getFlowCodeUID())
                    .setDWHTenantKey(entityForCreation.getDWHTenantKey())
                    .build();
            return new Tuple2<>(accountForCreationKey, entityForCreation);
        }
    }
}
