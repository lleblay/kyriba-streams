/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 24.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.cache;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.RequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.time.Duration;

import static kyriba.streams.job.StreamingJob.CURRENCY_CONVERTER_SERVICE;


/**
 * @author M-VBE
 * @since 20.2
 */
public class CurrencyCache implements Serializable {
    public static final Logger LOG = LoggerFactory.getLogger(CurrencyCache.class);

    //todo: handle exceptions
    public transient static final AsyncLoadingCache<String, Double> CACHE = Caffeine.newBuilder()
            .expireAfterWrite(Duration.ofMinutes(20))
            .maximumSize(10_000L)
            .buildAsync((k, executor) -> Dsl.asyncHttpClient(Dsl.config()).prepareRequest(new RequestBuilder("GET")
                    .setUrl(CURRENCY_CONVERTER_SERVICE + "/" + k)
                    .build())
                    .execute()
                    .toCompletableFuture()
                    .thenApplyAsync(response -> {
                        if (response.hasResponseStatus() && response.hasResponseBody() && response.getStatusCode() == 200) {
                            return Double.parseDouble(response.getResponseBody());
                        }
                        LOG.warn("URI: {}, Error: {}", response.getUri(), response.getResponseBody());
                        return 1.0; //todo: This shouldn't be done this way, but just to deme purpose
                    }));

}
