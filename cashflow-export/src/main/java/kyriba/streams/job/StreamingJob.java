/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kyriba.streams.job;

import DataWarehouse.FactCashFlows;
import DataWarehouse.FactCashFlowsKey;
import dcusdemo.DEMO.CASH_FLOW.Envelope;
import kyriba.streams.job.account.AccountGraphDefinition;
import kyriba.streams.job.cache.CurrencyCache;
import kyriba.streams.job.cashflow.CashFlowMapper;
import kyriba.streams.job.cashflow.CashFlowProcessingFieldsHolder;
import kyriba.streams.job.cashflowcode.CashFlowCodeGraphDefinition;
import kyriba.streams.job.currency.CashFlowWithCurrencyEnrichmentFunction;
import kyriba.streams.job.currency.CurrencyGraphDefinition;
import kyriba.streams.job.statedescriptors.StateDescriptors;
import kyriba.streams.job.tenant.TenantEnrichmentStreamProcessor;
import kyriba.streams.job.utils.GenericKeyedBroadcastEnrichmentFunction;
import kyriba.streams.job.utils.KafkaKeyedSerializationSchemaWrapper;
import kyriba.streams.job.utils.MappingUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroDeserializationSchema;
import org.apache.flink.formats.avro.registry.confluent.ConfluentRegistryAvroSerializationSchema;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static kyriba.streams.job.utils.MappingUtils.toLongId;
import static org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer.KEY_POLL_TIMEOUT;

/**
 * Skeleton for a Flink Streaming Job.
 *
 * <p>For a tutorial how to write a Flink streaming application, check the
 * tutorials and examples on the <a href="https://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution, run
 * 'mvn clean package' on the command line.
 *
 * <p>If you change the name of the main class (with the public static void main(String[] args))
 * method, change the respective entry in the POM.xml file (simply search for 'mainClass').
 */
public class StreamingJob {

    public static final String SCHEMA_REGISTRY_URL = "http://schema-registry.data.k8s.kod.kyriba.com";
    public static final String CURRENCY_CONVERTER_SERVICE = "http://cashflow-export.data.k8s.kod.kyriba.com/marketdata";
    public static Logger LOG = LoggerFactory.getLogger(StreamingJob.class);

    public static void main(String[] args) throws Exception {

        String sourcePrefix = "DATATEAM.DEMO";
        String targetPrefix = "DATATEAM.DataWarehouse2";
        final StreamExecutionEnvironment env = getStreamExecutionEnvironment(sourcePrefix, targetPrefix, "Test", 1);

        // execute program
        env.execute("Flink Streaming Java API Skeleton");
    }

    public static StreamExecutionEnvironment getStreamExecutionEnvironment(String sourcePrefix, String targetPrefix, String sourceKey, int paralellism) {
        // set up the streaming execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(paralellism);
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "kafka.data.k8s.kod.kyriba.com:9092");
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.setProperty(KEY_POLL_TIMEOUT, "1000");
        properties.setProperty("group.id", "test-5");

        FlinkKafkaConsumer<Envelope> cashFlowConsumer = new FlinkKafkaConsumer<>(sourcePrefix+".CASH_FLOW",
                ConfluentRegistryAvroDeserializationSchema.forSpecific(Envelope.class, SCHEMA_REGISTRY_URL), properties);
        cashFlowConsumer.setStartFromEarliest();
        DataStream<Envelope> cashFlowTopic = env.addSource(cashFlowConsumer, "CASH_FLOW Topic");


        //process tenants
        final BroadcastStream<Tuple2<dcusdemo.DEMO.DimTenant.Envelope, dcusdemo.DEMO.CORPORATE_CUSTOMER.Envelope>> tenantBroadcastStream =
                TenantEnrichmentStreamProcessor.addTenantEnrichmentProcessor(properties, env, sourcePrefix, targetPrefix);

        //process accounts
        final BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimAccount.Envelope, dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope>> accountBroadcastStream =
                AccountGraphDefinition.defineGraph(properties, env, tenantBroadcastStream, sourceKey, sourcePrefix, targetPrefix);

        //process flow codes 
        final BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimFlowCode.Envelope, dcusdemo.DEMO.CASH_FLOW_CODE.Envelope>> cashFlowCodeBroadcastStream =
                CashFlowCodeGraphDefinition.defineGraph(properties, env, tenantBroadcastStream, sourcePrefix, targetPrefix);

        //process currencies 
        final BroadcastStream<Tuple2<DATATEAM.DataWarehouse.DimCurrency.Envelope, DATATEAM.DEMO.CURRENCY.Envelope>> currencyBroadcastStream =
                CurrencyGraphDefinition.defineGraph(properties, env, tenantBroadcastStream, sourcePrefix, targetPrefix);


        //todo: use DDU field in join - keyBy using tuple?
        //todo: error handling
        //todo: does every message has getAfter() populated (deletion?)?

        //map fields to cash flows
        SingleOutputStreamOperator<Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder>> currencyConversionToBeApplied = cashFlowTopic
                .map(new CashFlowMapper(sourceKey))
                .name("Map direct cash flows fields")
                .keyBy(v -> toLongId(v.f0.getAfter().getCASHFLOWID()).orElseThrow())
                .connect(tenantBroadcastStream)
                .process(new TenantEnrichmentStreamProcessor.TenantEnrichmentFunction())
                .name("Map TENANT fields to CASH FLOW")
                .keyBy(v -> toLongId(v.f0.getAfter().getCASHFLOWID()).orElseThrow())
                .connect(accountBroadcastStream)
                .process(new CashFlowWithAccountEnricher())
                .name("Map ACCOUNT fields to CASH FLOWS")
                .keyBy(v -> toLongId(v.f0.getAfter().getCASHFLOWID()).orElseThrow())
                .connect(cashFlowCodeBroadcastStream)
                .process(new CashFlowWithCashFlowCodeEnrichmentFunction())
                .name("Map FLOW CODE fields to CASH FLOW")
                .keyBy(v -> toLongId(v.f0.getAfter().getCASHFLOWID()).orElseThrow())
                .connect(currencyBroadcastStream)
                .process(new CashFlowWithCurrencyEnrichmentFunction())
                .name("Map CURRENCY fields to CASH FLOW");

        final SingleOutputStreamOperator<Tuple2<Envelope, FactCashFlows>> mappedCashFlows = AsyncDataStream.unorderedWait(
                currencyConversionToBeApplied.keyBy(v -> toLongId(v.f0.getAfter().getCASHFLOWID()).orElseThrow()), new RichAsyncFunction<>() {

                    @Override
                    public void asyncInvoke(Tuple3<Envelope, FactCashFlows, CashFlowProcessingFieldsHolder> input, ResultFuture<Tuple2<Envelope, FactCashFlows>> resultFuture) throws Exception {
                        final FactCashFlows f1 = input.f1;
                        f1.setAccountCurCashFlowRate(1.00);
                        resultFuture.complete(List.of(Tuple2.of(input.f0, input.f1)));
                        /*if (input.f2.flowCurrencyCode.equals(input.f2.accountCurrencyCode)) {
                            final FactCashFlows f1 = input.f1;
                            f1.setAccountCurCashFlowRate(1.00);
                            resultFuture.complete(List.of(Tuple2.of(input.f0, input.f1)));
                        } else {
                            CurrencyCache.CACHE.get(input.f2.getCurrencyConversionString(MappingUtils.toLocalDate(input.f0.getAfter().getAVAILABILITYDATE())))
                                    .thenAccept(currencyRate -> {
                                        final FactCashFlows f1 = input.f1;
                                        f1.setAccountCurCashFlowRate(currencyRate);
                                        resultFuture.complete(List.of(Tuple2.of(input.f0, input.f1)));
                                    });
                            //todo: handle exceptions
                        }*/

                    }
                }, 200000, TimeUnit.MILLISECONDS);


        //define sink for created cash flows
        FlinkKafkaProducer<Tuple2<FactCashFlowsKey, FactCashFlows>> cashFlowsSink = new FlinkKafkaProducer<>(
                "FactCashFlowsOut",                  // target topic
                new KafkaKeyedSerializationSchemaWrapper<>(
                        ConfluentRegistryAvroSerializationSchema.forSpecific(FactCashFlowsKey.class, "FactCashFlowsOut-key", SCHEMA_REGISTRY_URL),
                        ConfluentRegistryAvroSerializationSchema.forSpecific(FactCashFlows.class, "FactCashFlowsOut-value", SCHEMA_REGISTRY_URL),
                        "FactCashFlowsOut"),
                properties,
                FlinkKafkaProducer.Semantic.AT_LEAST_ONCE);


        mappedCashFlows
                .map(new EntityKeyRemapper())
                .addSink(cashFlowsSink)
                .name("Cash flow sink");

        return env;
    }


    private static class CashFlowWithAccountEnricher extends GenericKeyedBroadcastEnrichmentFunction<Long, Tuple2<Envelope, FactCashFlows>, Tuple2<DATATEAM.DataWarehouse.DimAccount.Envelope, dcusdemo.DEMO.COMPANY_BANK_ACCOUNT.Envelope>> {

        public CashFlowWithAccountEnricher() {
            super(new ValueStateDescriptor<>("CashFlowWithAccountEnricher", TypeInformation.of(new TypeHint<>() {
                    })), StateDescriptors.accountStateDescriptor,
                    v -> toLongId(v.f1.getAfter().getCOMPANYACCOUNTID()).orElseThrow(),
                    v -> toLongId(v.f0.getAfter().getCOMPANYACCOUNTID()).orElseThrow(),
                    (flows, accounts) -> {
                        flows.f1.setAccountKey(accounts.f0.getAfter().getAccountKey());
                        return flows;
                    });
        }
    }


    private static class CashFlowWithCashFlowCodeEnrichmentFunction extends GenericKeyedBroadcastEnrichmentFunction<Long, Tuple2<Envelope, FactCashFlows>, Tuple2<DATATEAM.DataWarehouse.DimFlowCode.Envelope, dcusdemo.DEMO.CASH_FLOW_CODE.Envelope>> {
        public CashFlowWithCashFlowCodeEnrichmentFunction() {
            super(new ValueStateDescriptor<>("CashFlowWithCashFlowCodeEnrichmentFunctionValue", TypeInformation.of(new TypeHint<>() {
                    })), StateDescriptors.dimCashFlowCodeBroadcaster,
                    v -> toLongId(v.f1.getAfter().getCASHFLOWCODEID()).orElseThrow(),
                    v -> toLongId(v.f0.getAfter().getCASHFLOWCODEID()).orElseThrow(),
                    (flows, codes) -> {
                        flows.f1.setFlowCodeKey(codes.f0.getAfter().getFlowCodeKey());
                        flows.f1.setFlowCodeType(codes.f0.getAfter().getFlowCodeType());
                        return flows;
                    });
        }

    }

    private static class EntityKeyRemapper implements MapFunction<Tuple2<Envelope, FactCashFlows>, Tuple2<FactCashFlowsKey, FactCashFlows>> {


        @Override
        public Tuple2<FactCashFlowsKey, FactCashFlows> map(Tuple2<Envelope, FactCashFlows> v) throws Exception {
            final FactCashFlows entityForCreation = v.f1;
            final FactCashFlowsKey accountForCreationKey = FactCashFlowsKey.newBuilder()
                    .setDDCashFlowAlternateKey(entityForCreation.getDDCashFlowAlternateKey())
                    .setDWHTenantKey(entityForCreation.getDWHTenantKey())
                    .setDWHSourceKey(entityForCreation.getDWHSourceKey())
                    .build();
            return new Tuple2<>(accountForCreationKey, entityForCreation);
        }

    }
}
