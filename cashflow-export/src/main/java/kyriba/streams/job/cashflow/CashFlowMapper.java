/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 26.10.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.cashflow;

import DataWarehouse.FactCashFlows;
import dcusdemo.DEMO.CASH_FLOW.Envelope;
import dcusdemo.DEMO.CASH_FLOW.Value;
import kyriba.streams.job.utils.MappingUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.UUID;

import static kyriba.streams.job.utils.MappingUtils.toId;
import static kyriba.streams.job.utils.MappingUtils.toLongId;

/**
 * @author M-VBE
 * @since 20.2
 */
//todo: time zones? currently take UTC timezone as it is in DB
public final class CashFlowMapper implements MapFunction<Envelope, Tuple2<Envelope, FactCashFlows>> {

    public static final int DESCRIPTION_MAX_SIZE = 200;

    static final String ORIGIN_PY_ERP = "PY/ERP";
    static final String ORIGIN_PY_CAMT054 = "PY/CAMT054";
    static final String ORIGIN_CP = "CP";
    static final String ORIGIN_CASH_INTEREST_OLD = "Cash/Interest";
    static final String ORIGIN_BI_ACCRUAL = "Accrual/BI";
    static final String ORIGIN_FX_RESULT = "Result/FX";

    private final String sourceKey;

    public CashFlowMapper(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    @Override
    public Tuple2<Envelope, FactCashFlows> map(Envelope value) {
        final Value after = value.getAfter();
        // TODO: quick fix
        final LocalDateTime updateDate = toLocalDateTime(after.getUPDATEDATE());
        final LocalDate availabilityDate = MappingUtils.toLocalDate(after.getAVAILABILITYDATE());
        final LocalDateTime creationDate = toLocalDateTime(after.getCREATIONDATE());
        final LocalDate accountingDate = MappingUtils.toLocalDate(after.getACCOUNTINGDATE());
        final LocalDate endDate = after.getENDDATE() != null ? MappingUtils.toLocalDate(after.getENDDATE()) : null;
        final LocalDate transactionDate = MappingUtils.toLocalDate(after.getTRANSACTIONDATE());

        final FactCashFlows mappedFactCashFlow = FactCashFlows.newBuilder()
                .setDWHTenantKey(UUID.randomUUID().toString()) //todo: find more appropriate way
                .setDWHSourceKey(sourceKey)
                .setDataDomainUid(toLongId(value.getAfter().getDATADOMAINUID()).map(String::valueOf).orElse(null))
                .setDDCashFlowAlternateKey(toLongId(value.getAfter().getCASHFLOWID()).orElseThrow())
                .setDDCashFlowAlternateKey2(after.getFLOWID())
                .setDDCashFlowDescr(Optional.ofNullable(value.getAfter().getCASHFLOWDESCR()).map(v -> v.substring(0, Math.min(v.length(), DESCRIPTION_MAX_SIZE))).orElse(null))
                .setDDCashFlowReference(value.getAfter().getCASHFLOWREFERENCE())
                .setDDCashFlowLastUpdateDate(updateDate.toLocalDate().format(DateTimeFormatter.ofPattern("ddMMyyyy")))
                .setDDCashFlowLastUpdateTime(updateDate.toLocalTime())
                .setDDCashFlowLastUpdateUserName("")//todo: provide the name
                .setDDCashFlowStatus(CashFlowStatusEnum.fromCode(after.getFLOWSTATUS()).map(CashFlowStatusEnum::getExportCode).orElse(null))
                .setCashFlowCreationDate(creationDate.toLocalDate())
                .setCashFlowCreationTime(creationDate.toLocalTime())
                .setCashFlowItemsCount(toId(value.getAfter().getFLOWNUMBER()).map(BigInteger::intValue).orElse(null))
                .setAccountCurCashFlowsGrossAmount(after.getACCOUNTAMOUNT())
                .setFlowCurCashFlowsGrossAmount(after.getFLOWAMOUNT())
                .setETLUpdateTS(Instant.now())
                .build();
        return Tuple2.of(value, mappedFactCashFlow);
    }

    private LocalDateTime toLocalDateTime(long date) {
        long dateInMilli = date;
        if (date > 1000000000000000L) {
            dateInMilli = date / 1000;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(dateInMilli), ZoneOffset.UTC);
    }


    public enum CashFlowStatusEnum {
        ESTIMATED_FORECAST(0, "Estimated forecast"),
        CONFIRMED_FORECAST(1, "Confirmed forecast"),
        ACTUAL(2, "Actual"),
        INTRADAY(3, "Intraday");

        private final int code;
        private final String exportCode;

        CashFlowStatusEnum(int code, String exportCode) {
            this.code = code;
            this.exportCode = exportCode;
        }

        public static Optional<CashFlowStatusEnum> fromCode(Integer code) {
            if (code == null) {
                return Optional.empty();
            }
            for (CashFlowStatusEnum v : CashFlowStatusEnum.values()) {
                if (v.code == code) {
                    return Optional.of(v);
                }
            }
            return Optional.empty();
        }

        public int getCode() {
            return code;
        }

        public String getExportCode() {
            return exportCode;
        }
    }


}
