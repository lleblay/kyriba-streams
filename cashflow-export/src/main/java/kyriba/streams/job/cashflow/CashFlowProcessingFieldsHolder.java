/* ******************************************************************************
 * Copyright 2000 - 2019 Kyriba Corp. All Rights Reserved.                      *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *   Date            Author        Changes                                      *
 * 12.11.2020         M-VBE         Created                                      *
 ********************************************************************************/
package kyriba.streams.job.cashflow;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author M-VBE
 * @since 20.2
 */
public class CashFlowProcessingFieldsHolder implements Serializable {
    public String accountCurrencyCode;
    public String flowCurrencyCode;

    /**
     * @return a string in a format {0}-{1}/{2}, where {0} - {@code flowCurrencyCode}, {1} - accountCurrencyCode, {2} - date in ISO format
     */
    public String getCurrencyConversionString(LocalDate localDate) {
        return flowCurrencyCode + "-" + accountCurrencyCode + "/" + localDate.format(DateTimeFormatter.ISO_DATE);
    }
}
