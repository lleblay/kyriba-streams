DROP SCHEMA "DataWarehouse2" cascade ;

CREATE SCHEMA "DataWarehouse2" AUTHORIZATION postgres;

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimBank";

CREATE TABLE "DataWarehouse2"."DimBank" (
	"DWHTenantKey" uuid NOT NULL,
	"BankKey" bigserial NOT NULL,
	"BankAlternateKey" int8 NULL,
	"BankCode" varchar(10) NULL,
	"BankLEICode" varchar(150) NULL,
	"BankInterfaceCode" varchar(10) NULL,
	"BankExternalCode" varchar(20) NULL,
	"BankDescr1" varchar(150) NULL,
	"BankDescr2" varchar(150) NULL,
	"BankDefaultGroupName" varchar(10) NULL,
	"BankDefaultGroupDescr" varchar(35) NULL,
	"BankDealIdentifierName" varchar(35) NULL,
	"IsBankIntercompanyFlag" bool NULL,
	"IsBankIntermediaryFlag" bool NULL,
	"IsBankCounterpartyFlag" bool NULL,
	"IsBankInternalCounterpartyFlag" bool NULL,
	"IsBankNetSettlementsForThisCounterpartyFlag" bool NULL,
	"BankParentCounterpartyCode" varchar(12) NULL,
	"BankParentCounterpartyDescr" varchar(150) NULL,
	"BankThirdPartyCode" varchar(15) NULL,
	"BankThirdPartyDescr" varchar(100) NULL,
	"BankContactName" varchar(35) NULL,
	"BankForeignExchangeConfirmationMethodCode" varchar(150) NULL,
	"BankLoanConfirmationMethodCode" varchar(150) NULL,
	"BankElectronicConfirmationBICCode" varchar(11) NULL,
	"BankRiskTierName" varchar(15) NULL,
	"BankRiskTierDescr" varchar(50) NULL,
	"BankCashExposureLimitAmount" numeric(38,6) NULL,
	"BankCashExposureLimitCurrencyCode" varchar(5) NULL,
	"BankCashExposureLimitPerc" numeric(38,6) NULL,
	"BankWebAddressUrl" varchar(250) NULL,
	"BankStreetName1" varchar(100) NULL,
	"BankStreetName2" varchar(100) NULL,
	"BankCityName" varchar(35) NULL,
	"BankZIPCode" varchar(10) NULL,
	"BankStateName" varchar(35) NULL,
	"BankCountryCode" varchar(5) NULL,
	"BankCountryDescr" varchar(50) NULL,
	"BankHomepageURL" varchar(250) NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateDate" timestamp NULL,
	"LastUpdateBy" varchar(50) NULL,
	"IsDummy" bool NOT NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"IsBankNetDebitAndCreditExposure" bool NULL,
	"BankUserZone1" varchar(100) NULL,
	"BankUserZone2" varchar(100) NULL,
	"BankUserZone3" varchar(100) NULL,
	"BankUserZone4" varchar(100) NULL,
	"BankUserZone5" varchar(100) NULL,
	"BankUserZone1Description" varchar(500) NULL,
	"BankUserZone2Description" varchar(500) NULL,
	"BankUserZone3Description" varchar(500) NULL,
	"BankUserZone4Description" varchar(500) NULL,
	"BankUserZone5Description" varchar(500) NULL,
	"BankCashExposureLimitCurrencyDescr" varchar(100) NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimBank_pk" PRIMARY KEY ("DWHTenantKey", "BankKey")
);
CREATE INDEX "IdxDimBankForDelete" ON "DataWarehouse2"."DimBank" USING btree ("BankAlternateKey");


-- "DataWarehouse2"."DimCashFlowInfo" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimCashFlowInfo";

CREATE TABLE "DataWarehouse2"."DimCashFlowInfo" (
	"DWHTenantKey" uuid NOT NULL,
	"CashFlowKey" bigserial NOT NULL,
	"CashFlowCodeAlternateKey" int8 NULL,
	"CashFlowCodeName" varchar(50) NULL,
	"CashFlowOriginAlternateKey" varchar(50) NULL,
	"CashFlowOriginModuleCode" varchar(35) NULL,
	"CashFlowOriginProcessName" varchar(200) NULL,
	"CashFlowOriginData" varchar(50) NULL,
	"CashFlowStatusAlternateKey" varchar(50) NULL,
	"CashFlowActualModeAlternateKey" int4 NULL,
	"CashFlowGLStatusAlternateKey" varchar(50) NULL,
	"CashFlowStatusName" varchar(50) NULL,
	"CashFlowStatusCode" varchar(5) NULL,
	"CashFlowActualMode" varchar(50) NULL,
	"CashFlowGLStatusDescr" varchar(50) NULL,
	"ETLUpdateTS" timestamp NULL,
	"CashFlowGenerationMode" varchar(50) NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsDummy" bool NULL DEFAULT false,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimCashFlowInfo_pk" PRIMARY KEY ("DWHTenantKey", "CashFlowKey")
);


-- "DataWarehouse2"."DimCashFlowUserZone" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimCashFlowUserZone";

CREATE TABLE "DataWarehouse2"."DimCashFlowUserZone" (
	"DWHTenantKey" uuid NOT NULL,
	"CashFlowUserZoneKey" bigserial NOT NULL,
	"CashFlowUserZoneAlternateKey" text NULL,
	"CashFlowUserZone1Desc" varchar(350) NULL,
	"CashFlowUserZone2Desc" varchar(350) NULL,
	"CashFlowUserZone3Desc" varchar(350) NULL,
	"CashFlowUserZone4Desc" varchar(350) NULL,
	"CashFlowUserZone5Desc" varchar(350) NULL,
	"ETLUpdateTS" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsDummy" bool NULL DEFAULT false,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimCashFlowUserZone_pk" PRIMARY KEY ("DWHTenantKey", "CashFlowUserZoneKey")
);


-- "DataWarehouse2"."DimCountry" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimCountry";

CREATE TABLE "DataWarehouse2"."DimCountry" (
	"DWHTenantKey" uuid NOT NULL,
	"CountryKey" bigserial NOT NULL,
	"ISO2" varchar(5) NULL,
	"ISO3" varchar(5) NULL,
	"Country" varchar(35) NULL,
	"ETLUpdateTS" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsDummy" bool NULL DEFAULT false,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimCountry_pk" PRIMARY KEY ("DWHTenantKey", "CountryKey")
);


-- "DataWarehouse2"."DimCurrency" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimCurrency";

CREATE TABLE "DataWarehouse2"."DimCurrency" (
	"DWHTenantKey" uuid NOT NULL,
	"CurrencyKey" bigserial NOT NULL,
	"CurrencyCode" varchar(5) NULL,
	"CurrencyName" varchar(60) NULL,
	"IsDummy" bool NOT NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimCurrency_pk" PRIMARY KEY ("DWHTenantKey", "CurrencyKey"),
	CONSTRAINT "UQ_DimCurrency_DWHTenantKeyCurrencyCode" UNIQUE ("DWHTenantKey", "CurrencyCode")
);


-- "DataWarehouse2"."DimDate" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimDate";

CREATE TABLE "DataWarehouse2"."DimDate" (
	"DWHTenantKey" uuid NOT NULL,
	"DateKey" bigserial NOT NULL,
	"DateAlternateKey" varchar(8) NULL,
	"FullDate" date NULL,
	"DayInEpochNum" int4 NULL,
	"DateName" varchar(10) NULL,
	"DayOfWeekName" varchar(9) NULL,
	"DayOfCalendarWeekNum" int2 NULL,
	"DayOfCalendarMonthNum" int2 NULL,
	"DayOfCalendarYearNum" int2 NULL,
	"IsLastDayOfCalendarWeekFlag" bool NULL,
	"IsLastDayOfCalendarMonthFlag" bool NULL,
	"CalendarWeekOfMonthKey" int4 NULL,
	"CalendarWeekOfMonthName" varchar(17) NULL,
	"CalendarMonthKey" int4 NULL,
	"CalendarMonthInEpochNum" int2 NULL,
	"CalendarMonthAndYearName" varchar(14) NULL,
	"CalendarMonthAndYearNum" varchar(7) NULL,
	"CalendarMonthOfYearName" varchar(9) NULL,
	"CalendarMonthOfYearShortName" varchar(5) NULL,
	"CalendarMonthOfYearNum" int2 NULL,
	"CalendarQuarterKey" int2 NULL,
	"CalendarQuarterAndYearName" varchar(7) NULL,
	"CalendarQuarterOfYearName" varchar(5) NULL,
	"CalendarQuarterOfYearNum" int2 NULL,
	"CalendarSemesterKey" int2 NULL,
	"CalendarSemesterAndYearName" varchar(7) NULL,
	"CalendarSemesterOfYearName" varchar(5) NULL,
	"CalendarSemesterOfYearNum" int2 NULL,
	"CalendarYearKey" int2 NULL,
	"CalendarWeekKey" int4 NULL,
	"CalendarWeekInEpochNum" int2 NULL,
	"CalendarWeekAndYearName" varchar(13) NULL,
	"CalendarWeekOfYearName" varchar(7) NULL,
	"CalendarWeekOfYearNum" int2 NULL,
	"CalendarYearOfWeekKey" int2 NULL,
	"CalendarYearOfWeekName" varchar(6) NULL,
	"DayOfFiscalWeekNum" int2 NULL,
	"DayOfFiscalMonthNum" int2 NULL,
	"DayOfFiscalYearNum" int2 NULL,
	"IsLastDayOfFiscalWeekFlag" bool NULL,
	"IsLastDayOfFiscalMonthFlag" bool NULL,
	"FiscalWeekOfMonthKey" int4 NULL,
	"FiscalWeekOfMonthName" varchar(18) NULL,
	"FiscalMonthKey" int4 NULL,
	"FiscalMonthAndYearName" varchar(17) NULL,
	"FiscalMonthAndYearNum" varchar(10) NULL,
	"FiscalMonthOfYearName" varchar(9) NULL,
	"FiscalMonthOfYearShortName" varchar(5) NULL,
	"FiscalMonthOfYearNum" int2 NULL,
	"FiscalQuarterKey" int2 NULL,
	"FiscalQuarterAndYearName" varchar(8) NULL,
	"FiscalQuarterOfYearName" varchar(5) NULL,
	"FiscalQuarterOfYearNum" int2 NULL,
	"FiscalSemesterKey" int2 NULL,
	"FiscalSemesterAndYearName" varchar(8) NULL,
	"FiscalSemesterOfYearName" varchar(5) NULL,
	"FiscalSemesterOfYearNum" int2 NULL,
	"FiscalYearKey" int2 NULL,
	"FiscalYearName" varchar(7) NULL,
	"FiscalWeekKey" int4 NULL,
	"FiscalWeekAndYearName" varchar(14) NULL,
	"FiscalWeekOfYearName" varchar(8) NULL,
	"FiscalWeekOfYearNum" int2 NULL,
	"FiscalYearOfWeekKey" int2 NULL,
	"FiscalYearOfWeekName" varchar(8) NULL,
	"ETLUpdateTS" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsDummy" bool NULL DEFAULT false,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimDate_pk" PRIMARY KEY ("DWHTenantKey", "DateKey")
);


-- "DataWarehouse2"."DimTenant" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimTenant";

CREATE TABLE "DataWarehouse2"."DimTenant" (
	"DWHTenantKey" uuid NOT NULL,
	"TenantKey" bigserial NOT NULL,
	"TenantName" varchar(35) NULL,
	"IsDummy" bool NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimTenant_pk" PRIMARY KEY ("DWHTenantKey", "TenantKey"),
	CONSTRAINT uq_dimacccount_tenantname UNIQUE ("TenantName")
);


-- "DataWarehouse2"."DimBankBranch" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimBankBranch";

CREATE TABLE "DataWarehouse2"."DimBankBranch" (
	"DWHTenantKey" uuid NOT NULL,
	"BankBranchKey" bigserial NOT NULL,
	"BankBranchAlternateKey" int8 NULL,
	"BankBranchCode" varchar(10) NULL,
	"BankBranchInterfaceCode" varchar(10) NULL,
	"BankBranchDescr1" varchar(150) NULL,
	"BankBranchDescr2" varchar(150) NULL,
	"IsBankBranchIntercompanyFlag" bool NULL,
	"IsBankBranchMainBranchOfTheCountryFlag" bool NULL,
	"BankKey" int8 NULL,
	"BankBranchBankCode" varchar(10) NULL,
	"BankBranchBankDescr" varchar(150) NULL,
	"BankBranchAccountLocationName" varchar(35) NULL,
	"BankBranchBICCode" varchar(11) NULL,
	"BankBranchOtherIdentifierCode" varchar(35) NULL,
	"BankBranchOtherIdentifierTypeName" varchar(150) NULL,
	"IsBankBranchIntermediaryFlag" bool NULL,
	"BankBranchResponderCode" varchar(100) NULL,
	"BankBranchServiceName" varchar(30) NULL,
	"BankBranchTimeZoneCode" varchar(100) NULL,
	"BankBranchCutOffTime" time NULL,
	"BankBranchStreet1Name" varchar(100) NULL,
	"BankBranchStreet2Name" varchar(100) NULL,
	"BankBranchCityName" varchar(35) NULL,
	"BankBranchZIPCode" varchar(10) NULL,
	"BankBranchStateName" varchar(35) NULL,
	"BankBranchCountryCode" varchar(5) NULL,
	"BankBranchHomepage" varchar(250) NULL,
	"BankBranchContactName" varchar(150) NULL,
	"BankBranchContactDepartmentName" varchar(50) NULL,
	"BankBranchContactPhoneNum" varchar(35) NULL,
	"BankBranchContactFaxNum" varchar(35) NULL,
	"BankBranchContactEMail" varchar(150) NULL,
	"BankBranchCalendarCode" varchar(6) NULL,
	"BankBranchMemoDescr" varchar(1000) NULL,
	"BankBranchUserZone1Descr" varchar(350) NULL,
	"BankBranchUserZone2Descr" varchar(350) NULL,
	"BankBranchUserZone3Descr" varchar(350) NULL,
	"BankBranchUserZone4Descr" varchar(350) NULL,
	"BankBranchUserZone5Descr" varchar(350) NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateDate" timestamp NULL,
	"LastUpdateBy" varchar(50) NULL,
	"IsDummy" bool NOT NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"BankBranchCorpIDCode" varchar(35) NULL,
	"BankBranchOffsetStatements" varchar(35) NULL,
	"BankBranchTimeZoneName" varchar(100) NULL,
	"BankBranchDSTName" varchar(100) NULL,
	"BankBranchCodeStatements" varchar(100) NULL,
	"BankBranchCountryDescription" varchar(50) NULL,
	"BankBranchCalendarDescription" varchar(35) NULL,
	"SourceFile" varchar(100) NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimBankBranch_pk" PRIMARY KEY ("DWHTenantKey", "BankBranchKey"),
	CONSTRAINT "DimBankBranch_BankKey_fkey" FOREIGN KEY ("DWHTenantKey", "BankKey") REFERENCES "DataWarehouse2"."DimBank"("DWHTenantKey", "BankKey")
);
CREATE INDEX "IdxDimBankBranchForDelete" ON "DataWarehouse2"."DimBankBranch" USING btree ("BankBranchAlternateKey");
CREATE INDEX "idx_DimBankBranch_BankKey_fkey" ON "DataWarehouse2"."DimBankBranch" USING btree ("DWHTenantKey", "BankKey");


-- "DataWarehouse2"."DimCompany" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimCompany";

CREATE TABLE "DataWarehouse2"."DimCompany" (
	"DWHTenantKey" uuid NOT NULL,
	"CompanyKey" bigserial NOT NULL,
	"CompanyCountryDescr" varchar(150) NULL,
	"CompanyAlternateKey" int8 NULL,
	"CompanyCode" varchar(150) NULL,
	"CompanyShortCode" varchar(150) NULL,
	"CompanyInterfaceCode" varchar(150) NULL,
	"CompanyDescr1" varchar(150) NULL,
	"CompanyDescr2" varchar(150) NULL,
	"IsCompanyHiddenInListFlag" bool NULL,
	"CompanyDefaultGroupName" varchar(150) NULL,
	"CompanyDefaultGroupDescr" varchar(150) NULL,
	"CompanyCorpIdCode" varchar(150) NULL,
	"CompanyTXPCode" varchar(150) NULL,
	"CompanyOtherIdentifierTypeName" varchar(150) NULL,
	"CompanyOtherIdentifierCode" varchar(150) NULL,
	"CompanySepaCreditorCode" varchar(150) NULL,
	"CompanyConsolidationCode" varchar(150) NULL,
	"CompanyLEICode" varchar(150) NULL,
	"IsCompanyNonResidentFlag" bool NULL,
	"IsCompanyIssuerOfInterchangeFlag" bool NULL,
	"CompanyCurrencyCode" varchar(150) NULL,
	"CompanyCurrencyDescr" varchar(150) NULL,
	"CompanyGLCurrencyDescr" varchar(150) NULL,
	"CompanyAccountingCalendarName" varchar(150) NULL,
	"CompanyAccountingCalendarDescr" varchar(150) NULL,
	"CompanyStreet1Name" varchar(150) NULL,
	"CompanyStreet2Name" varchar(150) NULL,
	"CompanyCityName" varchar(150) NULL,
	"CompanyZIPCode" varchar(150) NULL,
	"CompanyStateName" varchar(150) NULL,
	"CompanyCountryName" varchar(150) NULL,
	"CompanyHomepageURL" varchar(150) NULL,
	"CompanyContactName" varchar(150) NULL,
	"CompanyContactDepartmentName" varchar(150) NULL,
	"CompanyContactPhoneNum" varchar(150) NULL,
	"CompanyContactFaxNum" varchar(150) NULL,
	"CompanyContactEMail" varchar(150) NULL,
	"CompanyLongitude" numeric(38,6) NULL,
	"CompanyLatitude" numeric(38,6) NULL,
	"IsCompanyIntermediaryFlag" bool NULL,
	"IsCompanyCounterpartyFlag" bool NULL,
	"CompanyContactCode" varchar(150) NULL,
	"CompanyLegalFormName" varchar(150) NULL,
	"CompanyCapitalAmount" numeric(38,6) NULL,
	"CompanyLegalRepresentative1Name" varchar(150) NULL,
	"CompanyLegalRepresentative1PositionName" varchar(150) NULL,
	"CompanyLegalRepresentative2Name" varchar(150) NULL,
	"CompanyLegalRepresentative2PositionName" varchar(150) NULL,
	"CompanyLegalFreeText1Descr" varchar(150) NULL,
	"CompanyLegalFreeText2Descr" varchar(150) NULL,
	"CompanyLegalFreeText3Descr" varchar(150) NULL,
	"CompanyLegalFreeText4Descr" varchar(150) NULL,
	"CompanyLegalFreeText5Descr" varchar(150) NULL,
	"IsCompanyBuyerInSCFModuleFlag" bool NULL,
	"CompanyThirdPartyCode" varchar(150) NULL,
	"CompanyThirdPartyDescr" varchar(150) NULL,
	"CompanyBEICode" varchar(150) NULL,
	"CompanyDistinguishedName" varchar(150) NULL,
	"CompanyUserZone1Descr" varchar(150) NULL,
	"CompanyUserZone2Descr" varchar(150) NULL,
	"CompanyUserZone3Descr" varchar(150) NULL,
	"CompanyUserZone4Descr" varchar(150) NULL,
	"CompanyUserZone5Descr" varchar(150) NULL,
	"CompanyGLCurrencyCode" varchar(150) NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateDate" timestamp NULL,
	"LastUpdateBy" varchar(50) NULL,
	"IsDummy" bool NOT NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"CompanyGLCurrencyRateType" varchar(150) NULL,
	"CompanyGLCurrencyRateTypeDescription" varchar(150) NULL,
	"CompanyNDFSettlementCurrency" varchar(150) NULL,
	"CompanyFundCode" varchar(150) NULL,
	"CompanyNetSettlementsForThisCounterparty" bool NULL,
	"CompanyIssuerSpread" numeric(38,6) NULL,
	"CheckBatchIDUniquenessOfBy" varchar(150) NULL,
	"CompanyBuyerGroup" varchar(150) NULL,
	"CompanyBuyerGroupDescription" varchar(150) NULL,
	"CheckBatchIDUniquenessFrom" varchar(150) NULL,
	"CheckBatchIDUniquenessTo" varchar(150) NULL,
	"CompanyNotificationUserGroup" varchar(150) NULL,
	"CompanyNotificationUserGroupDescription" varchar(150) NULL,
	"SourceFile" varchar(150) NULL,
	"CompanyAdvisoryFirm" varchar(150) NULL,
	"CompanyAuditorName" varchar(150) NULL,
	"CompanyAuditorCityName" varchar(150) NULL,
	"CompanyAuditorCountryName" varchar(150) NULL,
	"CompanyAuditorCountryDescr" varchar(150) NULL,
	"CompanyAuditorHomepageURL" varchar(150) NULL,
	"CompanyAuditorStateName" varchar(150) NULL,
	"CompanyAuditorStreet1Name" varchar(150) NULL,
	"CompanyAuditorStreet2Name" varchar(150) NULL,
	"CompanyAuditorZIPCode" varchar(150) NULL,
	"CompanyCurrencyKey" int8 NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimCompany_pk" PRIMARY KEY ("DWHTenantKey", "CompanyKey"),
	CONSTRAINT "DimCompany_CompanyCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "CompanyCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey")
);
CREATE INDEX "IdxDimCompanyForDelete" ON "DataWarehouse2"."DimCompany" USING btree ("CompanyAlternateKey");
CREATE INDEX "idx_DimCompany_CompanyCurrencyKey_fkey" ON "DataWarehouse2"."DimCompany" USING btree ("DWHTenantKey", "CompanyCurrencyKey");


-- "DataWarehouse2"."DimAccount" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimAccount";

CREATE TABLE "DataWarehouse2"."DimAccount" (
	"DWHTenantKey" uuid NOT NULL,
	"DWHSourceKey" varchar(30) NOT NULL,
	"AccountKey" bigserial NOT NULL,
	"AccountAlternateKey" int8 NULL,
	"AccountCode" varchar(12) NULL,
	"AccountDescr1" varchar(150) NULL,
	"AccountDescr2" varchar(150) NULL,
	"IsAccountHiddenInListsFlag" bool NULL,
	"AccountType" varchar(35) NULL,
	"AccountStatus" varchar(35) NULL,
	"IsAccountNonResidentFlag" bool NULL,
	"AccountOpeningDate" date NULL,
	"IsAccountClosedFlag" bool NULL,
	"AccountClosingDate" date NULL,
	"AccountCurrencyKey" int8 NULL,
	"CompanyKey" int8 NULL,
	"BankBranchKey" int8 NULL,
	"AccountDefaultGroupCode" varchar(10) NULL,
	"AccountDefaultGroupDescr" varchar(150) NULL,
	"AccountStreet1Name" varchar(100) NULL,
	"AccountStreet2Name" varchar(100) NULL,
	"AccountCityName" varchar(35) NULL,
	"AccountZIPCode" varchar(10) NULL,
	"AccountStateName" varchar(35) NULL,
	"AccountHomepageURL" varchar(250) NULL,
	"AccountContactName" varchar(150) NULL,
	"AccountContactDepartmentName" varchar(50) NULL,
	"AccountContactPhoneNum" varchar(35) NULL,
	"AccountContactFaxNum" varchar(35) NULL,
	"AccountContactEmail" varchar(150) NULL,
	"AccountCalendarCode" varchar(6) NULL,
	"AccountCalendarDescr" varchar(35) NULL,
	"IsAccountZBAGeneratorFlag" bool NULL,
	"AccountZBACounterpartyIdentifierCode" varchar(16) NULL,
	"AccountGenerateAZBAFlowForThisAccountRuleName" varchar(240) NULL,
	"AccountSettlementAccountCode" varchar(12) NULL,
	"AccountSettlementAccountDescr" varchar(150) NULL,
	"AccountCounterpartySettlementAccountCode" varchar(12) NULL,
	"AccountCounterpartySettlementAccountDescr" varchar(150) NULL,
	"AccountChartOfAccountsCode" varchar(20) NULL,
	"AccountChartOfAccountsDescr" varchar(35) NULL,
	"AccountGLAccountCode" varchar(50) NULL,
	"AccountGLAccountDescr" varchar(35) NULL,
	"AccountInternalAccountCode" varchar(50) NULL,
	"IsAccountIncludedInTheGLReconciliationFlag" bool NULL,
	"IsAccountConsideringOneDayFloatTransationsFlag" bool NULL,
	"IsAccountConsideringTwoDayFloatTransationsFlag" bool NULL,
	"IsAccountConsideringThreeDayFloatTransationsFlag" bool NULL,
	"IsAccountConsideringInvestmentPositionTransactionsFlag" bool NULL,
	"IsAccountIntegratingEndOfDayStatementsFlag" bool NULL,
	"IsAccountIntegratingIntradayStatementsFlag" bool NULL,
	"AccountTimeZoneCode" varchar(100) NULL,
	"AccountCutOffTime" time NULL,
	"AccountCategory01Code" varchar(15) NULL,
	"AccountCategory01Descr" varchar(50) NULL,
	"AccountCategory02Code" varchar(15) NULL,
	"AccountCategory02Descr" varchar(50) NULL,
	"AccountCategory03Code" varchar(15) NULL,
	"AccountCategory03Descr" varchar(50) NULL,
	"AccountCategory04Code" varchar(15) NULL,
	"AccountCategory04Descr" varchar(50) NULL,
	"AccountCategory05Code" varchar(15) NULL,
	"AccountCategory05Descr" varchar(50) NULL,
	"AccountCategory06Code" varchar(15) NULL,
	"AccountCategory06Descr" varchar(50) NULL,
	"AccountCategory07Code" varchar(15) NULL,
	"AccountCategory07Descr" varchar(50) NULL,
	"AccountCategory08Code" varchar(15) NULL,
	"AccountCategory08Descr" varchar(50) NULL,
	"AccountCategory09Code" varchar(15) NULL,
	"AccountCategory09Descr" varchar(50) NULL,
	"AccountCategory10Code" varchar(15) NULL,
	"AccountCategory10Descr" varchar(50) NULL,
	"IsAccountInterestBearingFlag" bool NULL,
	"IsAccountCentrallyManagedFlag" bool NULL,
	"AccountOwnerName" varchar(150) NULL,
	"AccountRenconcilerName" varchar(150) NULL,
	"AccountFreeText1Descr" varchar(50) NULL,
	"AccountFreeText2Descr" varchar(50) NULL,
	"AccountFreeText3Descr" varchar(50) NULL,
	"AccountFreeAmount1" numeric(38,6) NULL,
	"AccountFreeAmount2" numeric(38,6) NULL,
	"AccountFreeAmount3" numeric(38,6) NULL,
	"AccountUserZone1Descr" varchar(350) NULL,
	"AccountUserZone2Descr" varchar(350) NULL,
	"AccountUserZone3Descr" varchar(350) NULL,
	"AccountUserZone4Descr" varchar(350) NULL,
	"AccountUserZone5Descr" varchar(350) NULL,
	"IsAccountIncludedInElectronicBankContractsFlag" bool NULL,
	"IsAccountIncludedInElectronicBankContractsDate" date NULL,
	"IsAccountExcludedInElectronicBankContractsFlag" bool NULL,
	"IsAccountExcludedFromElectronicBankContractsDate" date NULL,
	"IsAccountPartOfConfirmTheAccountListFlag" bool NULL,
	"IsAccountPartOfConfirmTheAccountListDate" date NULL,
	"IsAccountPartOfConfirmTheAccountBalancesFlag" bool NULL,
	"IsAccountPartOfConfirmTheAccountBalancesDate" date NULL,
	"IsAccountPartOfConfirmtheAccountSignatoriesFlag" bool NULL,
	"IsAccountPartOfConfirmtheAccountSignatoriesDate" date NULL,
	"AccountDomesticTransferIssuerNum" varchar(10) NULL,
	"AccountInternationalTransferIssuerNum" varchar(10) NULL,
	"AccountMaturityTransferIssuerNum" varchar(10) NULL,
	"AccountDomesticDirectDebitIssuerNum" varchar(10) NULL,
	"AccountInternationalDirectDebitIssuerNum" varchar(10) NULL,
	"AccountPayableDraftsIssuerNum" varchar(10) NULL,
	"AccountReceivableDraftsIssuerNum" varchar(10) NULL,
	"IsAccountAvailableForPaymentsFlag" bool NULL,
	"AccountMemoDescr" varchar(1000) NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateDate" timestamp NULL,
	"LastUpdateBy" varchar(50) NULL,
	"IsDummy" bool NOT NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"AccountBanTypeCode" varchar(100) NULL,
	"AccountId" varchar(100) NULL,
	"AccountNature" varchar(100) NULL,
	"AccountIdStatementIdentifier" varchar(100) NULL,
	"FeeStatementIdentifier" varchar(100) NULL,
	"AccountIdDescription" varchar(500) NULL,
	"AccountIdReportingFilterType" varchar(100) NULL,
	"AccountIdEstablishment" varchar(100) NULL,
	"AccountIdEstablishmentDescription" varchar(500) NULL,
	"AccountCountryCode" varchar(10) NULL,
	"AccountCountryDescription" varchar(500) NULL,
	"ConsiderBankStatementsFrom" varchar(100) NULL,
	"InitialAccountingBalanceCode" varchar(100) NULL,
	"InitialAccountingBalance" numeric(38,6) NULL,
	"InitialAccountingBalanceDate" date NULL,
	"AccountTimeZoneDescription" varchar(100) NULL,
	"AccountCurrencyDescription" varchar(100) NULL,
	"Account" varchar(50) NULL,
	"SourceFile" varchar(150) NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"AccountBanType" varchar(100) NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimAccount_pk" PRIMARY KEY ("DWHTenantKey", "DWHSourceKey", "AccountKey"),
	CONSTRAINT uq_dimacccount_tenantalternatekey UNIQUE ("DWHTenantKey", "DWHSourceKey", "AccountAlternateKey")


);
CREATE INDEX "IdxDimAccountForDelete" ON "DataWarehouse2"."DimAccount" USING btree ("AccountAlternateKey");
CREATE INDEX "idx_DimAccount_AccountCurrencyKey_fkey" ON "DataWarehouse2"."DimAccount" USING btree ("DWHTenantKey", "AccountCurrencyKey");
CREATE INDEX "idx_DimAccount_BankBranchKey_fkey" ON "DataWarehouse2"."DimAccount" USING btree ("DWHTenantKey", "BankBranchKey");
CREATE INDEX "idx_DimAccount_CompanyKey_fkey" ON "DataWarehouse2"."DimAccount" USING btree ("DWHTenantKey", "CompanyKey");


-- "DataWarehouse2"."DimBudget" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimBudget";

CREATE TABLE "DataWarehouse2"."DimBudget" (
	"DWHTenantKey" uuid NOT NULL,
	"BudgetKey" bigserial NOT NULL,
	"BudgetAlternateKey" int8 NULL,
	"BudgetCode" varchar(50) NULL,
	"BudgetGroup" varchar(50) NULL,
	"IsDummy" bool NOT NULL DEFAULT false,
	"ETLUpdateTS" timestamp NULL,
	"BudgetGroupingCodeKey" int8 NULL,
	"BudgetCodeGroupingUID" int8 NULL,
	"BudgetCodeDescription" varchar(500) NULL,
	"BudgetCodeType" varchar(150) NULL,
	"IsBudgetCodeHiddenInList" bool NULL,
	"IsActive" bool NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsLateArriving" bool NULL DEFAULT false,
	CONSTRAINT "DimBudget_pk" PRIMARY KEY ("DWHTenantKey", "BudgetKey")
);
CREATE INDEX "idx_DimBudget_BudgetGroupingCodeKey_fkey" ON "DataWarehouse2"."DimBudget" USING btree ("DWHTenantKey", "BudgetGroupingCodeKey");


-- "DataWarehouse2"."DimFlowCode" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."DimFlowCode";

CREATE TABLE "DataWarehouse2"."DimFlowCode" (
	"DWHTenantKey" uuid NOT NULL,
	"FlowCodeKey" bigserial NOT NULL,
	"FlowCode" varchar(50) NULL,
	"FlowCodeDescription" varchar(500) NULL,
	"FlowCodeType" varchar(100) NULL,
	"IsFlowCodeIncludedinBalanceCalculation" bool NULL,
	"IsFlowCodeIncludedinTurnoverCalculation" bool NULL,
	"IsBudgetCodeMandatoryForThisCashFlowCode" bool NULL,
	"IsFlowCodeHiddeninList" bool NULL,
	"FlowCodeBudgetUID" varchar(100) NULL,
	"FlowCodeUID" int8 NULL,
	"FlowCodeGroupingCode" varchar(100) NULL,
	"FlowCodeBudgetCode" varchar(100) NULL,
	"FlowCodeCounterpartyFlowCode" varchar(100) NULL,
	"FlowCodeGroupingUID" varchar(50) NULL,
	"FlowCodeCounterpartyFlowUID" varchar(50) NULL,
	"ETLUpdateTS" timestamp NULL,
	"FlowCodeGroupingKey" int8 NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateBy" varchar(100) NULL,
	"LastUpdateDate" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	"IsDummy" bool NULL DEFAULT false,
	"IsLateArriving" bool NULL DEFAULT false,
	"FlowCodeDefaultBudgetKey" int8 NULL,
	CONSTRAINT "DimFlowCode_pk" PRIMARY KEY ("DWHTenantKey", "FlowCodeKey"),
	CONSTRAINT uq_dimflowcode_dwhtenantkeyflowcodeuid UNIQUE ("DWHTenantKey", "FlowCodeUID")

);
CREATE INDEX "IdxDimFlowCodeForDelete" ON "DataWarehouse2"."DimFlowCode" USING btree ("FlowCodeUID");
CREATE INDEX "idx_DimFlowCode_FlowCodeGroupingKey_fkey" ON "DataWarehouse2"."DimFlowCode" USING btree ("DWHTenantKey", "FlowCodeGroupingKey");


-- "DataWarehouse2"."FactCashFlowForecasts" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."FactCashFlowForecasts";

CREATE TABLE "DataWarehouse2"."FactCashFlowForecasts" (
	"DWHTenantKey" uuid NOT NULL,
	"DWHSourceKey" varchar(30) NOT NULL,
	"CashFlowForecastKey" bigserial NOT NULL,
	"TenantKey" int8 NULL,
	"DDCashFlowForecastAlternateKey" int8 NULL,
	"DDCashFlowForecastAlternateKey2" varchar(15) NULL,
	"DDCashFlowForecastReference" varchar(150) NULL,
	"DDCashFlowForecastDescr" varchar(200) NULL,
	"DDCashFlowForecastLastUpdateDate" varchar(8) NULL,
	"DDCashFlowForecastLastUpdateTime" time NULL,
	"DDCashFlowForecastLastUpdateUserName" varchar(50) NULL,
	"TransactionDateKey" int8 NULL,
	"ValueDateKey" int8 NULL,
	"AccountingDateKey" int8 NULL,
	"AccountKey" int8 NULL,
	"CashFlowCurrencyKey" int8 NULL,
	"AccountCurrencyKey" int8 NULL,
	"CorporateCurrencyKey" int8 NULL,
	"BudgetKey" int8 NULL,
	"CashFlowInfoKey" int8 NULL,
	"CashFlowUserZoneKey" int8 NULL,
	"FlowCurCashForecastsGrossAmount" numeric(38,6) NULL,
	"FlowCurCashForecastFeesAmount" numeric(38,6) NULL,
	"FlowCurCashForecastCommissionsAmount" numeric(38,6) NULL,
	"FlowCurCashForecastInterestsAmount" numeric(38,6) NULL,
	"FlowCurCashForecastsNetAmount" numeric(38,6) NULL,
	"AccountCurCashForecastsGrossAmount" numeric(38,6) NULL,
	"AccountCurCashForecastFeesAmount" numeric(38,6) NULL,
	"AccountCurCashForecastCommissionsAmount" numeric(38,6) NULL,
	"AccountCurCashForecastInterestsAmount" numeric(38,6) NULL,
	"AccountCurCashForecastsNetAmount" numeric(38,6) NULL,
	"CorpCurCashForecastsGrossAmount" numeric(38,6) NULL,
	"CorpCurCashForecastFeesAmount" numeric(38,6) NULL,
	"CorpCurCashForecastCommissionsAmount" numeric(38,6) NULL,
	"CorpCurCashForecastInterestsAmount" numeric(38,6) NULL,
	"CorpCurCashForecastsNetAmount" numeric(38,6) NULL,
	"CashFlowForecastItemsCount" int4 NULL,
	"DDCashFlowForecastDirection" varchar(50) NULL,
	"DDCashFlowForecastStatus" varchar(50) NULL,
	"LastUpdateBatchDate" timestamp NOT NULL,
	"CashFlowForecastRank" int4 NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateDate" timestamp NULL,
	"LastUpdateBy" varchar(50) NULL,
	"IsLast" bool NOT NULL,
	"AccountCurCashForecastRate" float8 NULL,
	"CorpCurCashForecastRate" float8 NULL,
	"CashForecastCreationDate" date NULL,
	"CashForecastCreationTime" time NULL,
	"ETLUpdateTS" timestamp NULL,
	"EstimatedForecastAccountAlternateKey" int8 NULL,
	"EstimatedForecastFlowCodeAlternateKey" int8 NULL,
	"EstimatedForecastBudgetAlternateKey" int8 NULL,
	"EstimatedForecastTransactionDateKey" int8 NULL,
	"EstimatedForecastValueDateKey" int8 NULL,
	"EstimatedForecastFlowAmount" numeric(38,6) NULL,
	"EstimatedForecastFlowCurrency" varchar(3) NULL,
	"ConfirmedForecastAccountAlternateKey" int8 NULL,
	"ConfirmedForecastFlowCodeAlternateKey" int8 NULL,
	"ConfirmedForecastBudgetAlternateKey" int8 NULL,
	"ConfirmedForecastTransactionDateKey" int8 NULL,
	"ConfirmedForecastValueDateKey" int8 NULL,
	"ConfirmedForecastFlowAmount" numeric(38,6) NULL,
	"ConfirmedForecastFlowCurrency" varchar(3) NULL,
	"FlowCodeType" varchar(35) NULL,
	"DataDomainUid" varchar(35) NULL,
	"CashReconciliationTimestamp" timestamp NULL,
	"FlowCodeKey" int8 NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	CONSTRAINT "FactCashFlowForecasts_pk" PRIMARY KEY ("DWHTenantKey", "DWHSourceKey", "CashFlowForecastKey")
);
CREATE INDEX "IdxFactCashFlowForecastsForDelete" ON "DataWarehouse2"."FactCashFlowForecasts" USING btree ("DDCashFlowForecastAlternateKey", "LastUpdateDate");
CREATE INDEX "idx_FactCashFlowForecasts_AccountCurrencyKey_fkey" ON "DataWarehouse2"."FactCashFlowForecasts" USING btree ("DWHTenantKey", "AccountCurrencyKey");
CREATE INDEX "idx_FactCashFlowForecasts_CashFlowCurrencyKey_fkey" ON "DataWarehouse2"."FactCashFlowForecasts" USING btree ("DWHTenantKey", "CashFlowCurrencyKey");
CREATE INDEX "idx_FactCashFlowForecasts_ConfirmedForecastTransactionDateKey_f" ON "DataWarehouse2"."FactCashFlowForecasts" USING btree ("DWHTenantKey", "ConfirmedForecastTransactionDateKey");
CREATE INDEX "idx_FactCashFlowForecasts_CorporateCurrencyKey_fkey" ON "DataWarehouse2"."FactCashFlowForecasts" USING btree ("DWHTenantKey", "CorporateCurrencyKey");
CREATE INDEX "idx_FactCashFlowForecasts_EstimatedForecastTransactionDateKey_f" ON "DataWarehouse2"."FactCashFlowForecasts" USING btree ("DWHTenantKey", "EstimatedForecastTransactionDateKey");


-- "DataWarehouse2"."FactCashFlows" definition

-- Drop table

-- DROP TABLE "DataWarehouse2"."FactCashFlows";

CREATE TABLE "DataWarehouse2"."FactCashFlows" (
	"DWHTenantKey" uuid NOT NULL,
	"DWHSourceKey" varchar(30) NOT NULL,
	"CashFlowKey" bigserial NOT NULL,
	"TenantKey" int8 NULL,
	"DDCashFlowAlternateKey" int8 NULL,
	"DDCashFlowAlternateKey2" varchar(15) NULL,
	"DDCashFlowReference" varchar(150) NULL,
	"DDCashFlowDescr" varchar(200) NULL,
	"DDCashFlowLastUpdateDate" varchar(8) NULL,
	"DDCashFlowLastUpdateTime" time NULL,
	"DDCashFlowLastUpdateUserName" varchar(50) NULL,
	"TransactionDateKey" int8 NULL,
	"ValueDateKey" int8 NULL,
	"AccountingDateKey" int8 NULL,
	"AccountKey" int8 NULL,
	"CashFlowCurrencyKey" int8 NULL,
	"AccountCurrencyKey" int8 NULL,
	"CorporateCurrencyKey" int8 NULL,
	"BudgetKey" int8 NULL,
	"CashFlowInfoKey" int8 NULL,
	"CashFlowUserZoneKey" int8 NULL,
	"FlowCurCashFlowsGrossAmount" numeric(38,6) NULL,
	"FlowCurCashFlowFeesAmount" numeric(38,6) NULL,
	"FlowCurCashFlowCommissionsAmount" numeric(38,6) NULL,
	"FlowCurCashFlowInterestsAmount" numeric(38,6) NULL,
	"FlowCurCashFlowsNetAmount" numeric(38,6) NULL,
	"AccountCurCashFlowsGrossAmount" numeric(38,6) NULL,
	"AccountCurCashFlowFeesAmount" numeric(38,6) NULL,
	"AccountCurCashFlowCommissionsAmount" numeric(38,6) NULL,
	"AccountCurCashFlowInterestsAmount" numeric(38,6) NULL,
	"AccountCurCashFlowsNetAmount" numeric(38,6) NULL,
	"CorpCurCashFlowsGrossAmount" numeric(38,6) NULL,
	"CorpCurCashFlowFeesAmount" numeric(38,6) NULL,
	"CorpCurCashFlowCommissionsAmount" numeric(38,6) NULL,
	"CorpCurCashFlowInterestsAmount" numeric(38,6) NULL,
	"CorpCurCashFlowsNetAmount" numeric(38,6) NULL,
	"CashFlowItemsCount" int4 NULL,
	"DDCashFlowDirection" varchar(50) NULL,
	"DDCashFlowStatus" varchar(50) NULL,
	"IsActive" bool NULL DEFAULT true,
	"LastUpdateDate" timestamp NULL,
	"LastUpdateBy" varchar(50) NULL,
	"AccountCurCashFlowRate" float8 NULL,
	"CorpCurCashFlowRate" float8 NULL,
	"CashFlowCreationDate" date NULL,
	"CashFlowCreationTime" time NULL,
	"ETLUpdateTS" timestamp NULL,
	"CashReconciliationKey" int8 NULL,
	"CashFlowReconciliationAlternateKey" int8 NULL,
	"CashReconciliationDateKey" int4 NULL,
	"EstimatedForecastTransactionDateKey" int8 NULL,
	"EstimatedForecastValueDateKey" int8 NULL,
	"ConfirmedForecastValueDateKey" int8 NULL,
	"ConfirmedForecastTransactionDateKey" int8 NULL,
	"IntradayTransactionDateKey" int8 NULL,
	"IntradayValueDateKey" int8 NULL,
	"FlowCodeKey" int8 NULL,
	"EstimatedForecastFlowAmount" numeric(38,6) NULL,
	"EstimatedForecastFlowCurrency" varchar(3) NULL,
	"CashReconciliationImpact" varchar(50) NULL,
	"ConfirmedForecastFlowCurrency" varchar(3) NULL,
	"ConfirmedForecastFlowAmount" numeric(38,6) NULL,
	"EstimatedForecastAccountAlternateKey" int8 NULL,
	"EstimatedForecastFlowCodeAlternateKey" int8 NULL,
	"EstimatedForecastBudgetAlternateKey" int8 NULL,
	"ConfirmedForecastAccountAlternateKey" int8 NULL,
	"ConfirmedForecastFlowCodeAlternateKey" int8 NULL,
	"ConfirmedForecastBudgetAlternateKey" int8 NULL,
	"FlowCodeType" varchar(35) NULL,
	"DataDomainUid" varchar(35) NULL,
	"CashReconciliationTimestamp" timestamp NULL,
	"ETLCreateTS" timestamp NULL,
	"AuditKey" int8 NULL,
	CONSTRAINT "FactCashFlows_pk" PRIMARY KEY ("DWHTenantKey", "DWHSourceKey", "CashFlowKey"),
	CONSTRAINT uq_factcashflows_dwhtenantkeycashflowalternatekey UNIQUE ("DWHTenantKey", "DWHSourceKey", "DDCashFlowAlternateKey"),
	CONSTRAINT uq_factcashflows_dwhtenantkeycashflowkey UNIQUE ("DWHTenantKey", "DWHSourceKey", "CashFlowKey")
);
CREATE INDEX "IdxFactCashFlowsForDelete" ON "DataWarehouse2"."FactCashFlows" USING btree ("DDCashFlowAlternateKey");
CREATE INDEX "idx_FactCashFlows_AccountCurrencyKey_fkey" ON "DataWarehouse2"."FactCashFlows" USING btree ("DWHTenantKey", "AccountCurrencyKey");
CREATE INDEX "idx_FactCashFlows_CashFlowCurrencyKey_fkey" ON "DataWarehouse2"."FactCashFlows" USING btree ("DWHTenantKey", "CashFlowCurrencyKey");
CREATE INDEX "idx_FactCashFlows_CorporateCurrencyKey_fkey" ON "DataWarehouse2"."FactCashFlows" USING btree ("DWHTenantKey", "CorporateCurrencyKey");
CREATE INDEX "idx_FactCashFlows_FlowCodeKey_fkey" ON "DataWarehouse2"."FactCashFlows" USING btree ("DWHTenantKey", "FlowCodeKey");


-- "DataWarehouse2"."DimFlowCode" foreign keys

ALTER TABLE "DataWarehouse2"."DimFlowCode" ADD CONSTRAINT "DimFlowCode_FlowCodeDefaultBudgetKey_fkey" FOREIGN KEY ("DWHTenantKey", "FlowCodeDefaultBudgetKey") REFERENCES "DataWarehouse2"."DimBudget"("DWHTenantKey", "BudgetKey");


-- "DataWarehouse2"."FactCashFlowForecasts" foreign keys

ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_AccountCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "AccountCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_AccountKey_fkey" FOREIGN KEY ("DWHTenantKey","DWHSourceKey", "AccountKey") REFERENCES "DataWarehouse2"."DimAccount"("DWHTenantKey","DWHSourceKey", "AccountKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_AccountingDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "AccountingDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_BudgetKey_fkey" FOREIGN KEY ("DWHTenantKey", "BudgetKey") REFERENCES "DataWarehouse2"."DimBudget"("DWHTenantKey", "BudgetKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_CashFlowCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashFlowCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_CashFlowInfoKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashFlowInfoKey") REFERENCES "DataWarehouse2"."DimCashFlowInfo"("DWHTenantKey", "CashFlowKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_CashFlowUserZoneKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashFlowUserZoneKey") REFERENCES "DataWarehouse2"."DimCashFlowUserZone"("DWHTenantKey", "CashFlowUserZoneKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_ConfirmedForecastTransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "ConfirmedForecastTransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_ConfirmedForecastValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "ConfirmedForecastValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_CorporateCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "CorporateCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_EstimatedForecastTransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "EstimatedForecastTransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_EstimatedForecastValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "EstimatedForecastValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_FlowCodeKey_fkey" FOREIGN KEY ("DWHTenantKey", "FlowCodeKey") REFERENCES "DataWarehouse2"."DimFlowCode"("DWHTenantKey", "FlowCodeKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_TenantKey_fkey" FOREIGN KEY ("DWHTenantKey", "TenantKey") REFERENCES "DataWarehouse2"."DimTenant"("DWHTenantKey", "TenantKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_TransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "TransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlowForecasts" ADD CONSTRAINT "FactCashFlowForecasts_ValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "ValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");


-- "DataWarehouse2"."FactCashFlows" foreign keys

ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_AccountCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "AccountCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_AccountKey_fkey" FOREIGN KEY ("DWHTenantKey","DWHSourceKey", "AccountKey") REFERENCES "DataWarehouse2"."DimAccount"("DWHTenantKey","DWHSourceKey", "AccountKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_AccountingDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "AccountingDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_BudgetKey_fkey" FOREIGN KEY ("DWHTenantKey", "BudgetKey") REFERENCES "DataWarehouse2"."DimBudget"("DWHTenantKey", "BudgetKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_CashFlowCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashFlowCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_CashFlowInfoKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashFlowInfoKey") REFERENCES "DataWarehouse2"."DimCashFlowInfo"("DWHTenantKey", "CashFlowKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_CashFlowUserZoneKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashFlowUserZoneKey") REFERENCES "DataWarehouse2"."DimCashFlowUserZone"("DWHTenantKey", "CashFlowUserZoneKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_CashReconciliationDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "CashReconciliationDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_ConfirmedForecastTransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "ConfirmedForecastTransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_ConfirmedForecastValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "ConfirmedForecastValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_CorporateCurrencyKey_fkey" FOREIGN KEY ("DWHTenantKey", "CorporateCurrencyKey") REFERENCES "DataWarehouse2"."DimCurrency"("DWHTenantKey", "CurrencyKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_EstimatedForecastTransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "EstimatedForecastTransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_EstimatedForecastValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "EstimatedForecastValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_FlowCodeKey_fkey" FOREIGN KEY ("DWHTenantKey", "FlowCodeKey") REFERENCES "DataWarehouse2"."DimFlowCode"("DWHTenantKey", "FlowCodeKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_IntradayTransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "IntradayTransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_IntradayValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "IntradayValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_TenantKey_fkey" FOREIGN KEY ("DWHTenantKey", "TenantKey") REFERENCES "DataWarehouse2"."DimTenant"("DWHTenantKey", "TenantKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_TransactionDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "TransactionDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");
ALTER TABLE "DataWarehouse2"."FactCashFlows" ADD CONSTRAINT "FactCashFlows_ValueDateKey_fkey" FOREIGN KEY ("DWHTenantKey", "ValueDateKey") REFERENCES "DataWarehouse2"."DimDate"("DWHTenantKey", "DateKey");