###
POST http://cashflow-export.data.k8s.kod.kyriba.com/sources

{
    "name": "Oracle-Demo",
    "type": "ORACLE",
    "dbUser": "admin",
    "dbPassword": "JcdV3vvZvIzbOhGmwaeK",
    "dbName": "ORCL",
    "dbSchema":"Demo",
    "dbHostname": "dcusdemo3.cp0xllyoxtxr.us-east-1.rds.amazonaws.com"
}

###
GET http://cashflow-export.data.k8s.kod.kyriba.com/sources
Content-Type: application/json

###
POST http://cashflow-export.data.k8s.kod.kyriba.com/sources
Content-Type: application/json

{
    "name": "DIAMOND",
    "type": "ORACLE",
    "dbUser": "admin",
    "dbPassword": "JcdV3vvZvIzbOhGmwaeK",
    "dbName": "ORCL",
    "dbHostname": "dcusdemo3.cp0xllyoxtxr.us-east-1.rds.amazonaws.com",
    "dbServerName": "DATATEAM",
    "dbSchema":"DIAMOND"
}
###
DELETE http://cashflow-export.data.k8s.kod.kyriba.com/sources/DIAMOND

###
POST http://cashflow-export.data.k8s.kod.kyriba.com/targets
Content-Type: application/json

{
    "name": "DWH",
    "type": "PSQL",
    "dbUser": "postgres",
    "dbPassword": "cQrTIwzFpUviCHVbfuN5",
    "dbName": "postgres",
    "dbSchema":"DataWarehouse2",
    "dbServerName": "DATATEAM",
    "dbHostname": "aurorapgsql.cluster-cp0xllyoxtxr.us-east-1.rds.amazonaws.com"
}
###
DELETE http://cashflow-export.data.k8s.kod.kyriba.com/targets/DWH

###
POST http://cashflow-export.data.k8s.kod.kyriba.com/job/biexport
Content-Type: application/json

{
    "name": "CashFlow-Export-Diamond",
    "source": "DIAMOND",
    "target": "DWH",
    "parallelism": "4",
    "config": {
        "perimeter": "cash-flow",
        "report-ccy": "USD"
    }
}
###
DELETE http://cashflow-export.data.k8s.kod.kyriba.com/job/biexport/CashFlow-Export-Diamond


###
GET http://cashflow-export.data.k8s.kod.kyriba.com/sources
###
GET http://cashflow-export.data.k8s.kod.kyriba.com/targets
###
GET http://cashflow-export.data.k8s.kod.kyriba.com/job/biexport

### DEMO STARTS HERE
###
POST http://cashflow-export.data.k8s.kod.kyriba.com/sources
Content-Type: application/json

{
    "name": "EMERALD",
    "type": "ORACLE",
    "dbUser": "admin",
    "dbPassword": "JcdV3vvZvIzbOhGmwaeK",
    "dbName": "ORCL",
    "dbHostname": "dcusdemo3.cp0xllyoxtxr.us-east-1.rds.amazonaws.com",
    "dbServerName": "DATATEAM",
    "dbSchema":"EMERALD"
}
###
DELETE http://cashflow-export.data.k8s.kod.kyriba.com/sources/EMERALD
###
POST http://cashflow-export.data.k8s.kod.kyriba.com/job/biexport
Content-Type: application/json

{
    "name": "CashFlow-Export-Emerald",
    "source": "EMERALD",
    "target": "DWH",
    "parallelism": "4",
    "config": {
        "perimeter": "cash-flow",
        "report-ccy": "USD"
    }
}

###
DELETE http://cashflow-export.data.k8s.kod.kyriba.com/job/biexport/CashFlow-Export-Emerald