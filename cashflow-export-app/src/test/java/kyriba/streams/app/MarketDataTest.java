package kyriba.streams.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@MicronautTest
public class MarketDataTest {

    @Inject
    MarketDataClient marketDataClient;

    @Test
    void testMarketDataService() {
        assertEquals(
                0.9005292336947783,
                marketDataClient.rate("GBP-EUR", "2020-11-10").blockingGet());
    }
}
