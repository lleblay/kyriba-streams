package kyriba.streams.app;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Single;

import javax.validation.constraints.NotBlank;

@Client("/marketdata")
public interface MarketDataClient {
    @Get("/{ccypair}/{isodate}")
    Single<Double> rate(@NotBlank String ccypair, @NotBlank String isodate);
}
