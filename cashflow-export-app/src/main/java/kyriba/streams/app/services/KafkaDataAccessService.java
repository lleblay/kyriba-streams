package kyriba.streams.app.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.confluent.kafka.serializers.KafkaJsonSerializer;
import io.micronaut.runtime.event.annotation.EventListener;
import io.micronaut.runtime.server.event.ServerStartupEvent;
import kyriba.streams.app.configuration.AppConfig;
import kyriba.streams.app.kafkaUtils.GenericKafkaProducer;
import kyriba.streams.app.kafkaUtils.KafkaUtils;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;

@Singleton
public class KafkaDataAccessService {

    @Inject
    GenericKafkaProducer kafkaProducer;

    @Inject
    AppConfig appConfig;

    Properties configProducerProps = new Properties();

    ConfigDescriptionKStreamService configDescriptionKStreamService;

    @EventListener
    void onStartup(ServerStartupEvent event) {

        configProducerProps.setProperty("bootstrap.servers", appConfig.getBootstrapServersUrl());
        configProducerProps.setProperty("schema.registry.url",appConfig.getSchemaRegisteryUrl());
        configProducerProps.setProperty("group.id", appConfig.getAppName());
        configProducerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProducerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaJsonSerializer.class);
        configProducerProps.put("acks", "all");

        // make sure that topic to store export job configuration is created
        KafkaUtils.createTopic(appConfig.getJobConfigTopic(),3,2,configProducerProps);
        KafkaUtils.createTopic(appConfig.getBIExportSourceDescriptionTopic(),3,2,configProducerProps);
        KafkaUtils.createTopic(appConfig.getBIExportTargetDescriptionTopic(),3,2,configProducerProps);

        configDescriptionKStreamService = new ConfigDescriptionKStreamService();
        configDescriptionKStreamService.init(appConfig);
    }

    /*****************************************************************************/
    // BI Export Source Description methods
    /*****************************************************************************/

    // returns 0 if exist
    // returns 1 if created
    // return 2 if something went wrong
    public int pushBIExportSourceDescriptionToKafkaTopic(JsonNode configMessage, String key){

        if (configDescriptionKStreamService.biExportSourceDescriptionExists(key) && configMessage != null) return 0;

        try {
            kafkaProducer.pushMessageToTopic(configProducerProps,appConfig.getBIExportSourceDescriptionTopic(),key,configMessage);
        }catch (Exception e){
            return 2;
        }
        return 1;
    }

    public Collection<JsonNode> getAllBIExportSourceDescription(){
        return configDescriptionKStreamService.getAllBIExportSourceDescriptions();
    }

    public JsonNode getBIExportSourceDescriptionByName(String sourceName){
        return configDescriptionKStreamService.getBIExportSourceDescriptionByName(sourceName);
    }


    /*****************************************************************************/
    // BI Export Target Description methods
    /*****************************************************************************/

    // returns 0 if exist
    // returns 1 if created
    // return 2 if something went wrong
    public int pushBIExportTargetDescriptionToKafkaTopic(JsonNode configMessage, String key){

        if (configDescriptionKStreamService.biExportTargetDescriptionExists(key) && configMessage != null) return 0;

        try {
            kafkaProducer.pushMessageToTopic(configProducerProps,appConfig.getBIExportTargetDescriptionTopic(),key,configMessage);
        }catch (Exception e){
            return 2;
        }
        return 1;
    }

    public int cleanDebeziumOperator(String connectorName, String serverName){

        Properties config = new Properties();
        config.setProperty("bootstrap.servers", appConfig.getBootstrapServersUrl());
        config.setProperty("schema.registry.url",appConfig.getSchemaRegisteryUrl());
        config.setProperty("group.id", appConfig.getAppName());
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaJsonSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaJsonSerializer.class);
        config.put("acks", "all");
        ObjectMapper mapper = new ObjectMapper();

        ArrayNode key = mapper.createArrayNode();
        key.add(connectorName);
        ObjectNode serverNode = mapper.createObjectNode();
        serverNode.put("server", serverName);
        key.add(serverNode);

        try {
            kafkaProducer.pushMessageToTopic(config,appConfig.getConnectOffsetTopic(),key,null);
        }catch (Exception e){
            return 2;
        }
        return 1;
    }

    public Collection<JsonNode> getAllBIExportTargetDescription(){
        return configDescriptionKStreamService.getAllBIExportTargetDescriptions();
    }

    public JsonNode getBIExportTargetDescriptionByName(String sourceName){
        return configDescriptionKStreamService.getBIExportTargetDescriptionByName(sourceName);
    }

    /*****************************************************************************/
    // BI Export Job config methods
    /*****************************************************************************/

    public void pushBIExportJobConfigToKafkaTopic(JsonNode configMessage, String key){
        kafkaProducer.pushMessageToTopic(configProducerProps,appConfig.getJobConfigTopic(),key,configMessage);
    }

    public Collection<JsonNode> getBIExportConfigHistory(){
        return configDescriptionKStreamService.getBIExportJobConfigHistory();
    }

    public JsonNode getBIExportJobConfigByName(String jobName){
        return configDescriptionKStreamService.getBIExportJobConfigByName(jobName);
    }

}
