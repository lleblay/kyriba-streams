package kyriba.streams.app.services;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Singleton
public class MarketDataService {
    private final Map<String, CurrencyStore> currencyStoreMap = new HashMap<>();

    @PostConstruct
    public void load() {
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            String csvSplitBy = ",";
            String fileName = "conversionrates.csv";
            AtomicReference<Boolean> headerSet = new AtomicReference<>(false);
            List<String> headers = new ArrayList<>();
            Resources.readLines(classLoader.getResource(fileName), Charsets.UTF_8)
                    .stream()
                    .map(line -> line.split(csvSplitBy))
                    .filter(columns -> {
                        if(!headerSet.get()){
                            for (String column : columns) {
                                headers.add(column);
                            }
                            headerSet.set(true);
                            return false;
                        }
                        return true;
                    })
                    .map(currencyLine -> {
                        CurrencyStore store = new CurrencyStore();
                        store.setKey(convertString(currencyLine[4]));
                        store.setName(convertString(currencyLine[5]));
                        store.setTimePeriod(convertString(currencyLine[8]));
                        for (int i = 9;i < currencyLine.length;i++){
                            String date = convertString(headers.get(i));
                            LocalDate localDate = LocalDate.parse(date);
                            String value = convertString(currencyLine[i]);
                            if(!value.isEmpty()){
                                double rate = Double.parseDouble(value);
                                store.addRate(localDate, rate);
                            }
                        }
                        return store;
                    })
                    .forEach(store -> currencyStoreMap.put(store.getKey(), store));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String convertString(String str){
        return str.replace("\"", "");
    }

    public Boolean ccyExist(String ccy){
        return currencyStoreMap.containsKey(ccy);
    }

    public Double getRate(String ccy1, String ccy2, LocalDate date){
        Double rate1 = currencyStoreMap.get(ccy1).getRate(date);//CCY1/USD
        Double rate2 = currencyStoreMap.get(ccy2).getRate(date);//CCY2/USD
        return rate1/rate2;//CCY1/CCY2
    }

    private class CurrencyStore {
        private String key = "N/A";
        private String name = "N/A";
        private String timePeriod ="";
        private NavigableMap<LocalDate, Double> store = new TreeMap<>();

        public void addRate(LocalDate date, Double rate){
            store.put(date, rate);
        }

        public Double getRate(LocalDate date){
            Map.Entry<LocalDate, Double> entry = store.lowerEntry(date);
            if(entry==null){
                entry = store.firstEntry();
            }
            return entry.getValue();
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getTimePeriod() {
            return timePeriod;
        }

        public void setTimePeriod(String timePeriod) {
            this.timePeriod = timePeriod;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
