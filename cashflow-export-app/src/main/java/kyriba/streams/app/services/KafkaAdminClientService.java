package kyriba.streams.app.services;

import kyriba.streams.app.configuration.AppConfig;
import kyriba.streams.app.constants.TableNames;
import kyriba.streams.app.kafkaUtils.KafkaUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static kyriba.streams.app.constants.TableNames.SOURCE_CASH_TABLES;
import static kyriba.streams.app.constants.TableNames.TARGET_CASH_TABLES;
import static org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer.KEY_POLL_TIMEOUT;

@Singleton
public class KafkaAdminClientService {

    @Inject
    AppConfig appConfig;

    Properties properties;

    @PostConstruct
    public void init(){
        properties = new Properties();
        properties.setProperty("bootstrap.servers", appConfig.getBootstrapServersUrl());
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.setProperty(KEY_POLL_TIMEOUT, "1000");
        properties.setProperty("group.id", appConfig.getAppName());
    }

    public void createTopics(List<String> topics){
        for (String topic: topics){
            KafkaUtils.createTopic(topic,10,2,properties);
        }
    }

    public void precreateSourceTopics(String topicsPrefix){
        for (String table : SOURCE_CASH_TABLES) {
            String topic = String.format("%s.%s",topicsPrefix,table);
            KafkaUtils.createTopic(topic,10,2,properties);
        }
    }

    public void deleteSourceTopics(String topicsPrefix){
        for (String table : SOURCE_CASH_TABLES) {
            String topic = String.format("%s.%s",topicsPrefix,table);
            KafkaUtils.deleteTopic(topic,properties);
        }
    }


    public void precreateTargetTopics(String topicsPrefix){
        for (String table : TARGET_CASH_TABLES) {
            String topic = String.format("%s.%s",topicsPrefix,table);
            KafkaUtils.createTopic(topic,10,2,properties);
        }
    }

    public void deleteTargetTopics(String topicsPrefix){
        for (String table : TARGET_CASH_TABLES) {
            String topic = String.format("%s.%s",topicsPrefix,table);
            KafkaUtils.deleteTopic(topic,properties);
            topic = String.format("%sOut",table);
            KafkaUtils.deleteTopic(topic,properties);
        }
    }

    public void precreateIntermediaryTopics(){
        for (String table : TARGET_CASH_TABLES) {
            String topic = String.format("%sOut",table);
            KafkaUtils.createTopic(topic,10,2,properties);
        }
    }

}
