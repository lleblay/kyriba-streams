package kyriba.streams.app.services;

import kyriba.streams.app.configuration.AppConfig;
import kyriba.streams.job.StreamingJob;
import org.apache.flink.api.common.JobID;
import org.apache.flink.client.program.ClusterClient;
import org.apache.flink.client.program.rest.RestClusterClient;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.JobManagerOptions;
import org.apache.flink.configuration.RestOptions;
import org.apache.flink.runtime.client.JobStatusMessage;
import org.apache.flink.runtime.jobgraph.JobGraph;
import org.apache.flink.shaded.guava18.com.google.common.io.Resources;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.graph.StreamGraph;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

@Singleton
public class FlinkClient {
    private ClusterClient<?> client;

    @Inject
    AppConfig appConfig;

    public JobID execute(String jobName, String sourceKey, String sourcePrefix, String targetPrefix, int parallelism){
        Configuration configuration = new Configuration();
        configuration.setString(JobManagerOptions.ADDRESS, appConfig.getFlinkUrl());
        configuration.setInteger(JobManagerOptions.PORT, appConfig.getFlinkPort());
        configuration.setInteger(RestOptions.PORT, appConfig.getFlinkPort());

        try {
            this.client = new RestClusterClient<>(configuration, "RemoreStreamEnvironment");
        } catch (Exception e) {
            throw new RuntimeException("Cannot establish connection to Job:"+e.getMessage(),e);
        }

        StreamExecutionEnvironment streamExecutionEnvironment = StreamingJob.getStreamExecutionEnvironment(sourcePrefix,targetPrefix,sourceKey, parallelism);
        StreamGraph streamGraph = streamExecutionEnvironment.getStreamGraph(jobName);
        JobGraph jobGraph = streamGraph.getJobGraph();
        jobGraph.addJars(Collections.singletonList(getJarUrl("cashflow-export-0.1-SNAPSHOT-all")));
        CompletableFuture<JobID> jobIDCompletableFuture = client.submitJob(jobGraph);

        JobID join = jobIDCompletableFuture.join();
        return join;
    }

    public void deleteJob(String jobName){
        Configuration configuration = new Configuration();
        configuration.setString(JobManagerOptions.ADDRESS, appConfig.getFlinkUrl());
        configuration.setInteger(JobManagerOptions.PORT, appConfig.getFlinkPort());
        configuration.setInteger(RestOptions.PORT, appConfig.getFlinkPort());

        try {
            this.client = new RestClusterClient<>(configuration, "RemoreStreamEnvironment");
        } catch (Exception e) {
            throw new RuntimeException("Cannot establish connection to Job:"+e.getMessage(),e);
        }

        try {
            Collection<JobStatusMessage> jobStatusMessages = client.listJobs().get();
            for (JobStatusMessage jobStatusMessage : jobStatusMessages) {
                if(jobStatusMessage.getJobName().equals(jobName)){
                    client.cancel(jobStatusMessage.getJobId());
                }
            }
        } catch (Exception e) {
            throw  new RuntimeException(e.getMessage(),e);
        }

    }


    URL getJarUrl(String jarName){
        String fileName = jarName + ".jar";
        URL embeddedJar = Resources.getResource(fileName);
        try{
            if(embeddedJar.toURI().getPath()==null){
                Path path = Paths.get(fileName);
                if(path.toFile().exists()==false){
                    throw new RuntimeException("File "+ path + " does not exists." );
                }
                embeddedJar = path.toUri().toURL();
            }
        }
        catch(URISyntaxException | MalformedURLException e){
            throw new RuntimeException(e.getMessage(),e);
        }
        return embeddedJar;
    }

}
