package kyriba.streams.app.services;

import com.fasterxml.jackson.databind.JsonNode;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import kyriba.streams.app.configuration.AppConfig;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static io.micronaut.http.HttpRequest.POST;

@Singleton
public class BIExportRestClient {


    @Client("http://connect.data.k8s.kod.kyriba.com/connectors")
    @Inject
    RxHttpClient httpClient;

    @Inject
    AppConfig appConfig;

    public Optional<JsonNode> createConnector(Map<String,Object> connectorConfig){
        String url = appConfig.getConnectServerUrl();
        Flowable<HttpResponse<JsonNode>> call = httpClient.exchange(
                POST(url,connectorConfig).contentType(MediaType.APPLICATION_JSON),
                JsonNode.class
        );
        HttpResponse<JsonNode> response = call.blockingFirst();
        if (response.status() == HttpStatus.CREATED || response.status() == HttpStatus.CONFLICT) return response.getBody(JsonNode.class);
       return null;
    }

    public Maybe<List<String>> getConnectors(){
        String url = appConfig.getConnectServerUrl();
        HttpRequest<?> getReq = HttpRequest.GET(url);
        Flowable flowable = httpClient.retrieve(getReq, List.class);
        return flowable.firstElement();
    }

    public boolean deleteConnector(String connectorName){
        String url = appConfig.getConnectServerUrl();
        HttpRequest<?> getReq = HttpRequest.DELETE(url + "/" + connectorName);
        Flowable<HttpResponse> flowable = httpClient.exchange(getReq).cast(HttpResponse.class);
        return flowable.firstElement().blockingGet().status() == HttpStatus.NO_CONTENT;
    }

}
