package kyriba.streams.app.services;

import com.fasterxml.jackson.databind.JsonNode;
import kyriba.streams.app.configuration.AppConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public class ConfigDescriptionKStreamService {

    AppConfig appConfig;

    KafkaStreams streams;

    public void init(AppConfig appConfig){
        this.appConfig = appConfig;
        createStream();
    }

    static final String CONFIG_KTABLE_STORE = "bi-export-job-config-store";
    static final String SOURCE_DESCR_KTABLE_STORE = "bi-export-source-descr-store";
    static final String TARGET_DESCR_KTABLE_STORE = "bi-export-target-descr-store";

    private void createStream() {
        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, appConfig.getAppName());
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG, appConfig.getAppName());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, appConfig.getBootstrapServersUrl());
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, "/tmp/kstreams");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        final StreamsBuilder builder = new StreamsBuilder();

        Deserializer<JsonNode> jsonDeserializer = new JsonDeserializer();
        Serializer<JsonNode> jsonSerializer = new JsonSerializer();

        Serde<JsonNode> jsonSerde = Serdes.serdeFrom(jsonSerializer, jsonDeserializer);

        final GlobalKTable<String, JsonNode> biExportJobConfigTable =
                builder.globalTable(appConfig.getJobConfigTopic(), Materialized.<String, JsonNode, KeyValueStore<Bytes, byte[]>>as(CONFIG_KTABLE_STORE)
                        .withKeySerde(Serdes.String())
                        .withValueSerde(jsonSerde));

        final GlobalKTable<String, JsonNode> biExportSourceDescTable =
                builder.globalTable(appConfig.getBIExportSourceDescriptionTopic(), Materialized.<String, JsonNode, KeyValueStore<Bytes, byte[]>>as(SOURCE_DESCR_KTABLE_STORE)
                        .withKeySerde(Serdes.String())
                        .withValueSerde(jsonSerde));

        final GlobalKTable<String, JsonNode> biExportTargetDescTable =
                builder.globalTable(appConfig.getBIExportTargetDescriptionTopic(), Materialized.<String, JsonNode, KeyValueStore<Bytes, byte[]>>as(TARGET_DESCR_KTABLE_STORE)
                        .withKeySerde(Serdes.String())
                        .withValueSerde(jsonSerde));

        streams = new KafkaStreams(builder.build(), streamsConfiguration);

        streams.cleanUp();
        streams.start();
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    public Collection<JsonNode> getBIExportJobConfigHistory(){
        ReadOnlyKeyValueStore<String, JsonNode> keyValueStore =
                streams.store(CONFIG_KTABLE_STORE, QueryableStoreTypes.keyValueStore());
        KeyValueIterator<String, JsonNode> range = keyValueStore.all();
        List<JsonNode> configHistoryList = new ArrayList<>();
        while (range.hasNext()) {
            KeyValue<String, JsonNode> next = range.next();
            configHistoryList.add(next.value);
        }
        range.close();
        return configHistoryList;
    }

    public JsonNode getBIExportJobConfigByName(String jobName) {
        ReadOnlyKeyValueStore<String, JsonNode> keyValueStore =
                streams.store(CONFIG_KTABLE_STORE, QueryableStoreTypes.keyValueStore());
        return keyValueStore.get(jobName);
    }

    public boolean biExportSourceDescriptionExists(String  key){
        ReadOnlyKeyValueStore<String, JsonNode> keyValueStore =
                streams.store(SOURCE_DESCR_KTABLE_STORE, QueryableStoreTypes.keyValueStore());
        return keyValueStore.get(key) != null;
    }

    public Collection<JsonNode> getAllBIExportSourceDescriptions(){
        ReadOnlyKeyValueStore<String, JsonNode> keyValueStore =
                streams.store(SOURCE_DESCR_KTABLE_STORE, QueryableStoreTypes.keyValueStore());
        return fromKeyValueIterator(keyValueStore.all());
    }

    public JsonNode getBIExportSourceDescriptionByName(String sourceName) {
        return (JsonNode) streams.store(SOURCE_DESCR_KTABLE_STORE, QueryableStoreTypes.keyValueStore()).get(sourceName);
    }


    public boolean biExportTargetDescriptionExists(String  key){
        ReadOnlyKeyValueStore<String, JsonNode> keyValueStore =
                streams.store(TARGET_DESCR_KTABLE_STORE, QueryableStoreTypes.keyValueStore());
        return keyValueStore.get(key) != null;
    }

    public Collection<JsonNode> getAllBIExportTargetDescriptions(){
        ReadOnlyKeyValueStore<String, JsonNode> keyValueStore =
                streams.store(TARGET_DESCR_KTABLE_STORE, QueryableStoreTypes.keyValueStore());
        return fromKeyValueIterator(keyValueStore.all());
    }

    public JsonNode getBIExportTargetDescriptionByName(String sourceName) {
        return (JsonNode) streams.store(TARGET_DESCR_KTABLE_STORE, QueryableStoreTypes.keyValueStore()).get(sourceName);
    }


    private Collection<JsonNode> fromKeyValueIterator(KeyValueIterator<String, JsonNode> iterator){
        List<JsonNode> list = new ArrayList<>();
        while (iterator.hasNext()) {
            KeyValue<String, JsonNode> next = iterator.next();
            list.add(next.value);
        }
        iterator.close();
        return list;
    }
}
