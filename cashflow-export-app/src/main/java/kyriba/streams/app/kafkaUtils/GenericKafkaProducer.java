package kyriba.streams.app.kafkaUtils;

import com.fasterxml.jackson.databind.JsonNode;
import kyriba.streams.app.configuration.AppConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Properties;

@Singleton
public class GenericKafkaProducer {

    @Inject
    AppConfig appConfig;

    @PostConstruct
    public void init() {

    }

    public void pushMessageToTopic(Properties props, String topic, String key, JsonNode value) {
        Producer<String, JsonNode> producer = new KafkaProducer<>(props);
        producer.send(new ProducerRecord<String, JsonNode>(topic, key, value), (m, e) -> {
            if (e != null) {
                e.printStackTrace();
            } else {
                System.out.printf("Produced record to topic %s partition [%d] @ offset %d%n", m.topic(), m.partition(), m.offset());
            }
        });

        producer.flush();
        producer.close();
    }

    public void pushMessageToTopic(Properties props, String topic, JsonNode key, JsonNode value) {
        Producer<JsonNode, JsonNode> producer = new KafkaProducer<>(props);
        producer.send(new ProducerRecord<JsonNode, JsonNode>(topic, key, value), (m, e) -> {
            if (e != null) {
                e.printStackTrace();
            } else {
                System.out.printf("Produced record to topic %s partition [%d] @ offset %d%n", m.topic(), m.partition(), m.offset());
            }
        });

        producer.flush();
        producer.close();
    }
}
