package kyriba.streams.app.constants;

import java.util.Arrays;
import java.util.List;

public class TableNames {

    public static List<String> SOURCE_CASH_TABLES = Arrays.asList("CASH_FLOW","COMPANY_BANK_ACCOUNT","CORPORATE_CUSTOMER","CASH_FLOW_CODE","CURRENCY");
    public static List<String> TARGET_REPLICATED_CASH_TABLES = Arrays.asList("DimFlowCode","DimAccount","DimTenant","DimCurrency");
    public static List<String> TARGET_CASH_TABLES = Arrays.asList("DimFlowCode","FactCashFlows","DimAccount","DimTenant","DimCurrency");

}
