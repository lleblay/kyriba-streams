package kyriba.streams.app.configuration;

import io.micronaut.context.annotation.Property;

import javax.inject.Singleton;

@Singleton
public class AppConfig {

    @Property(name = "micronaut.application.name")
    String appName;

    @Property(name = "kafka.schema.registry.url")
    String schemaRegisteryUrl;

    @Property(name = "kafka.bootstrap.servers.url")
    String bootstrapServersUrl;

    @Property(name = "kafka.export.jobs.config.topic")
    String jobConfigTopic;

    @Property(name = "kafka.connect.servers.url")
    String connectServerUrl;
    @Property(name = "kafka.connect.offset.topic")
    String connectOffsetTopic;
    @Property(name = "kafka.source.description.topic")
    String BIExportSourceDescriptionTopic;
    @Property(name = "kafka.target.description.topic")
    String BIExportTargetDescriptionTopic;
    @Property(name = "flink.url")
    String flinkUrl;
    @Property(name = "flink.port")
    Integer flinkPort;

    public String getConnectOffsetTopic() {
        return connectOffsetTopic;
    }

    public Integer getFlinkPort() {
        return flinkPort;
    }

    public String getFlinkUrl() {
        return flinkUrl;
    }


    public String getSchemaRegisteryUrl() {
        return schemaRegisteryUrl;
    }

    public String getBootstrapServersUrl() {
        return bootstrapServersUrl;
    }

    public String getJobConfigTopic() {
        return jobConfigTopic;
    }

    public String getAppName() {
        return appName;
    }

    public String getConnectServerUrl() {
        return connectServerUrl;
    }

    public String getBIExportSourceDescriptionTopic() {
        return BIExportSourceDescriptionTopic;
    }

    public String getBIExportTargetDescriptionTopic() {
        return BIExportTargetDescriptionTopic;
    }
}
