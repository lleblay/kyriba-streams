package kyriba.streams.app.configuration;

import com.fasterxml.jackson.databind.JsonNode;
import kyriba.streams.app.constants.TableNames;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
public class KafkaConnectConfigurationProvider {

    @Inject
    AppConfig appConfig;

    public Map<String, Object> sourceConnectorConfigFromSourceDescription(JsonNode sourceDescr) throws Exception {
        HashMap<String, Object> rootMap = new HashMap<>();
        HashMap<String, String> configMap = new HashMap<>();

        String schemaName = sourceDescr.get("dbSchema").asText();
        if (StringUtils.isEmpty(schemaName)) {
            throw new Exception("Found empty schema name in Source Description: " + sourceDescr.toPrettyString());
        }
        List<String> sourceTables = TableNames.SOURCE_CASH_TABLES.stream().map(table -> String.format("%s.%s", schemaName, table)).collect(Collectors.toList());

        String sourceName = sourceDescr.get("name").asText();
        String dbUser = sourceDescr.get("dbUser").asText();
        String dbPassword = sourceDescr.get("dbPassword").asText();
        String dbName = sourceDescr.get("dbName").asText();
        String hostname = sourceDescr.get("dbHostname").asText();
        String dbServerName = sourceDescr.get("dbServerName").asText();
        if (StringUtils.isEmpty(sourceName))
            throw new Exception("Found empty connector name in Source Description:" + sourceDescr.toPrettyString());
        if (StringUtils.isEmpty(dbUser))
            throw new Exception("Found empty database user in Source Description:" + sourceDescr.toPrettyString());
        if (StringUtils.isEmpty(dbPassword))
            throw new Exception("Found empty password in Source Description:" + sourceDescr.toPrettyString());
        if (StringUtils.isEmpty(dbName))
            throw new Exception("Found empty database name in Source Description:" + sourceDescr.toPrettyString());
        if (StringUtils.isEmpty(hostname))
            throw new Exception("Found empty database hostname in Source Description:" + sourceDescr.toPrettyString());
        if (StringUtils.isEmpty(dbServerName))
            throw new Exception("Found empty database server name in Source Description:" + sourceDescr.toPrettyString());

        configMap.put("connector.class", "io.debezium.connector.oracle.OracleConnector");
        configMap.put("database.user", dbUser);
        configMap.put("database.dbname", dbName);
        configMap.put("database.schema", schemaName);
        configMap.put("tasks.max", "1");
        configMap.put("database.hostname", hostname);
        configMap.put("database.password", dbPassword);
        configMap.put("database.history.kafka.bootstrap.servers", appConfig.getBootstrapServersUrl());
        configMap.put("database.history.kafka.topic", "schema-changes.inventory");
        configMap.put("database.server.name", dbServerName);
        configMap.put("database.out.server.name", "dbzxout");
        configMap.put("database.port", "1521");
        configMap.put("database.tablename.case.insensitive", "true");
        configMap.put("database.oracle.version", "11");
        configMap.put("tombstones.on.delete", "false");
        configMap.put("table.include.list", String.join(",", sourceTables));
        configMap.put("database.connection.adapter", "logminer");

        rootMap.put("name", sourceName + "-ORCL-Source");
        rootMap.put("config", configMap);
        return rootMap;
    }

    public Map<String, Object> targetConnectorConfigFromTargetDescription(JsonNode targetDesc) throws Exception {
        HashMap<String, Object> rootMap = new HashMap<>();
        HashMap<String, String> configMap = new HashMap<>();

        String schemaName = targetDesc.get("dbSchema").asText();
        if (StringUtils.isEmpty(schemaName)) {
            throw new Exception("Found empty schema name in Target Description: " + targetDesc.toPrettyString());
        }
        List<String> targetTables = TableNames.TARGET_REPLICATED_CASH_TABLES.stream().map(table -> String.format("%s.%s", schemaName, table)).collect(Collectors.toList());

        String targetName = targetDesc.get("name").asText();
        String dbUser = targetDesc.get("dbUser").asText();
        String dbPassword = targetDesc.get("dbPassword").asText();
        String dbName = targetDesc.get("dbName").asText();
        String hostname = targetDesc.get("dbHostname").asText();
        String dbServerName = targetDesc.get("dbServerName").asText();
        if (StringUtils.isEmpty(targetName))
            throw new Exception("Found empty connector name in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(dbUser))
            throw new Exception("Found empty database user in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(dbPassword))
            throw new Exception("Found empty password in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(dbName))
            throw new Exception("Found empty database name in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(hostname))
            throw new Exception("Found empty database hostname in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(dbServerName))
            throw new Exception("Found empty database server name in target Description:" + targetDesc.toPrettyString());

        configMap.put("connector.class", "io.debezium.connector.postgresql.PostgresConnector");
        configMap.put("database.hostname", hostname);
        configMap.put("database.port", "5432");
        configMap.put("database.user", dbUser);
        configMap.put("database.password", dbPassword);
        configMap.put("database.dbname", dbName);
        configMap.put("database.server.name", dbServerName);
        configMap.put("tombstones.on.delete", "false");
        configMap.put("table.whitelist", String.join(",", targetTables));
        configMap.put("schema.whitelist", schemaName);
        configMap.put("plugin.name", "pgoutput");
        configMap.put("heartbeat.interval.ms", "1000");
        configMap.put("slot.name", targetName.toLowerCase().replace("-", "_"));

        rootMap.put("name", targetName + "-PG-Source");
        rootMap.put("config", configMap);
        return rootMap;
    }

    public Map<String, Object> targetSinkConnectorConfigFromTargetDescription(JsonNode targetDesc, String tableName) throws Exception {

        HashMap<String, Object> rootMap = new HashMap<>();
        HashMap<String, String> configMap = new HashMap<>();

        String schemaName = targetDesc.get("dbSchema").asText();
        if (StringUtils.isEmpty(schemaName)) {
            throw new Exception("Found empty schema name in Target Description: " + targetDesc.toPrettyString());
        }

        String targetName = targetDesc.get("name").asText();
        String dbUser = targetDesc.get("dbUser").asText();
        String dbPassword = targetDesc.get("dbPassword").asText();
        String hostname = targetDesc.get("dbHostname").asText();

        String topicToSince = String.format("%sOut", tableName);
        String targetTable = String.format("%s.%s", schemaName, tableName);


        String connectionUrl = String.format("jdbc:postgresql://%s:5432/postgres?stringtype=unspecified", hostname);

        if (StringUtils.isEmpty(targetName))
            throw new Exception("Found empty connector name in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(dbUser))
            throw new Exception("Found empty database user in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(dbPassword))
            throw new Exception("Found empty password in target Description:" + targetDesc.toPrettyString());
        if (StringUtils.isEmpty(hostname))
            throw new Exception("Found empty database hostname in target Description:" + targetDesc.toPrettyString());


        configMap.put("connector.class", "io.confluent.connect.jdbc.JdbcSinkConnector");
        configMap.put("connection.user", dbUser);
        configMap.put("connection.password", dbPassword);
        configMap.put("insert.mode", "upsert");
        configMap.put("pk.mode", "record_key");
        configMap.put("topics", String.join(",", topicToSince));
        configMap.put("table.name.format", targetTable);
        configMap.put("connection.url", connectionUrl);
        //"connection.url": "jdbc:postgresql://aurorapgsql.cluster-cp0xllyoxtxr.us-east-1.rds.amazonaws.com:5432/postgres?stringtype=unspecified",

        rootMap.put("name", targetName + "-" + targetTable + "-Sink");
        rootMap.put("config", configMap);
        return rootMap;
    }
}
