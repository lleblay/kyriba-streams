package kyriba.streams.app.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import kyriba.streams.app.configuration.KafkaConnectConfigurationProvider;
import kyriba.streams.app.constants.TableNames;
import kyriba.streams.app.services.BIExportRestClient;
import kyriba.streams.app.services.FlinkClient;
import kyriba.streams.app.services.KafkaAdminClientService;
import kyriba.streams.app.services.KafkaDataAccessService;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.JobID;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Controller("/job/biexport")
@Validated
public class BIExportJobController {

    @Inject
    private KafkaDataAccessService kafkaDataAccessService;

    @Inject
    private KafkaAdminClientService kafkaAdminClientService;

    @Inject
    BIExportRestClient biExportRestClient;

    @Inject
    private FlinkClient client;

    /**
     * @param job body
     * @return created job
     */
    @Operation(summary = "Creates a new bi export job",
            description = "Creates a new bi export job")
    @ApiResponse(
            content = @Content(mediaType = "application/json",
                    schema = @Schema(type="json"))
    )
    @ApiResponse(responseCode = "400", description = "Invalid Body Supplied")
    @Tag(name = "Create new BI Export Job")
    @Post(uri = "/", produces = MediaType.APPLICATION_JSON)
    public HttpResponse createJob(@Body @Valid JsonNode job) throws Exception {

        // pre-create  needed topics
        // check if there is already existing source and target, if yes create their topics
        String sourceName = job.get("source").asText();
        String targetName = job.get("target").asText();
        String jobName = job.get("name").asText();
        Integer parallelism = job.get("parallelism").asInt();

        if (StringUtils.isEmpty(jobName)){
            return HttpResponse.notFound("Can't create a job with no name!");
        }

        JsonNode source = kafkaDataAccessService.getBIExportSourceDescriptionByName(sourceName);
        JsonNode target = kafkaDataAccessService.getBIExportTargetDescriptionByName(targetName);
        if (source == null){
           return HttpResponse.notFound("No matching source found");
        }
        if (target ==  null){
            return HttpResponse.notFound("No matching target found");
        }
        String targetServerName = target.get("dbServerName").asText();
        String targetSchema = target.get("dbSchema").asText();
        String targetPrefix = String.format("%s.%s",targetServerName,targetSchema);

        String sourceServerName = source.get("dbServerName").asText();
        String sourceSchema = source.get("dbSchema").asText();
        String sourcePrefix = String.format("%s.%s",sourceServerName,sourceSchema);


        //submit flink job using flink client
        JobID id = client.execute(jobName, sourceName, sourcePrefix,targetPrefix, parallelism);

        // push job config to a kafka topic
        kafkaDataAccessService.pushBIExportJobConfigToKafkaTopic(job,job.get("name").asText());
        return HttpResponse.ok();
    }

    @Get(uri = "/", produces = MediaType.APPLICATION_JSON)
    public Collection<JsonNode> getAllJobs() {
        return kafkaDataAccessService.getBIExportConfigHistory();
    }

    @Get(uri = "/job/{jobName}", produces = MediaType.APPLICATION_JSON)
    public JsonNode getJobByName(final String jobName) {
        return kafkaDataAccessService.getBIExportJobConfigByName(jobName);
    }

    @Get(uri = "/connectors", produces = MediaType.APPLICATION_JSON)
    public List<String> getConnectors(){
        return biExportRestClient.getConnectors().blockingGet();
    }


    @Delete(uri = "/{jobName}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse deleteJob(final String jobName){
        // try to delete job related connectors if needed
        JsonNode jobConfig = kafkaDataAccessService.getBIExportJobConfigByName(jobName);
        if(jobConfig==null){
            throw new RuntimeException(jobName+" job not found.");
        }

        String jobTargetName = jobConfig.get("target").asText();
        String jobSourceName = jobConfig.get("source").asText();

        JsonNode sourceConfig = kafkaDataAccessService.getBIExportSourceDescriptionByName(jobSourceName);
        if(sourceConfig==null){
            throw new RuntimeException(jobSourceName+" source not found.");
        }
        JsonNode targetConfig = kafkaDataAccessService.getBIExportTargetDescriptionByName(jobTargetName);
        if(targetConfig==null){
            throw new RuntimeException(jobTargetName+" job not found.");
        }

        client.deleteJob(jobName);
        kafkaDataAccessService.pushBIExportJobConfigToKafkaTopic(null, jobName);

        return HttpResponse.ok();
    }

}
