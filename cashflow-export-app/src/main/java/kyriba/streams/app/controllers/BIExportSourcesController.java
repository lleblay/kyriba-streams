package kyriba.streams.app.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;
import kyriba.streams.app.configuration.KafkaConnectConfigurationProvider;
import kyriba.streams.app.services.BIExportRestClient;
import kyriba.streams.app.services.KafkaAdminClientService;
import kyriba.streams.app.services.KafkaDataAccessService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("/sources")
@Validated
public class BIExportSourcesController {

    @Inject
    BIExportRestClient biExportRestClient;
    @Inject
    KafkaConnectConfigurationProvider connectConfigurationProvider;
    @Inject
    private KafkaDataAccessService kafkaDataAccessService;
    @Inject
    private KafkaAdminClientService kafkaAdminClientService;

    @Post(uri = "/", produces = MediaType.APPLICATION_JSON)
    public HttpResponse createSourceDescription(@Body @Valid JsonNode sourceDescription) throws Exception {
        int res = kafkaDataAccessService.pushBIExportSourceDescriptionToKafkaTopic(sourceDescription, sourceDescription.get("name").asText());

        String sourceServerName = sourceDescription.get("dbServerName").asText();
        String sourceSchema = sourceDescription.get("dbSchema").asText();
        kafkaAdminClientService.createTopics(Arrays.asList(sourceServerName, "schema-changes.inventory"));
        kafkaAdminClientService.precreateSourceTopics(String.format("%s.%s", sourceServerName, sourceSchema));
        // get all available connectors
        List<String> allConnectors = biExportRestClient.getConnectors().blockingGet();

        // now as the topics are created, let's create the connectors
        Map<String, Object> sourceConnectorConfig = connectConfigurationProvider.sourceConnectorConfigFromSourceDescription(sourceDescription);

        // if source oracle connector isn't available, create it
        if (!allConnectors.contains(sourceConnectorConfig.get("name"))) {
            if (biExportRestClient.createConnector(sourceConnectorConfig) == null) {
                return HttpResponse.serverError("Can't create source connector! Internal Server Error!");
            }
        }

        Map<String, Object> body = new HashMap<>();
        if (res == 0) {
            body.put("error", "Source description already exist!");
            return HttpResponse.status(HttpStatus.FORBIDDEN).body(body);
        } else if (res == 1) {
            return HttpResponse.ok(sourceDescription);
        } else {
            return HttpResponse.serverError();
        }
    }

    @Delete(uri = "/{sourceName}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse deleteSourceDescription(final String sourceName) {

        JsonNode sourceConfig = kafkaDataAccessService.getBIExportSourceDescriptionByName(sourceName);
        if (sourceConfig == null) {
            throw new RuntimeException(sourceName + " source not found.");
        }

        String sourceServerName = sourceConfig.get("dbServerName").asText();
        String sourceSchema = sourceConfig.get("dbSchema").asText();

        // get all available connectors
        List<String> allConnectors = biExportRestClient.getConnectors().blockingGet();

        // delete source connectors if it's not used in other jobs
        for (String connector : allConnectors) {
            if (connector.startsWith(sourceName + "-")) {
                biExportRestClient.deleteConnector(connector);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                kafkaDataAccessService.cleanDebeziumOperator(connector, sourceConfig.get("dbServerName").asText());
            }
        }
        kafkaAdminClientService.deleteSourceTopics(String.format("%s.%s", sourceServerName, sourceSchema));

        kafkaDataAccessService.pushBIExportSourceDescriptionToKafkaTopic(null, sourceName);
        return HttpResponse.ok();
    }

    @Get(uri = "/", produces = MediaType.APPLICATION_JSON)
    public HttpResponse getAllSourceDescription() {
        return HttpResponse.ok(kafkaDataAccessService.getAllBIExportSourceDescription());
    }

    @Get(uri = "/{sourceName}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse getBIExportSourceDescriptionByName(final String sourceName) {
        return HttpResponse.ok(kafkaDataAccessService.getBIExportSourceDescriptionByName(sourceName));
    }
}
