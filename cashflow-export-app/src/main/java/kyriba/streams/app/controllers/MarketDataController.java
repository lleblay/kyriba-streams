package kyriba.streams.app.controllers;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.validation.Validated;
import io.reactivex.Single;
import kyriba.streams.app.services.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Controller("/marketdata")
@Validated
public class MarketDataController {
    private final MarketDataService service;
    private static final Logger LOG = LoggerFactory.getLogger(MarketDataController.class);

    @Inject
    public MarketDataController(MarketDataService service) {
        this.service = service;
    }

    @Get(uri = "/{ccypair}/{isodate}", produces = MediaType.APPLICATION_JSON)
    public Single<Double> rate(@NotBlank String ccypair, @NotBlank String isodate) {
        String ccy1 = ccypair.substring(0,3);
        if(!service.ccyExist(ccy1)){
            return Single.just(1.0);
            //throw new RuntimeException("Ccy: "+ccy1+" does not exist");
        }
        String ccy2 = ccypair.substring(4);
        if(!service.ccyExist(ccy2)){
            return Single.just(1.0);
            //throw new RuntimeException("Ccy: "+ccy2+" does not exist");
        }
        LocalDate localDate = LocalDate.parse(isodate);

        Double rate = service.getRate(ccy1, ccy2, localDate);
        LOG.info("Markete Data call for {}, ccy-pair : {}, isodate {}, date : {}, rate {}", ccypair, isodate, localDate, rate);
        return Single.just(rate);
    }
}
