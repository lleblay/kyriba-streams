package kyriba.streams.app.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.validation.Validated;
import kyriba.streams.app.configuration.KafkaConnectConfigurationProvider;
import kyriba.streams.app.constants.TableNames;
import kyriba.streams.app.services.BIExportRestClient;
import kyriba.streams.app.services.KafkaAdminClientService;
import kyriba.streams.app.services.KafkaDataAccessService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("/targets")
@Validated
public class BIExportTargetsController {

    @Inject
    private KafkaDataAccessService kafkaDataAccessService;

    @Inject
    private KafkaAdminClientService kafkaAdminClientService;

    @Inject
    BIExportRestClient biExportRestClient;

    @Inject
    KafkaConnectConfigurationProvider connectConfigurationProvider;

    @Post(uri = "/", produces = MediaType.APPLICATION_JSON)
    public HttpResponse createSourceDescription(@Body @Valid JsonNode targetDescription) throws Exception {

        String targetServerName = targetDescription.get("dbServerName").asText();
        String targetSchema = targetDescription.get("dbSchema").asText();

        kafkaAdminClientService.precreateTargetTopics(String.format("%s.%s",targetServerName,targetSchema));
        kafkaAdminClientService.precreateIntermediaryTopics();

        // get all available connectors
        List<String> allConnectors = biExportRestClient.getConnectors().blockingGet();

        // if postgres target connector isn't available, create it
        Map<String,Object> targetConnectorConfig = connectConfigurationProvider.targetConnectorConfigFromTargetDescription(targetDescription);

        if (!allConnectors.contains(targetConnectorConfig.get("name"))) {
            if (biExportRestClient.createConnector(targetConnectorConfig) == null) {
                return HttpResponse.serverError("Can't create target connector! Internal Server Error!");
            }
        }

        // if postgres target sink connector isn't available, create it
        for (String targetTable: TableNames.TARGET_CASH_TABLES){
            Map<String,Object> targetSinkConnectorConfig = connectConfigurationProvider.targetSinkConnectorConfigFromTargetDescription(targetDescription,targetTable);
            if (!allConnectors.contains(targetSinkConnectorConfig.get("name"))) {
                if (biExportRestClient.createConnector(targetSinkConnectorConfig) == null) {
                    return HttpResponse.serverError("Can't create target sink connector! Internal Server Error!");
                }
            }
        }

        String targetName = targetDescription.get("name").asText();
        int res = kafkaDataAccessService.pushBIExportTargetDescriptionToKafkaTopic(targetDescription, targetName);
        Map<String,Object> body = new HashMap<>();
        if (res == 0){
            body.put("error","Source description already exist!");
            return HttpResponse.status(HttpStatus.FORBIDDEN).body(body);
        }
        else if (res == 1){
            return HttpResponse.ok(targetDescription);
        }
        else {
            return HttpResponse.serverError();
        }
    }

    @Delete(uri = "/{targetName}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse deleteSourceDescription(final String targetName) throws InterruptedException {

        JsonNode target = kafkaDataAccessService.getBIExportTargetDescriptionByName(targetName);
        if (target == null){
            return HttpResponse.notFound("No matching target found");
        }

        // check if there is no jobs using the same source, if not, delete the source
        List<String> allConnectors = biExportRestClient.getConnectors().blockingGet();

        // delete target connectors if it's not used in other jobs
        for (String connector: allConnectors) {
            if (connector.startsWith(targetName+"-")){
                biExportRestClient.deleteConnector(connector);
                Thread.sleep(1000);
                kafkaDataAccessService.cleanDebeziumOperator(connector, target.get("dbServerName").asText());
            }
        }

        String targetServerName = target.get("dbServerName").asText();
        String targetSchema = target.get("dbSchema").asText();

        kafkaAdminClientService.deleteTargetTopics(String.format("%s.%s",targetServerName,targetSchema));
        kafkaDataAccessService.pushBIExportTargetDescriptionToKafkaTopic(null, targetName);
        return HttpResponse.ok();
    }

    @Get(uri = "/", produces = MediaType.APPLICATION_JSON)
    public HttpResponse getAllSourceDescription() {
        return HttpResponse.ok(kafkaDataAccessService.getAllBIExportTargetDescription());
    }

    @Get(uri = "/{targetName}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse getBIExportSourceDescriptionByName(final String targetName) {
        return HttpResponse.ok(kafkaDataAccessService.getBIExportTargetDescriptionByName(targetName));
    }
}
