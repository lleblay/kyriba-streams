package kyriba.streams.app;

import io.micronaut.runtime.Micronaut;

/**
 * @author Graeme Rocher
 * @since 1.0
 */
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.tags.*;
import io.swagger.v3.oas.annotations.servers.*;
import io.swagger.v3.oas.annotations.security.*;
/**
 * @author graemerocher
 * @since 1.0
 */
@OpenAPIDefinition(
        info = @Info(
                title = "BI Export Streaming App",
                version = "0.1",
                description = "This page provides details about the exposed APIs to trigger a stream processing flink jobs",
//                license = @License(name = "Apache 2.0", url = "http://foo.bar"),
                contact = @Contact(url = "http://Kyriba.com/streams", name = "Musab BAIRAT", email = "musab.bairat@kyriba.com")
        ),
//        tags = {
//                @Tag(name = "Tag 1", description = "desc 1", externalDocs = @ExternalDocumentation(description = "docs desc")),
//                @Tag(name = "Tag 2", description = "desc 2", externalDocs = @ExternalDocumentation(description = "docs desc 2")),
//                @Tag(name = "Tag 3")
//        },
        externalDocs = @ExternalDocumentation(description = "definition docs desc"),
        security = {
                @SecurityRequirement(name = "req 1", scopes = {"a", "b"}),
                @SecurityRequirement(name = "req 2", scopes = {"b", "c"})
        },
        servers = {
                @Server(
                        description = "BI Export Jobs APIs",
                        url = "http://Kyriba.com/streams",
                        variables = {
                                @ServerVariable(name = "var1", description = "var 1", defaultValue = "1", allowableValues = {"1", "2"}),
                                @ServerVariable(name = "var2", description = "var 2", defaultValue = "1", allowableValues = {"1", "2"})
                        })
        }
)
public class Application {
    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}
